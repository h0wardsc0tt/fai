
<cfoutput>
<div id="fai_search" class="container-fluid" style="min-height: 100%;position:relative;">
<cfinclude template="./_dat_fai_report_prep.cfm">
<cfinclude template="./_tmp_customer_lookup.cfm">

</div>

<div id="fai_report" class="container-fluid hidden" style="min-height: 100%;position:relative;">
	<div class="fai-header" align="center">(FAI) FIRST ARTICLE INSPECTION - DATA ENTRY SCREEN</div>
    <form action="" method="POST" id="fai_form" class="form-horizontal col-lg-12" role="form" accept-charset="UTF-8">
    <!--<a href="mailto:someone@example.com?Subject=Hello">Send Mail</a>-->
        <div class="row">
            <div class="form-col fai-header-cols col-xs-3">
               <div class="form-group">
                   <label for="id" class="control-label col-xs-2">ID:</label>
                    <div class="fai-input col-xs-7">
                        <input id="id" name="id" type="text" disabled style="width:80px; text-align:center;">
                    </div>
                </div>
            </div>
            <div class="form-col fai-header-cols col-xs-4">
                <div class="form-group">
                   <label for="faistatus" class="control-label col-xs-5">STATUS:</label>
                    <div class="fai-input">
                        <select class="level1 form-control col-xs-7" id="faistatus" name="faistatus" style="width:100px;">
                            <option value="IN WORK">In Work</option>
                            <option value="ON HOLD">On Hold</option>
                            <option value="CLOSED">Closed</option>
                            <option value="VOIDED">Voided</option>
                        </select>
                    </div>
                </div>
            </div>
           <div class="form-col fai-header-cols col-xs-5">
                <div class="form-group">
                   <label for="daystocomplete" class="control-label col-xs-6">DAYS&nbsp;TO&nbsp;COMP:</label>
                    <div class="fai-input col-xs-6">
                        <input id="daystocomplete" name="daystocomplete" disabled type="text" style="width:40px; text-align:center;" class="level1 numeric">
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom:3px;">
            <div class="form-col receitp-info col-xs-12">
            (Level 1) RECEIPT INFORMATION
            </div>
        </div>
    	<div class="row" style="position:relative;">
        <div class="blockout_level_1 blockout_section"></div>
            <div class="form-col fai-cols">
                <div class="form-group">
                    <label for="inspector" class="control-label fai-label-color">INSPECTOR :</label>
                    <div class="fai-input">
                        <select class="level1 form-control fai-input-color selectpicker" id="inspector" name="inspector"
                         data-size="5" data-live-search="true"  data-selected-text-format="count > 2">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="datereceived" class="control-label fai-label-color">DATE RECEIVED :</label>
                    <div class="fai-input">
                        <input type="text" autocomplete="off" maxlength="100" name="datereceived" id="datereceived" class="level1 form-control fai-input-color input-date"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ricompletedate" class="control-label fai-label-color">RI COMPLETE DATE :</label>
                    <div class="fai-input">
                        <input type="text" autocomplete="off" maxlength="100" name="ricompletedate" id="ricompletedate" class="level1 form-control fai-input-color input-date"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="company1" class="control-label fai-label-color">COMPANY :</label>
                    <div class="fai-input">
                       <select class="level1 form-control fai-input-color selectpicker" id="company1" name="company1"
                       data-size="5" data-live-search="true"  data-selected-text-format="count > 2">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="project" class="control-label fai-label-color">PROJECT :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="project" id="project" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="buyer" class="control-label fai-label-color">BUYER :</label>
                    <div class="fai-input">
                        <select class="level1 form-control fai-input-color selectpicker" id="buyer" name="buyer"
                        	data-size="5" data-live-search="true"  data-selected-text-format="count > 2">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="projectengineer" class="control-label fai-label-color">**PROJ ENG / Requestor :</label>
                    <div class="fai-input">
                       <select class="level1 form-control fai-input-color selectpicker" id="projectengineer"  name="projectengineer"
                       		data-size="5" data-live-search="true"  data-selected-text-format="count > 2" multiple>
                       </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="po" class="control-label fai-label-color">PO ## :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="po" id="po" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rmonumber" class="control-label fai-label-color">RMO NUMBER :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="rmonumber" id="rmonumber" class="level1 form-control fai-input-color numeric"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="itemnumber" class="control-label fai-label-color">ITEM NUMBER :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="itemnumber" id="itemnumber" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="revision" class="control-label fai-label-color">REVISION :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="revision" id="revision" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="control-label fai-label-color">DESCRIPTION :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="description" id="description" class="level1 form-control fai-input-color noEnterKey"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lotqtyrecd" class="control-label fai-label-color">LOT QTY RECEIVED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="lotqtyrecd" id="lotqtyrecd" class="level1 form-control fai-input-color numeric"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="riqtyinspected" class="control-label fai-label-color">QTY INSPECTED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="riqtyinspected" id="riqtyinspected" class="level1 form-control fai-input-color numeric"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="distributormanufacturer" class="control-label fai-label-color">DIST / MFG :</label>
                    <div class="fai-input">
                         <select class="level1 form-control fai-input-color selectpicker" id="distributormanufacturer" name="distributormanufacturer"
                        	data-size="5" data-live-search="true"  data-selected-text-format="count > 2">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="riqtyrejected" class="control-label fai-label-color">RI QTY REJECTED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="50" name="riqtyrejected" id="riqtyrejected" class="level1 form-control fai-input-color numeric"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="reasonforfai" class="control-label fai-label-color">REASON FOR FAI :</label>
                    <div class="fai-input">
                      <select class="level1 form-control red-border fai-input-color" id="reasonforfai" name="reasonforfai">
                           <option value=""></option>
                           <option value="Validate CAPA">VALIDATE CAPA</option>
                           <option value="QUALITY ISSUE">QUALITY ISSUE</option>
                           <option value="NEW REV">NEW REV</option>
                           <option value="PRIOR FAI REJECTION - DELTA">PRIOR FAI REJECTION - DELTA</option>
                           <option value="NEW SUPPLIER">NEW SUPPLIER</option>
                           <option value="NEW PART NUMBER">NEW PART NUMBER</option>
                           <option value="NEW SUPPLIER & PART NUMBER">NEW SUPPLIER & PART NUMBER</option>
                           <option value="ENGINEERING REV CONVERSION">ENGINEERING REV CONVERSION</option>n>
                           <option value="ENGINEERING VERIFICATION">ENGINEERING VERIFICATION</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="faitype" class="control-label fai-label-color">FAI TYPE :</label>
                    <div class="fai-input">
                      <select class="level1 form-control red-border fai-input-color" id="faitype" name="faitype">
                            <option value=""></option>
                            <option value="PCB">PCB</option>
                            <option value="OTHER">OTHER</option>
                            <option value="METAL FAB">METAL FAB</option>
                            <option value="CABLE ASSY">CABLE ASSY</option>
                            <option value="LABEL OR MANUALS">LABEL OR MANUALS</option>
                            <option value="VOIDED">VOIDED</option>
                            <option value="KEYPADS">KEYPADS</option>
                            <option value="ENGINEERING VERIFICATION">ENGINEERING VERIFICATION</option>
                            <option value="GRAPHIC APPROVAL">GRAPHIC APPROVAL</option>
                            <option value="PLASTICS">PLASTICS</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="failevel" class="control-label fai-label-color">FAI LEVEL :</label>
                    <div class="fai-input">
                      <select class="level1 form-control red-border fai-input-color" id="failevel" name="failevel">
                            <option value=""></option>
                            <option value="LEVEL 1 (INSPECT ONLY)">LEVEL 1 (INSPECT ONLY)</option>
                            <option value="LEVEL 2 (SMT / THA)">LEVEL 2 (SMT / THA)</option>
                            <option value="LEVEL 3 ( MANF ASSY & TEST)">LEVEL 3 ( MANF ASSY & TEST)</option>
                            <option value="LEVEL 4 (QA TEST - ENVIRONMENT)">LEVEL 4 (QA TEST - ENVIRONMENT)</option>
                            <option value="LEVEL 5 (FIELD TEST)">LEVEL 5 (FIELD TEST)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pilot" class="control-label fai-label-color">PILOT? :</label>
                    <div class="fai-input">
                        <input type="checkbox" name="pilot" id="pilot" class="level1 form-control fai-input-color"/>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom:0px;">
                    <label for="ripassfail" class="control-label fai-label-color">RI PASS / FAIL :</label>
                    <div class="fai-input">
                     <select class="level1 form-control red-border fai-input-color" id="ripassfail" name="ripassfail">
                            <option value=""></option>
                            <option value="PASS">PASS</option>
                            <option value="FAIL">FAIL</option>
                        </select>
                    </div>
                </div>
                
            </div>

            <div class="form-col fai-right-cols  col-xs-6">
                <div class="form-group" style="margin-bottom: 0px;">
                    <div class="upper-right" style="height:175px">
                        <!---<div id="dragandrophandler">Drag & Drop Attachments Here</div><div id="status1"></div>--->
                     	<label for="needdate" class="control-label" style="padding-top: 0px;">DATE NEEDED :</label>
                        <input type="text" autocomplete="off" maxlength="100" name="needdate" id="needdate" style="width:100px;height: 28px;display: inline-block;" 
                        	class="level1 form-control fai-input-color input-date"/>
                            <label for="suppliereport" class="control-label" style="padding-top: 0px; */">SUPPLIER FAI REPORT (Yes) :</label>
                            <input id="supplierreport" name="supplierreport" type="checkbox" class="level1" style="height:25px;width:25px;vertical-align:bottom;">
                            <label for="rohscert" class="control-label" style="padding-top: 0px; */">ROHS Cert (Yes) :</label>
                            <input id="rohscert" name="rohscert" type="checkbox" class="level1" style="height:25px;width:25px;vertical-align:bottom;">
     					<div style="padding-top:3px;">
                            <label for="faitype" class="control-label ">ATTACHMENTS :</label>
                            <div id="fileuploader_new_fai" style="display:none;">You must save the FAI before you can upload attachments.</div>
                            <div id="fileuploader" class="level1-hide" style="display:inline-block !important;">Upload</div>
                            <div>
                                <div style="border:solid 1px ##c1bbbb;height:100px;overflow:auto;">
                                    <ul id="attachment_list" style="padding:0;">
                                        
                                   </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="form-group" style="margin-bottom: 0px;">
                	<div align="center" style="text-align:center;background-color:##7f7f7f; color:##fff;">INSPECTION INSTRUCTION DETAILS</div>               	           
                </div>
                <div class="form-group form-group-inspectionresults" style="margin-bottom:0px;">
                	<textarea id="additionalinspectioninstructions" class="level1-textarea additionalinstructions fai-input-color" name="additionalinspectioninstructions" style="overflow-y: auto"> 
                    </textarea>            
                </div>                
                
                
                
                <div class="form-group red-font" style="font-size:10px;">
                	<span class="footmark" style="position:absolute; left:-15px;">** ENTER PROJECT ENGINEER NAME FOR ENGINEERING EVALUATION REQUESTS</span>
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                	<div align="center" style="text-align:center;background-color:##7f7f7f; color:##fff;">INSPECTION RESULTS</div>               	           
                </div>
                <div class="form-group form-group-inspectionresults" style="margin-bottom:0px;">
                	<textarea id="rievalutionresults" class="level1-textarea inspectionresults fai-input-color" name="rievalutionresults" style="overflow-y: auto"> 
                    </textarea>            
                </div>
            </div>
        </div>

		<div style="level2-tmp min-height: 100%;position:relative; display:block;">
			<cfinclude template="./_tmp_smt_tha_results.cfm">
        </div>
        <div style="level3-tmp min-height: 100%;position:relative; display:block;">
			<cfinclude template="./_tmp_mfg_test_results.cfm">
		</div>
        <div style="level4-tmp min-height: 100%;position:relative; display:block;">
			<cfinclude template="./_tmp_qa_test_lab_results.cfm">
		</div>
        <div style="level5-tmp min-height: 100%;position:relative; display:block;">
			<cfinclude template="./_tmp_eng.cfm">
		</div>
        <div style="level6-tmp min-height: 100%;position:relative; display:block;">
			<cfinclude template="./_tmp_first_article_results.cfm">
		</div>

	</form>
</div>
<cfinclude template="./_tmp__email_form.cfm">
</cfoutput>




