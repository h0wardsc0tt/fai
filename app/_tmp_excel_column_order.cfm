<div id="excelOrder" class="modal fade " data-backdrop="static" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Excel Column Order</h4>
                </div>
                <div class="modal-body text-center">
                <div class="excel_sort_container">
                      <ul id="excel_sortable">
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>ADDRESS</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>ALTPHONE</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>CITY</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>COMMENT</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>COMPANY</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
<!--                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>DATECREATED</div> <input type="text">&nbsp;&nbsp;&nbsp;<input type="checkbox"></li>-->
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>EMAIL</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input  class="excel-ck"type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>ENTERED DATE</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp; <input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>FAX</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>FIRSTNAME</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>INTEREST</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp; <input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>IP</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>LASTNAME</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>LOCATION</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp; <input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>NAME</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input  class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>ORIGIN</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>PHONE</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>STATE</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                         <li class="ui-state-default"><i class="fa fa-sort"></i><div>STORENUMBER</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp; <input class="excel-ck" type="checkbox"></li>
                         <li class="ui-state-default"><i class="fa fa-sort"></i><div>TITLE</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                         <li class="ui-state-default"><i class="fa fa-sort"></i><div>TYPE</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>UID</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp; <input class="excel-ck" type="checkbox" checked></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>USERAGENT</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>WEBSITE</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input class="excel-ck" type="checkbox"></li>
                          <li class="ui-state-default"><i class="fa fa-sort"></i><div>ZIP</div> <input class="excel-text" type="text">&nbsp;&nbsp;&nbsp;<input type="checkbox" checked></li>

                     </ul>
                     </div>
                </div>
                <div class="modal-footer">
                    <div class="col-lg-4 no-pad" style="text-align:left; display:none;">
                		Include Header : <input type="radio" name="excel-header" value="yes" checked> yes <input type="radio" name="excel-header" value="no"> no
                    </div> 

                	<span id="excel_save" style="padding-right:50px; color:#EF0F13;"></span>
                    <button id="excel_save_btn" type="button" class="btn btn-primary" style="display:none;" onclick="excel_order($('#excel_sortable'), 'Saved');">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>