<!--- Set Defaults --->
<cfoutput>
	<div class="fai-header" align="center">(FAI) FIRST ARTICLE INSPECTION - SEARCH SCREEN</div>

    <div id="fai_search_spinner" class="col-lg-12">
    	<h2 style="padding:0; margin:0;">Select FAIs</h2>
    </div>    
	<!--- Customer Search Form --->
    <form action="" method="POST" id="fai_Search_Form" class="form-horizontal col-lg-12" role="form" onsubmit="return fai_search_submit()">
    	<div class="search_form_fields row">
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="s_revision" class="control-label col-xs-5">Revision:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_revision" id="s_revision" class="form-control s-form"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_inspector" class="control-label col-xs-5">Inspector:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_inspector" id="s_inspector" class="form-control s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_description" class="control-label col-xs-5">Description:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_description" id="s_description" class="form-control s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_finalresults" class="control-label col-xs-5">Final Results:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_finalresults" id="s_finalresults" class="form-control s-form" />
                    </div>
                </div>
                <div class="form-group">
                        <label for="s_project" class="control-label col-xs-5">Project:</label>
                        <div class="col-xs-7">
                            <input type="text" maxlength="100" name="s_project" id="s_project" class="form-control s-form" />
                        </div>
                    </div>
                </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="s_itemnumber" class="control-label col-xs-5">Part ##:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_itemnumber" id="s_itemnumber" class="form-control s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_datecompleted_from" class="control-label col-xs-5">Finial Results From:<div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" autocomplete="off" maxlength="100" name="s_datecompleted_from" id="s_datecompleted_from" class="form-control cal-field s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_needdate_from" class="control-label col-xs-5">Needed From:<div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" autocomplete="off" maxlength="100" name="s_needdate_from" id="s_needdate_from" class="form-control cal-field s-form" value="#FORM.Lead_Email#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_ricompletedate_from" class="control-label col-xs-5">RI Comp From:<div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" autocomplete="off" maxlength="100" name="s_ricompletedate_from" id="s_ricompletedate_from" class="form-control cal-field s-form" />
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="form-group">
                    <label for="s_distributormanufacturer" class="control-label col-xs-5">Dist / MFG:</label>
                    <div class="col-xs-7">
                        <input type="text" maxlength="100" name="s_distributormanufacturer" id="s_distributormanufacturer" class="form-control s-form" value="#FORM.Lead_StoreNumber#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_datecompleted_to" class="control-label col-xs-5">Finial Results To:<div class="cal-cont s-form"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" autocomplete="off" maxlength="100" name="s_datecompleted_to" id="s_datecompleted_to" class="form-control cal-field s-form" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_needdate_to" class="control-label col-xs-5">Needed To:<div class="cal-cont s-form"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" autocomplete="off" maxlength="100" name="s_needdate_to" id="s_needdate_to" class="form-control cal-field s-form" value="#FORM.Lead_Interest#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="s_ricompletedate_to" class="control-label col-xs-5">RI Comp To:<div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-xs-7">
                        <input type="text" autocomplete="off" maxlength="100" name="s_ricompletedate_to" id="s_ricompletedate_to" class="form-control cal-field s-form" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 no-pad-search" style="margin-top:0px;">
                    	<div style="display:inline-block;">
                        	<input type="submit" name="Cust_Search_Submit" id="Cust_Search_Submit" class="btn btn-primary form-control" value="Search" />
                        </div>
						<div style="display:inline-block;">
                        	<button type="submit" name="search_reset" id="search_reset" onclick="reset_form();return false;" class="btn btn-primary form-control" style="width:80px;">Reset</button>
                         </div>
						<input type="hidden" name="Sort_Column" id="Sort_Column"  value="#VARIABLES.Sort_Column#"/>
						<input type="hidden" name="Sort_Order" id="Sort_Order"  value="#VARIABLES.Sort_Order#"/>
                   </div>
                </div>
            </div>
        </div>
            
        <!--- Customer Search Results --->
        <div class="col-lg-12 table-responsive no-pad">
            <h4><span  id="search_results_type">#VARIABLES.lookType#</span> <i id="filter_excel_export" style="color:##1901F0;" class="fa fa-file-excel-o"></i> <i  id="filter_pdf_export"  style="color: ##F50E12; display:none;" class="fa fa-file-pdf-o"></i></h4>
            <table id="Cust_Results" class="table " style="white-space:nowrap;margin:0px;">
                <thead id="Cust_Results_Header">
                    <tr>                  
                        <th style="width:15%;"><a id="h_id" href="javascript:void(0);" class="sortable sort"><span class="header_title">FAI ##</span></a></th>
                        <th style="width:19%;"><a id="h_description" href="javascript:void(0);" class="sortable sort"><span class="header_title">Description</span></a></th>
                        <th style="width:19%;"><a id="h_finalresults" href="javascript:void(0);" class="sortable sort"><span class="header_title">Final Results</span></a></th>
                        <th style="width:10%;"><a id="h_distributormanufacturer" href="javascript:void(0);" class="sortable sort"><span class="header_title">Dist / MFG</span></a></th>
                        <th style="width:10%;"><a id="h_itemnumber" href="javascript:void(0);" class="sortable sort"><span class="header_title">Part ##</span></a></th>
                        <th style="width:10%;"><a id="h_revision" href="javascript:void(0);" class="sortable sort"><span class="header_title">Revision</span></a></th>
                        <th style="width:10%;"><a id="h_ricompletedate" href="javascript:void(0);" class="sortable sort"><span class="header_title">RI Comp Date</span></a></th>
                        <th style="width:10%;"><a id="h_needdate" href="javascript:void(0);" class="sortable sort"><span class="header_title">Date Needed</span></a></th>
                        <th style="width:4%;"><a id="h_datecompleted" href="javascript:void(0);" class="sortable sort"><span class="header_title">Final Results Comp.</span></a></th>
                        <th style="width:12%;"><a id="h_reasonforfai" href="javascript:void(0);" class="sortable sort"><span class="header_title">Reason for FAI</span></a></th>
                    </tr>
                </thead>
                <tbody>
                    <cfloop from="1" to="#ArrayLen(returnSelectResult.DATA.id)#" index="cust">
                    	<cfset CustomerCount = #returnSelectResult.DATA.count[cust]#>
                        <tr>
                            <td><a href="javascript:void(0);" onclick="show_fai_from_filter('#returnSelectResult.DATA.id[cust]#');">#returnSelectResult.DATA.id[cust]#</a></td>
                            <td>#returnSelectResult.DATA.description[cust]#</td>
                            <td>#returnSelectResult.DATA.finalresults[cust]#</td>
                            <td col="supplier">#returnSelectResult.DATA.distributormanufacturer[cust]#</td>
                            <td>#returnSelectResult.DATA.itemnumber[cust]#</td>
                            <td>#returnSelectResult.DATA.revision[cust]#</td>
                            <td>#DateFormat(returnSelectResult.DATA.ricompletedate[cust],"mm/dd/yyyy")#</td>
                            <td>#DateFormat(returnSelectResult.DATA.needdate[cust],"mm/dd/yyyy")#</td>
                            <td>#DateFormat(returnSelectResult.DATA.datecompleted[cust],"mm/dd/yyyy")#</td>
                            <td>#returnSelectResult.DATA.reasonforfai[cust]#</td>
                        </tr>
                    </cfloop>
                </tbody>
            </table>
           
            <div class="col-xs-12 no-pad csp-paginate-cont">
                <div class="col-xs-6 no-pad">
                    <div class="col-xs-1 no-pad">
                        <button id="CSP_Prev_Page" name="CSP_Prev_Page" class="btn btn-primary btn-left" <cfif VARIABLES.Page_Start EQ 1>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-left"></span></button>
                    </div>
                    <div class="col-xs-11 text-left">
                        <div class="csp-perpage">
                            <span>
                                Show
                                <select id="CSP_Per_Page" name="CSP_Per_Page">
                                    <option value="5" <cfif FORM.CSP_Per_Page EQ 5>selected="selected"</cfif>>5</option>
                                    <option value="10" <cfif FORM.CSP_Per_Page EQ 10>selected="selected"</cfif>>10</option>
                                    <option value="20" <cfif FORM.CSP_Per_Page EQ 20>selected="selected"</cfif>>20</option>
                                </select>
                                items per page
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-pad">
                    <div class="col-xs-11 text-right"><div class="csp-results">Showing <span class="csp-page-start">#VARIABLES.Page_Start#</span> - <span class="csp-page-end">#VARIABLES.Page_End#</span> of (<span class="total_leads">#CustomerCount#</span>) Results</div></div>
                    <div class="col-xs-1 no-pad">
                        <button id="CSP_Next_Page" name="CSP_Next_Page" class="btn btn-primary btn-right pull-right" <cfif VARIABLES.Page_End GTE CustomerCount>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-right"></span></button>
                    </div>
                </div>
                <input type="hidden" id="CSP_Current_Page" name="CSP_Current_Page" value="#FORM.CSP_Current_Page#" />
            </div>
        </div>
    </form>
    
    <form name="excelExport" id="excelExport" method="post" action="./excel.cfm" target="excel_target">
		<input type="hidden" name="lead_filter" id="lead_filter" />
		<input type="hidden" name="excel_columns" id="excel_columns"/>		
		<input type="hidden" name="excel_leads_excluded" id="excel_leads_excluded"/>		
		<input type="hidden" name="show_excel_header" id="show_excel_header" value="yes"/>		
    </form> 
    <iframe id="excel_target" name="excel_target" style="display:none;"></iframe> 
<script type="text/javascript">
var security = '#fai_security#';
<!---
var json_hme_users = JSON.parse(JSON.stringify(#hme_users_json#));
var json_hme_departments = JSON.parse(JSON.stringify(#hme_departments_json#));
var json_offices_json = JSON.parse(JSON.stringify(#hme_offices_json#));
--->
var json_dropdowns = JSON.parse(JSON.stringify(#dropDowns_json#));
var json = JSON.parse(JSON.stringify(#leads_json#));
var leadFilter = JSON.stringify(#leadFilter_json#);
var lead_count = #CustomerCount#;
var json_fai = {};
console.log(json_dropdowns);
console.log(json_dropdowns.tickets[0].value);

//console.log(json_hme_departments);
//console.log(json_offices_json);
</script>
</cfoutput>
<cfinclude template="./_tmp_search_form.cfm">

