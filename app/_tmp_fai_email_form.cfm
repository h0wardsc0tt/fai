
<div id="email_form" align="center" class="email-element email-form">
<div id="email_header" class="email_header">
	<div style="color:#fff; text-align:left;">
        <i style="font-size:23px;" class="fa fa-envelope"></i>
        <div style="display:inline-block; padding-left:10px; font-size:20px;">FAI Email</div>
    </div>
</div>


<div style="height:443px; width:700px; background-color:#fff; padding-top:10px;">
    <div style="display:inline-block; vertical-align:top;">
        <div onClick="send_message()" class="email-send-button email-send-out" 
            onMouseOut="$(this).removeClass('email-send-over').addClass('email-send-out');" 
            onMouseOver="this.style.cursor='pointer';$(this).removeClass('email-send-out').addClass('email-send-over');" 
            style="height:45px; width:45px; display:inline-block;text-align:center;font-size:12px;">
        	<div style="padding-top:15px;">Send</div>
        </div>
        <div style="display:inline-block; vertical-align:top;">
            <div style="height:20px; width:45px;border:solid 1px #B4AFAF;font-size:12px; margin-bottom:5px;margin-top:4px;">
                <div style="padding-top:2px;">To..</div></div>
            <div style="height:20px; width:45px;border:solid 1px #B4AFAF;font-size:12px; margin-bottom:5px;margin-top:8px;">
                <div style="padding-top:2px;">CC..</div></div>
            <div style="height:20px; width:45px;font-size:12px; margin-bottom:5px;text-align:center;">
                <div style="padding-top:4px;">Subject</div></div>
            <div style="height:20px; width:45px;border:solid 1px #B4AFAF;font-size:12px; margin-top:18px;">
                <div style="padding-top:2px;">Attach</div></div>
        </div>
    </div>
    <div style="display:inline-block;">
      	<div><select id="email_to" class="form-control email-to email-input-elm selectpicker"  style="height:22px; width:600px;margin-bottom:5px;"
                         data-size="5" data-live-search="true"  data-selected-text-format="count > 2" multiple>
             </select></div>
     	<div><select id="email_cc" class="form-control email-cc email-input-elm selectpicker"  style="height:42px; width:600px;margin-bottom:5px;"
                         data-size="5" data-live-search="true"  data-selected-text-format="count > 2" multiple>
             </select></div>
        <div><textarea class="email-subject email-input-elm" style="padding:0;"></textarea></div>
    	<div><select id="email_attachement_input" class="form-control email-attachments email-input-elm selectpicker"  style="height:22px; width:600px;margin-bottom:5px;"
                         data-size="5" data-live-search="true"  data-selected-text-format="count > 1" multiple>
             </select></div>
    </div>
    <div class="email-validatrion-error">
        <div class="expandable-element" style="height: 0px;">
            <div style="border:solid 1px #ff0000;color:#1910EF">
            	<div class="email_close_btn" onMouseOver="this.style.cursor='pointer';"><i title="close" style="color:#dddddd" onClick="close_email_validation();" class="fa fa-times"></i></div>
                <div style="padding:10px 5px 10px 5px; text-align:left;" class="error-message">You must provide a security question answer</div>
             </div>
        </div>
    </div>
  
	<textarea type="text" id="email_textarea" class="email-textarea"></textarea>
</div>
<div class="email-footer-buttons"> 
	<button onClick="$('.email-element').hide();" type="button" class="btn btn-default">Cancel</button>
</div>
</div>



</div>
<div class="email-element email-blackout"></div>


 

