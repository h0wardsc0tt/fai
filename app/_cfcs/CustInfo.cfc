<cfcomponent>    
	<cffunction name="LeadsSelect" output="false" returntype="any" access="public" description="Select a lead or filtered list">
        <cfargument type="any" name="id" dbvarname="@id" required="no">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="no">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="no">
        <cfargument type="string" name="s_company1" dbvarname="@s_company1" required="no">
        <cfargument type="string" name="s_description" dbvarname="@s_description" required="no">
        <cfargument type="string" name="distributormanufacturer" dbvarname="@distributormanufacturer" required="no">
        <cfargument type="string" name="s_inspector" dbvarname="@s_inspector" required="no">
        <cfargument type="string" name="s_itemnumber" dbvarname="@s_itemnumber" required="no">
        <cfargument type="string" name="s_needdate_from" dbvarname="@s_needdate_from" required="no">
        <cfargument type="string" name="s_needdate_to" dbvarname="@s_needdate_to" required="no">
        <cfargument type="string" name="s_po" dbvarname="@s_po" required="no">
        <cfargument type="string" name="s_project" dbvarname="@s_project" required="no">
        <cfargument type="string" name="s_ricompletedate_from" dbvarname="@s_ricompletedate_from" required="no">
        <cfargument type="string" name="s_ricompletedate_to" dbvarname="@s_ricompletedate_to" required="no">
        <cfargument type="string" name="s_rmonumber" dbvarname="@s_rmonumber" required="no">
        <cfargument type="string" name="sortColumn" dbvarname="@sortColumn" required="no">
        <cfargument type="string" name="sortOrder" dbvarname="@sortOrder" required="no">
                                
        <cfset results = "OK">
		<cftry>
 			<cfstoredproc procedure="#storedProcedure#" datasource="#APPLICATION.FAI_Datasource#" >  
            
            	<cfif isDefined("Arguments.id")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
               	<cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.pageNumber")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.pageNumber#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>

				<cfif isDefined("Arguments.itemsPerPage")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.itemsPerPage#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>
               
                <cfif isDefined("Arguments.s_company1")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="#NOT len(trim(Arguments.s_company1))#"
                        value="#Arguments.s_company1#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_description")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_description#"
                        null="#NOT len(trim(Arguments.s_description))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_distributormanufacturer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_distributormanufacturer#"
                        null="#NOT len(trim(Arguments.s_distributormanufacturer))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_inspector")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_inspector#"
                        null="#NOT len(trim(Arguments.s_inspector))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_itemnumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_itemnumber#"
                        null="#NOT len(trim(Arguments.s_itemnumber))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_needdate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_needdate_from#"
                        null="#NOT len(trim(Arguments.s_needdate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_needdate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_needdate_to#"
                        null="#NOT len(trim(Arguments.s_needdate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_po")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_po#"
                        null="#NOT len(trim(Arguments.s_po))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_project")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_project#"
                        null="#NOT len(trim(Arguments.s_project))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_ricompletedate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_ricompletedate_from#"
                        null="#NOT len(trim(Arguments.s_ricompletedate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_ricompletedate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_ricompletedate_to#"
                        null="#NOT len(trim(Arguments.s_ricompletedate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_rmonumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_rmonumber#"
                        null="#NOT len(trim(Arguments.s_rmonumber))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

            	<cfprocresult name="qry_getSelect">     
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
        <cfreturn qry_getSelect />
	</cffunction>      
    
</cfcomponent>