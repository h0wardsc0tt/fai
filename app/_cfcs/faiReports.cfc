<cfcomponent> 

    <cffunction name="engineering_wip_report" output="false" returntype="query" access="public">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="qryengineering_wip" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocresult name="qry_engineering_wip">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_engineering_wip />
    </cffunction>

    <cffunction name="awaiting_qe_report" output="false" returntype="query" access="public">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="qryAwaitingQEs" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocresult name="qry_awaiting_qe">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_awaiting_qe />
    </cffunction>

    <cffunction name="closed_the_day_before_report" output="false" returntype="query" access="public">
        <cfargument type="string" name="fromdate" required="yes">
        <cfargument type="string" name="todate"  required="Yes">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="qryClosedDayBefore" datasource="#APPLICATION.FAI_Datasource#" >         
               <cfprocparam
                    cfsqltype="cf_sql_char"
                    value="#Arguments.fromdate#">
              <cfprocparam
                    cfsqltype="cf_sql_char"
                    value="#Arguments.todate#">
               <cfprocresult name="qry_closed_the_day_before_report">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_closed_the_day_before_report />
    </cffunction>

     <cffunction name="completed_by_ri_date_report" output="false" returntype="query" access="public">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="ARCHIVEqryRIInspectionComplete" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocresult name="qry_completed_by_ri_date_report">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_completed_by_ri_date_report />
    </cffunction>

    <cffunction name="wip_report" output="false" returntype="query" access="public">
        <cfargument type="string" name="pageNumber" required="yes">
        <cfargument type="string" name="itemsPerPage"  required="Yes">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="qryInWork" datasource="#APPLICATION.FAI_Datasource#" >         
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value="#Arguments.pageNumber#">
              <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value="#Arguments.itemsPerPage#">
               <cfprocresult name="qry_getWIP">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_getWIP />
    </cffunction>

     <cffunction name="report_Attachment" output="false" returntype="query" access="public" description="Return binary file">
        <cfargument type="string" name="id" required="yes">
        <cfargument type="string" name="reporttype"  required="Yes">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="getActiveAttachmentList" datasource="#APPLICATION.FAI_Datasource#" >         
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value="#Arguments.id#">
               <cfprocresult name="qry_getAttachments">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_getAttachments />
    </cffunction>

	<cffunction name="GenerateReport" returntype="any" output="false"  access="public">
        <cfargument type="string" name="id" required="no">
        <cfargument type="string" name="reporttype"  required="no">
        <cfargument type="string" name="fromdate"  required="no">
        <cfargument type="string" name="todate"  required="no">        

        <cfset subject = "">
        <cfset emailcontent = "">
        <cfset to_addrs = "">
        <cfset cc_addrs = "">
        <cfset results = "OK">
        <cfif Arguments.reporttype eq "completion_notification_report"> 
			<cftry>
				<cfstoredproc procedure="qrySearchByFAINo" datasource="#APPLICATION.FAI_Datasource#" >         
				   <cfprocparam
						cfsqltype="cf_sql_integer"
						value="#Arguments.id#">
				   <cfprocresult name="qry_searchByFAINo">  
				</cfstoredproc>
				<cfcatch type="Database">    
					<cfset results = "#cfcatch.detail#"> 
				</cfcatch>
			</cftry>
         </cfif>
         
         <cfif results eq "OK">
        	<cfif Arguments.reporttype eq "closed_the_day_before_report"> 
            	<cfinclude template="..\_dat_closed_the_day_before_report2.cfm">            
         	</cfif>
        	<cfif Arguments.reporttype eq "completion_notification_report"> 
            	<cfinclude template="..\_dat_fai_completion_notification_report2.cfm">            
         	</cfif>
        	<cfif Arguments.reporttype eq "work_in_progress_report"> 
            	<cfinclude template="..\_dat_fai_work_in_progress_report2.cfm">            
         	</cfif>
        	<cfif Arguments.reporttype eq "completed_by_ri_date_report"> 
            	<cfinclude template="..\_dat_completed_by_ri_date_report2.cfm">            
         	</cfif>
         	<cfif Arguments.reporttype eq "awaiting_qe_report"> 
            	<cfinclude template="..\_dat_awaiting_qe_report2.cfm">            
         	</cfif>
         	<cfif Arguments.reporttype eq "engineering_wip_report"> 
            	<cfinclude template="..\_dat_engineering_wip_report2.cfm">            
         	</cfif>
        </cfif>
         
         <cfscript>
		 	returnResult = StructNew();
			returnResult['subject'] = "#subject#";
			returnResult['emailcontent'] = "#emailcontent#";
			returnResult['to'] = "#to_addrs#";
			returnResult['cc'] = "#cc_addrs#";
			returnResult['results'] = results;
		 </cfscript>
        <cfreturn returnResult />
    </cffunction>   

                
</cfcomponent>