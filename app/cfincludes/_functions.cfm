<cfscript> 
function deqGuid(guid) { 
    var guidArray = guid.Split("-");
	var modifiedGuid = guidArray[1] & "-" & guidArray[4] & "-" & guidArray[2] & "-" & guidArray[3]& "-" & guidArray[5];	
	return modifiedGuid;
}  
</cfscript> 

<cfscript> 
function enqGuid(guid) { 
    var guidArray = guid.Split("-");
	var modifiedGuid = guidArray[1] & "-" & guidArray[3] & "-" & guidArray[4] & "-" & guidArray[2] & "-" & guidArray[5];   	
	return modifiedGuid;
}  
</cfscript>

<cfscript>
function getdbComponent() { 
	return CreateObject("component", "_cfcs.fai");
}

function getContentType(prefix) { 
	var content_type = ''; 
	switch(prefix){
		case '.pdf' :
			content_type = 'application/pdf';
			break;
    	case '.jpg':  
         	content_type = 'image/jpg';
			break;
    	case '.tif':  
        	content_type = 'image/tif; cht=iso-8859-1';
			break;
     	case '.xlsx': 
        	content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
			break;
    	case '.xls': 
        	content_type = 'application/vnd.ms-excel';
			break;
    	case '.xlsm':  
        	content_type = 'application/vnd.ms-excel.sheet.macroEnabled.12';
			break;
    	case 'xla':
         	content_type = 'application/vnd.ms-excel';
			break;
   	 	case '.xltx':  
         	content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.template';
			break;
    	case '.doc':  
         	content_type = 'application/msword';
			break;
    	case '.docx': 
         	content_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
			break;
   		case '.msg':  
         	content_type = 'application/vnd.ms-outlook';    
			break;
   		case '.mht': 
         	content_type = 'application/octet-stream';    
			break;
    	case '.rtf':
        	content_type = 'application/msword';
 			break;
    	case '.txt': 
       		content_type = 'text/plain';
			break;
    	case '.gif': 
        	content_type = 'image/gif';
			break;
    	default :
        	content_type = 'application/octet-stream';
			break;
	}
	
	return content_type;
}

</cfscript>


