<cfparam name="Page_MetaTitle" default="">
<cfparam name="Page_MetaKeywords" default="">
<cfparam name="Page_MetaDescription" default="">

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><cfoutput>#Page_MetaTitle#</cfoutput></title>
        <meta name="Title" content="<cfoutput>#Page_MetaTitle#</cfoutput>">
        <meta name="Description" content="<cfoutput>#Page_MetaDescription#</cfoutput>">
        <meta name="Keywords" content="<cfoutput>#Page_MetaKeywords#</cfoutput>">
        <meta name="format-detection" content="telephone=no">
        <link rel="icon" href="favicon.ico?v=4">
        <!-- build:css ./css/fai-styles.min.css -->
        <link rel="stylesheet" href="./css/3.3.5_bootstrap.min.css">
        <link rel="stylesheet" href="./css/4.3.0_font-awesome.min.css">
		<link rel="stylesheet" href="./css/1.12.1_themes_base_jquery-ui.css">
        <link rel="stylesheet" href="./css/1.11.4_themes_smoothness_jquery-ui.css">
        <link rel="stylesheet" href="./css/bootstrap-select.min.css">
        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/animate.css">
        <link rel="stylesheet" href="./css/dragDropUpload.css">
        <link rel="stylesheet" href="./css/uploadfile.css">
        <link rel="stylesheet" href="./css/jquery.multiselect.css">
        <link rel="stylesheet" href="./css/fai__email.css">
        <link rel="stylesheet" href="./css/fai-media.css">
         <!-- endbuild -->
        <link rel="stylesheet" href="./css/fai.css">


    </head>
    <body>

        