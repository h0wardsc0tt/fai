<cfsetting showdebugoutput="no">
<cfinclude template="./serverSettings.cfm">
<cfparam name="FORM.file" type="string" default="" />
<cfparam name="FORM.id" type="string" default="" />

<cfif Len(FORM.file)>
	<cffile
		action="upload"
		filefield="file"
		destination="#ExpandPath( './' )#"
		nameconflict="makeunique"
		/>
	<!--- Read in the binary data. --->
	<cffile
		action="readbinary"
		file="#ExpandPath( './' )##CFFILE.ServerFile#"
		variable="binFile"
		/>
	
	<!--- Delete photo from server. --->
	<cffile
		action="delete"
		file="#ExpandPath( './' )##CFFILE.ServerFile#"
		/>
	<cfscript>	
		obj = CreateObject("component", "_cfcs.fai");
		ret = obj.insertAttachment
		(
			id = Form.id,
			file = binFile, 
			fileName = CFFILE.clientFileName & '.' & LCase(CFFILE.clientFileExt), 
			prefix = '.' & LCase(CFFILE.clientFileExt)
		);
		serializer = new lib.JsonSerializer();
		writeOutput(ret);	
	</cfscript>
</cfif>
