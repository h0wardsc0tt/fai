<cfsavecontent variable = "html">
<cfoutput> 
<div style="margin:0px 5px 0px 5px;">
	<p style="font-size:12px;">Good afternoon,</p>
	<p style="font-size:12px;">The list of items currently in First Article process is attached. PCB FAIs are also listed below.</p>
    <p style="font-size:12px;">If you have any questions or updates, please do not hesitate to contact me.</p>
   	<p style="font-size:12px;">Regards,</p>
	<p style="font-size:12px;">David Day<br />QA Inspection Leader<br />x4058</p>
 </div>
 </cfoutput>
</cfsavecontent>

<cfset emailcontent = html>
<cfset subject = 'FAI''s Currently In Work'>
<cfset to_addrs = "Materials@hme.com">
<cfset cc_addrs = "JGillespie@hme.com; tkent@hme.com; rfretz@hme.com; mhoward@hme.com; lpresutti@hme.com; jmorgan@hme.com; tjordan@hme.com; rgaboury@hme.com; blegge@hme.com; kmccardle@hme.com; triches@hme.com; mrimorin@hme.com; jenriquez@hme.com; GPapas@hme.com">

