USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[uploadAttachment]    Script Date: 11/22/2016 8:52:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uploadAttachment](
	@id int,
	@fileName nvarchar(250),
	@prefix NVARCHAR(10),
	@file IMAGE,
	@guid NVARCHAR(150) OUTPUT
) AS

SET  @guid = NEWID()

INSERT INTO fai_Attachments
(
	id, 
	[filename], 
	prefix, 
	[file], 
	guid
)
VALUES
(
	@id,
	@fileName,
	@prefix,
	@file,
	cast(@guid  as uniqueidentifier)
)

