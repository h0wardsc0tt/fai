USE [db_fai]
GO
/****** Object:  UserDefinedFunction [dbo].[daysincurrentlocation]    Script Date: 11/22/2016 8:53:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[daysincurrentlocation](@dateinqalab DATETIME, @dateinmfg DATETIME, @dateinqsmt DATETIME, @dateinqeng DATETIME, @datereceived DATETIME)
RETURNS int
AS
BEGIN
	DECLARE @currentDate DateTime
	SET @currentDate = GETDATE()
	DECLARE @daysinloc int

	SET @daysinloc = CASE 
		WHEN @dateinqalab IS NOT Null THEN DATEDIFF(DAY, @dateinqalab, @currentDate)
		ELSE
			CASE 
			WHEN @dateinmfg IS NOT Null THEN DATEDIFF(DAY, @dateinmfg, @currentDate)
			ELSE
				CASE 
				WHEN @dateinqsmt IS NOT Null THEN DATEDIFF(DAY, @dateinqsmt, @currentDate)
				ELSE
					CASE 
					WHEN @dateinqeng IS NOT Null THEN DATEDIFF(DAY, @dateinqeng, @currentDate)
					ELSE  DATEDIFF(DAY, @datereceived, @currentDate)
					END
				END
			END
	END
	RETURN  @daysinloc
END
