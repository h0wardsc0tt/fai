USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_enableDisableAttachment]    Script Date: 11/22/2016 8:44:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_enableDisableAttachment](
	@isActive bit,
	@guid NVARCHAR(50)
)AS
UPDATE dbo.fai_Attachments SET isActive = @isActive
WHERE guid = @guid