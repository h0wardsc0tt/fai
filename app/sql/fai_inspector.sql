USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_inspector]    Script Date: 11/22/2016 8:46:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_inspector]
AS
SELECT distinct 'inspector' AS [key], UPPER([inspector]) AS value
FROM tbl_FAI
WHERE [inspector] IS NOT NULL AND [inspector] != ''  AND [inspector] != 'VOID'
ORDER BY value

