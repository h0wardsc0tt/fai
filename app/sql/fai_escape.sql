USE [db_fai]
GO
/****** Object:  UserDefinedFunction [dbo].[fai_escape]    Script Date: 11/22/2016 8:53:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fai_escape](@param NVARCHAR(MAX))
RETURNS NVARCHAR(MAX)
AS
BEGIN
set @param =	replace( 
				replace( 
				replace( 
				replace( @param
				,    '\', '\\' )
				,    '%', '\%' )
				,    '_', '\_' )
				,    '[', '\[' )	
RETURN  @param
END
