USE [db_fai]
GO
/****** Object:  UserDefinedFunction [dbo].[ifNull255]    Script Date: 11/22/2016 8:54:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[ifNull255](@param NVARCHAR(255), @column NVARCHAR(255))
RETURNS NVARCHAR(255)
AS
BEGIN
	RETURN CASE WHEN @param IS NULL THEN @column ELSE CASE WHEN @param = '' THEN NULL ELSE @param END END
END
