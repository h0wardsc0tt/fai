USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_buyer]    Script Date: 11/22/2016 8:42:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_approvedby]
AS
SELECT DISTINCT 'buyer' AS [key], UPPER(buyer) AS [value] 
FROM tbl_fai
WHERE buyer IS NOT NULL AND buyer != ''