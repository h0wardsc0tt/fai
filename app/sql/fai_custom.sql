USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_custom]    Script Date: 12/1/2016 7:13:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[fai_custom](
	@pageNumber INT,
	@itemsPerPage INT,
	@fromDate NVARCHAR(50) = NULL,
	@toDate NVARCHAR(50) = NULL,
	@supplier NVARCHAR(50) = NULL,
	@custom NVARCHAR(50) = NULL,
	@sortcolumn NVARCHAR(50) = NULL,
	@sortorder NVARCHAR(5) = NULL,
	@datecompleted_from NVARCHAR(50) = NULL,
	@datecompleted_to NVARCHAR(50) = NULL,
	@needdate_from NVARCHAR(50) = NULL,
	@needdate_to NVARCHAR(50) = NULL,
	@ricompletedate_from NVARCHAR(50) = NULL,
	@ricompletedate_to NVARCHAR(50) = NULL, 
	@itemnumber NVARCHAR(256) = NULL,
	@distributormanufacturer  NVARCHAR(256) = NULL,
	@finalresults NVARCHAR(256) = NULL,
	@description  NVARCHAR(256) = NULL,
	@inspector NVARCHAR(256) = NULL,
	@revision  NVARCHAR(256) = NULL,
	@id INT = NULL
) AS

DECLARE
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@cnt INT,
	@recordSet INT,
	@currentDate DateTime,
	@culture nvarchar(10)

SET @culture = 'en-US'
SET @currentDate = GETDATE()

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' + CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END

SELECT @cnt = count(*) 
FROM tbl_fai
WHERE 1=1
 AND (@custom  != 'wip' OR [FAI STATUS]='IN WORK' AND [DATE RECEIVED] IS NOT NULL)
 AND (@custom  != 'fpy' OR [FAI TYPE] != 'ENGINEERING VERIFICATION'
	AND [FAI TYPE] != 'GRAPHIC APPROVAL' 
	AND [FAI TYPE] != 'VOID' 
	AND [FAI TYPE] != 'VOIDED' 
	AND [REASON FOR FAI] !='ENGINEERING REV CONVERSION' 
	AND [REASON FOR FAI] != 'FIELD TEST' 
	AND [DISTRIBUTOR/MANUFACTURER] LIKE '%' + @supplier + '%')
 AND (@custom != 'ewip' OR [FAI STATUS]='In Work' AND [DATE COMPLETED] IS NULL AND [DATE IN ENGINEERING] IS NOT NULL AND [CLOSURE COMMENTS] IS NULL)
 AND (@custom != 'rc' OR [RI COMPLETE DATE] IS NOT NULL AND [FAI STATUS]='IN WORK' AND [DATE COMPLETED] IS NOT NULL)
 AND (@custom != 'rinc' OR [RI COMPLETE DATE] IS NULL AND [FAI STATUS]='IN WORK')	
 AND (@custom != 'aqe' OR [RI PASS / FAIL] Is Not Null AND [RI COMPLETE DATE] Is Not Null AND [FAI STATUS]='IN WORK' AND [CLOSURE COMMENTS] Is Not Null)	
 AND (@custom != 'riic' OR [RI COMPLETE DATE] Is Not Null AND [FAI STATUS]='IN WORK')
 AND (@custom != 'cbrid' OR [RI COMPLETE DATE] Is Null AND [FAI STATUS]='IN WORK')
 AND (@custom != 'cdb' OR [FAI STATUS]='CLOSED' AND [DATE COMPLETED] >= @fromDate And [DATE COMPLETED] <= @toDate)
 AND (@custom != 'wipnonpcb' OR [FAI TYPE]<>'PCB' AND [FAI STATUS]='IN WORK')
 AND (@custom != 'wippcb' OR [FAI TYPE]='PCB' AND [FAI STATUS]='IN WORK')
 AND (@custom != 'search' OR (@datecompleted_from IS NULL OR  [DATE COMPLETED] >= @datecompleted_from)
	AND (@datecompleted_to IS NULL OR [DATE COMPLETED] <= @datecompleted_to)
	AND (@needdate_from IS NULL OR  [NEED DATE] >= @needdate_from)
	AND (@needdate_to IS NULL OR [NEED DATE] <= @needdate_to)
	AND (@ricompletedate_from IS NULL OR  [RI COMPLETE DATE] >= @ricompletedate_from)
	AND (@ricompletedate_to IS NULL OR [RI COMPLETE DATE] <= @ricompletedate_to)
	AND (@itemnumber IS NULL OR [ITEM NUMBER] LIKE '%' + @itemnumber + '%' )
	AND (@distributormanufacturer IS NULL OR [DISTRIBUTOR/MANUFACTURER] LIKE '%' + @distributormanufacturer + '%' )
	AND (@revision IS NULL OR [REVISION]  LIKE '%' + @revision + '%' )
	AND (@inspector IS NULL OR [INSPECTOR]  LIKE '%' + @inspector + '%')
	AND (@description IS NULL OR [DESCRIPTION] LIKE '%' + @description + '%')
	AND (@finalresults IS NULL OR [FINAL RESULTS] LIKE '%' + @finalresults + '%')
	AND (@id IS NULL OR [id] = @id))


SELECT 
	@cnt as count,
	dbo.inv([APPROVED BY]) AS approvedby,
	'[' + STUFF((SELECT  ',' + '{"guid":"'+ cast(guid as varchar(36)) + '","filename":"' + filename + '","isactive":"' + cast(isActive as varchar(1)) + '"}'
			FROM [dbo].[fai_Attachments]
			WHERE  id = fai.id
			FOR XML PATH('')), 1, 1, '') + ']' AS attachments,
	dbo.inv([BUYER]) AS buyer,
	dbo.inv([CLOSURE COMMENTS]) AS closurecomments,
	dbo.inv([COMMENTS]) AS comments,
	dbo.inv([COMPANY 1]) AS company1,
	dbo.inv(dbo.currentlocation([DATE IN QA LAB], [DATE IN MANUFACTURING], [DATE IN SMT], [DATE IN ENGINEERING])) AS currentlocation,
	--dbo.inv([CURRENT  LOCATION]) AS currentlocation,
	dbo.inv([CURRENT STATUS]) AS currentstatus,
	dbo.inv([CURRENTLY LOCATED AS OF]) AS currentlylocatedasof,
	dbo.inv([DATE CODE]) AS datecode,
	dbo.inv(FORMAT([DATE IN ENGINEERING],'d', @culture)) AS dateinengineering,
	dbo.inv(FORMAT([DATE IN QA LAB],'d', @culture)) AS dateinqalab,
	dbo.inv(FORMAT([DATE COMPLETED],'d', @culture)) AS datecompleted,
	dbo.inv(FORMAT([DATE IN MANUFACTURING],'d', @culture)) AS dateinmanufacturing,
	dbo.inv(FORMAT([DATE IN SMT],'d', @culture)) AS dateinsmt,
	dbo.inv(FORMAT([DATE RECEIVED],'d', @culture)) AS datereceived,
	dbo.inv(dbo.daysincurrentlocation([DATE IN QA LAB], [DATE IN MANUFACTURING], [DATE IN SMT], [DATE IN ENGINEERING], [DATE RECEIVED])) AS daysincurrentlocation,
	dbo.inv([DAYS TO COMPLETE]) AS daystocomplete,
	dbo.inv([DESCRIPTION]) AS description,
	dbo.inv([DISTRIBUTOR/MANUFACTURER]) AS distributormanufacturer,
	dbo.inv([DMR REF #]) AS dmrref,
	dbo.inv([ENG APPROVER]) AS engapprover,
	dbo.inv(FORMAT([ENGINEERING DATE  COMPLETE],'d', @culture)) AS engineeringdatecomplete,
	dbo.inv([ENGINEERING EMAIL]) AS engineeringemail,
	dbo.inv([ENGINEERING RESULTS]) AS engineeringresults,
	dbo.inv([FAI LEVEL]) AS failevel,
	dbo.inv([FAI STATUS]) AS faistatus,
	dbo.inv([FAI TYPE]) AS faitype,
	dbo.inv([FINAL RESULTS]) AS finalresults,
	dbo.inv([ID]) AS id,
	dbo.inv([INSPECTOR]) AS inspector,
	dbo.inv([ITEM NUMBER]) AS itemnumber,
	dbo.inv([LOT QTY RECD]) AS lotqtyrecd,
	dbo.inv([MANF PASS / FAIL]) AS manfpassfail,
	dbo.inv([MANF RESULTS]) AS manfresults,
	dbo.inv(FORMAT([MANUFACTURING COMPLETE DATE],'d', @culture)) AS manufacturingcompletedate,
	dbo.inv([MANUFACTURING NEW PN]) AS manufacturingnewpn,
	dbo.inv([MANUFACTURING QTY COMPLETED]) AS manufacturingqtycompleted,
	dbo.inv([MANUFACTURING QTY RECIEVED]) AS manufacturingqtyrecieved,
	dbo.inv(FORMAT([NEED DATE],'d', @culture)) AS needdate,
	dbo.inv([PILOT?]) AS pilot,
	dbo.inv([PO #]) AS po,
	dbo.inv([PROJECT]) AS project,
	dbo.inv([PROJECT ENGINEER]) AS projectengineer,
	dbo.inv(FORMAT([QA  DATE],'d', @culture)) AS qadate,
	dbo.inv(FORMAT([QA LAB DATE COMPLETE],'d', @culture)) AS qalabdatecomplete,
	dbo.inv([qa lab qty completed]) AS qalabqtycompleted,
	dbo.inv([qa lab qty received]) AS qalabqtyreceived,
	dbo.inv([QA TEST LAB  REPORT #]) AS qatestlabreport,
	dbo.inv([QA TEST LAB PASS / FAIL]) AS qatestlabpassfail,
	dbo.inv([REASON FOR FAI]) AS reasonforfai,
	dbo.inv([REVISION]) AS revision,
	dbo.inv(LTRIM([RI EVALUTION  - RESULTS])) AS rievalutionresults,
	dbo.inv([RI PASS / FAIL]) AS ripassfail,
	dbo.inv([RI QTY INSPECTED]) AS riqtyinspected,
	dbo.inv([RI QTY REJECTED]) AS riqtyrejected,
	dbo.inv(FORMAT([RI COMPLETE DATE],'d', @culture)) AS ricompletedate,
	dbo.inv([RMO NUMBER]) AS rmonumber,
	dbo.inv([SMT PASS / FAIL]) AS smtpassfail,
	dbo.inv([SMT RESULTS]) AS smtresults,
	dbo.inv(FORMAT([SMT COMPLETE DATE],'d', @culture)) AS smtcompletedate,
	dbo.inv([SMT NEW PN]) AS smtnewpn,
	dbo.inv([SMT QTY]) AS smtqty,
	dbo.inv([SMT QTY COMPLETED]) AS smtqtycompleted,
	dbo.inv([SupplierReport])  AS supplierreport,
	dbo.inv([THA RESULTS]) AS tharesults,
	dbo.inv([THA PASS])  AS thapassfail,
	dbo.inv(DATEDIFF(DAY, [DATE RECEIVED], @currentDate)) AS totaldaysinwork
FROM [dbo].[tbl_FAI] fai
WHERE 1=1
 AND (@custom  != 'wip' OR [FAI STATUS]='IN WORK' AND [DATE RECEIVED] IS NOT NULL)
 AND (@custom  != 'fpy' OR [FAI TYPE] != 'ENGINEERING VERIFICATION'
	AND [FAI TYPE] != 'GRAPHIC APPROVAL' 
	AND [FAI TYPE] != 'VOID' 
	AND [FAI TYPE] != 'VOIDED' 
	AND [REASON FOR FAI] !='ENGINEERING REV CONVERSION' 
	AND [REASON FOR FAI] != 'FIELD TEST' 
	AND [DISTRIBUTOR/MANUFACTURER] LIKE '%' + @supplier + '%')
 AND (@custom != 'ewip' OR [FAI STATUS]='In Work' AND [DATE COMPLETED] IS NULL AND [DATE IN ENGINEERING] IS NOT NULL AND [CLOSURE COMMENTS] IS NULL)
 AND (@custom != 'rc' OR [RI COMPLETE DATE] IS NOT NULL AND [FAI STATUS]='IN WORK' AND [DATE COMPLETED] IS NOT NULL)
 AND (@custom != 'rinc' OR [RI COMPLETE DATE] IS NULL AND [FAI STATUS]='IN WORK')	
 AND (@custom != 'aqe' OR [RI PASS / FAIL] Is Not Null AND [RI COMPLETE DATE] Is Not Null AND [FAI STATUS]='IN WORK' AND [CLOSURE COMMENTS] Is Not Null)	
 AND (@custom != 'riic' OR [RI COMPLETE DATE] Is Not Null AND [FAI STATUS]='IN WORK')
 AND (@custom != 'cbrid' OR [RI COMPLETE DATE] Is Null AND [FAI STATUS]='IN WORK')
 AND (@custom != 'cdb' OR [FAI STATUS]='CLOSED' AND [DATE COMPLETED]>=@fromDate And [DATE COMPLETED]<=@toDate)
 AND (@custom != 'wipnonpcb' OR [FAI TYPE]<>'PCB' AND [FAI STATUS]='IN WORK')
 AND (@custom != 'wippcb' OR [FAI TYPE]='PCB' AND [FAI STATUS]='IN WORK')
 AND (@custom != 'search' OR (@datecompleted_from IS NULL OR  [DATE COMPLETED] >= @datecompleted_from)
	AND (@datecompleted_to IS NULL OR [DATE COMPLETED] <= @datecompleted_to)
	AND (@needdate_from IS NULL OR  [NEED DATE] >= @needdate_from)
	AND (@needdate_to IS NULL OR [NEED DATE] <= @needdate_to)
	AND (@ricompletedate_from IS NULL OR  [RI COMPLETE DATE] >= @ricompletedate_from)
	AND (@ricompletedate_to IS NULL OR [RI COMPLETE DATE] <= @ricompletedate_to)
	AND (@itemnumber IS NULL OR [ITEM NUMBER] LIKE '%' + @itemnumber + '%' )
	AND (@distributormanufacturer IS NULL OR [DISTRIBUTOR/MANUFACTURER] LIKE '%' + @distributormanufacturer + '%' )
	AND (@revision IS NULL OR [REVISION]  LIKE '%' + @revision + '%' )
	AND (@inspector IS NULL OR [INSPECTOR]  LIKE '%' + @inspector + '%')
	AND (@description IS NULL OR [DESCRIPTION] LIKE '%' + @description + '%')
	AND (@finalresults IS NULL OR [FINAL RESULTS] LIKE '%' + @finalresults + '%')
	AND (@id IS NULL OR [id] = @id))


ORDER BY (case when @sortcolumn = '[id]' and @sortorder = 'asc' then [id] end)  asc,
         (case when @sortcolumn = '[id]' and @sortorder = 'desc' then [id] end) desc,
         (case when @sortcolumn = '[DESCRIPTION]' and @sortorder = 'asc' then [DESCRIPTION] end) asc,
         (case when @sortcolumn = '[DESCRIPTION]' and @sortorder = 'desc' then [DESCRIPTION] end) desc,
         (case when @sortcolumn = '[FINAL RESULTS]' and @sortorder = 'asc' then [FINAL RESULTS] end) asc,
         (case when @sortcolumn = '[FINAL RESULTS]' and @sortorder = 'desc' then [FINAL RESULTS] end) desc,
         (case when @sortcolumn = '[DISTRIBUTOR/MANUFACTURER]' and @sortorder = 'asc' then [DISTRIBUTOR/MANUFACTURER] end) asc,
         (case when @sortcolumn = '[DISTRIBUTOR/MANUFACTURER]' and @sortorder = 'desc' then [DISTRIBUTOR/MANUFACTURER] end) desc,
         (case when @sortcolumn = '[ITEM NUMBER]' and @sortorder = 'asc' then [ITEM NUMBER] end) asc,
         (case when @sortcolumn = '[ITEM NUMBER]' and @sortorder = 'desc' then [ITEM NUMBER] end) desc,
         (case when @sortcolumn = '[REVISION]' and @sortorder = 'asc' then [REVISION] end) asc,
         (case when @sortcolumn = '[REVISION]' and @sortorder = 'desc' then [REVISION] end) desc,
         (case when @sortcolumn = '[RI COMPLETE DATE]' and @sortorder = 'asc' then [RI COMPLETE DATE] end) asc,
         (case when @sortcolumn = '[RI COMPLETE DATE]' and @sortorder = 'desc' then [RI COMPLETE DATE] end) desc,
         (case when @sortcolumn = '[DATE COMPLETED]' and @sortorder = 'asc' then [DATE COMPLETED] end) asc,
         (case when @sortcolumn = '[DATE COMPLETED]' and @sortorder = 'desc' then [DATE COMPLETED] end) desc,
		 --(case when @custom = 'ewip' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'rc' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'rinc' then getdate()-[DATE RECEIVED] end) desc,	
		 --(case when @custom = 'aqe' then getdate()-[DATE RECEIVED] end) desc,	
		 --(case when @custom = 'riic' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'cbrid' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'cdb' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'wipnonpcb' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'wippcb' then getdate()-[DATE RECEIVED] end) desc,
		 --(case when @custom = 'search' then getdate()-[DATE RECEIVED] end) desc,
         (case when @sortcolumn IS NULL then  (getdate()-[DATE RECEIVED]) end) desc

OFFSET @recordSet ROWS FETCH NEXT @itemsPerPage ROWS ONLY

 



