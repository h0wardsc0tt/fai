USE [db_fai]
GO
/****** Object:  UserDefinedFunction [dbo].[currentlocation]    Script Date: 11/22/2016 8:52:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[currentlocation](@dateinqalab DATETIME, @dateinmfg DATETIME, @dateinqsmt DATETIME, @dateinqeng DATETIME)
RETURNS NVARCHAR(50)
AS
BEGIN
	DECLARE @loc nvarchar(50)

	SET @loc = CASE 
		WHEN @dateinqalab IS NOT Null THEN 'QA Lab'
		ELSE
			CASE 
			WHEN @dateinmfg IS NOT Null THEN 'MFG'
			ELSE
				CASE 
				WHEN @dateinqsmt IS NOT Null THEN 'SMT'
				ELSE
					CASE 
					WHEN @dateinqeng IS NOT Null THEN 'Engineering'
					ELSE 'Rec Insp'
					END
				END
			END
		END 
	return  @loc
END
