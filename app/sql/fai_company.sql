USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_company]    Script Date: 11/22/2016 8:43:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_company]
AS
SELECT distinct 'company' AS [key], UPPER([company 1]) AS value
FROM tbl_FAI
WHERE [company 1] IS NOT NULL AND [company 1] != ''  AND [company 1] != 'VOID'
ORDER BY value