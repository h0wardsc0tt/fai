USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[qryInWork]    Script Date: 11/22/2016 8:51:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[qryInWork](
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL
)AS

DECLARE
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@cnt INT,
	@count INT,
	@recordSet INT,
	@culture nvarchar(10),
	@currentDate DateTime

SET @culture = 'en-US'
SET @currentDate = GETDATE()

SELECT @rowCount = count(*) FROM [db_fai].[dbo].[tbl_FAI] fai WHERE [FAI STATUS] = 'IN WORK'

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' + CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END

SELECT 
	[APPROVED BY] AS approvedby,
	dbo.jsonattachments(fai.id) AS attachments,
	--'[' + STUFF((SELECT  ',' + '{"guid":"'+ cast(guid as varchar(36)) + '","filename":"' + filename + '","isactive":"' + cast(isActive as varchar(1)) + '"}'
 --           FROM [db_fai].[dbo].[fai_Attachments]
 --           WHERE  id = fai.id
	--		FOR XML PATH('')), 1, 1, '') + ']'AS attachments,
	[BUYER] AS buyer,
	[CLOSURE COMMENTS] AS closurecomments,
	[COMMENTS] AS comments,
	[COMPANY 1] AS company1,	
	dbo.currentlocation([DATE IN QA LAB], [DATE IN MANUFACTURING], [DATE IN SMT], [DATE IN ENGINEERING]) AS currentlocation,
	--CASE 
	--	WHEN [DATE IN QA LAB] IS NOT Null THEN 'QA Lab'
	--	ELSE
	--		CASE 
	--		WHEN [DATE IN MANUFACTURING] IS NOT Null THEN 'MFG'
	--		ELSE
	--			CASE 
	--			WHEN [DATE IN SMT] IS NOT Null THEN 'SMT'
	--			ELSE
	--				CASE 
	--				WHEN [DATE IN ENGINEERING] IS NOT Null THEN 'Engineering'
	--				ELSE 'Rec Insp'
	--				END
	--			END
	--		END
	--END  AS currentlocation,
	[CURRENT STATUS] AS currentstatus,
	[CURRENTLY LOCATED AS OF] AS currentlylocatedasof,
	[DATE CODE] AS datecode,
	FORMAT([DATE IN ENGINEERING],'d', @culture) AS dateinengineering,
	FORMAT([DATE IN QA LAB],'d', @culture) AS dateinqalab,
	FORMAT([DATE COMPLETED],'d', @culture) AS datecompleted,
	FORMAT([DATE IN MANUFACTURING],'d', @culture) AS dateinmanufacturing,
	FORMAT([DATE IN SMT],'d', @culture) AS dateinsmt,
	FORMAT([DATE RECEIVED],'d', @culture) AS datereceived,
	dbo.daysincurrentlocation([DATE IN QA LAB], [DATE IN MANUFACTURING], [DATE IN SMT], [DATE IN ENGINEERING], [DATE RECEIVED]) AS daysincurrentlocation,
	--CASE 
	--	WHEN [DATE IN QA LAB] IS NOT Null THEN DATEDIFF(DAY, [DATE IN QA LAB], @currentDate)
	--	ELSE
	--		CASE 
	--		WHEN [DATE IN MANUFACTURING] IS NOT Null THEN DATEDIFF(DAY, [DATE IN MANUFACTURING], @currentDate)
	--		ELSE
	--			CASE 
	--			WHEN [DATE IN SMT] IS NOT Null THEN DATEDIFF(DAY, [DATE IN SMT], @currentDate)
	--			ELSE
	--				CASE 
	--				WHEN [DATE IN ENGINEERING] IS NOT Null THEN DATEDIFF(DAY, [DATE IN ENGINEERING], @currentDate)
	--				ELSE  DATEDIFF(DAY, [DATE RECEIVED], @currentDate)
	--				END
	--			END
	--		END
	--END  AS daysincurrentlocation,
	[DAYS TO COMPLETE] AS daystocomplete,
	[DESCRIPTION] AS description,
	[DISTRIBUTOR/MANUFACTURER] AS distributormanufacturer,
	CASE WHEN [DMR REF #] = 0 THEN NULL ELSE [DMR REF #] END AS dmrref,
	[ENG APPROVER] AS engapprover,
	FORMAT([ENGINEERING DATE  COMPLETE],'d', @culture) AS engineeringdatecomplete,
	[ENGINEERING EMAIL] AS engineeringemail,
	[ENGINEERING RESULTS] AS engineeringresults,
	[FAI LEVEL] AS failevel,
	[FAI STATUS] AS faistatus,
	[FAI TYPE] AS faitype,
	[FINAL RESULTS] AS finalresults,
	[ID] AS id,
	[INSPECTOR] AS inspector,
	[ITEM NUMBER] AS itemnumber,
	CASE WHEN [LOT QTY RECD] = 0 THEN '' ELSE [LOT QTY RECD] END AS lotqtyrecd,
	[MANF PASS / FAIL] AS manfpassfail,
	[MANF RESULTS] AS manfresults,
	FORMAT([MANUFACTURING COMPLETE DATE],'d', @culture) AS manufacturingcompletedate,
	[MANUFACTURING NEW PN] AS manufacturingnewpn,
	[MANUFACTURING QTY COMPLETED] AS manufacturingqtycompleted,
	[MANUFACTURING QTY RECIEVED] AS manufacturingqtyrecieved,
	FORMAT([NEED DATE],'d', @culture) AS needdate,
	[PILOT?] AS pilot,
	[PO #] AS po,
	[PROJECT] AS project,
	[PROJECT ENGINEER] AS projectengineer,
	FORMAT([QA  DATE],'d', @culture) AS qadate,
	FORMAT([QA LAB DATE COMPLETE],'d', @culture) AS qalabdatecomplete,
	[qa lab qty completed] AS qalabqtycompleted,
	[qa lab qty received] AS qalabqtyreceived,
	[QA TEST LAB  REPORT #] AS qatestlabreport,
	[QA TEST LAB PASS / FAIL] AS qatestlabpassfail,
	[REASON FOR FAI] AS reasonforfai,
	[REVISION] AS revision,
	LTRIM([RI EVALUTION  - RESULTS]) AS rievalutionresults,
	[RI PASS / FAIL] AS ripassfail,
	[RI QTY INSPECTED] AS riqtyinspected,
	[RI QTY REJECTED] AS riqtyrejected,
	FORMAT([RI COMPLETE DATE],'d', @culture) AS ricompletedate,
	[RMO NUMBER] AS rmonumber,
	[SMT PASS / FAIL] AS smtpassfail,
	[SMT RESULTS] AS smtresults,
	FORMAT([SMT COMPLETE DATE],'d', @culture) AS smtcompletedate,
	[SMT NEW PN] AS smtnewpn,
	[SMT QTY] AS smtqty,
	[SMT QTY COMPLETED] AS smtqtycompleted,
	[SupplierReport] AS supplierreport,
	[THA RESULTS] AS tharesults,
	[THA PASS]  AS thapassfail,
	DATEDIFF(DAY, [DATE RECEIVED], @currentDate) AS totaldaysinwork
FROM [db_fai].[dbo].[tbl_FAI] fai
WHERE [FAI STATUS] = 'IN WORK'
--WHERE [COMPANY 1] IS NOT NULL AND [COMPANY 1] != '' AND [FAI STATUS] = 'IN WORK'
ORDER BY [COMPANY 1], @currentDate - [DATE RECEIVED] DESC
OFFSET @recordSet  ROWS FETCH NEXT @itemsPerPage  ROWS ONLY

