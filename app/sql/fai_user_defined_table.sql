USE [db_fai]
GO
/****** Object:  User-Defined Table [dbo].[fai_parameters]    Script Date: 10/30/2016 6:54:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TYPE dbo.fai_parameters AS TABLE(
	id INT NULL,
	pageNumber INT NULL,
	itemsPerPage INT NULL,
	company1 NVARCHAR(255) NULL,
	description NVARCHAR(255) NULL,
	distributormanufacturer NVARCHAR(255) NULL,
	inspector NVARCHAR(255) NULL,
	itemnumber NVARCHAR(255) NULL,
	needdate_from NVARCHAR(50) NULL,
	needdate_to NVARCHAR(50) NULL,
	po NVARCHAR(255) NULL,
	project NVARCHAR(255) NULL,
	sortColumn NVARCHAR(50) NULL,
	sortOrder NVARCHAR(5) NULL,
	ricompletedate_from NVARCHAR(50) NULL,
	ricompletedateate_to NVARCHAR(50) NULL,
	rmonumber  NVARCHAR(10),
	custom NVARCHAR(2000) NULL,
	finalresults NVARCHAR(255) NULL,
	revision NVARCHAR(255) NULL,
	datecompleted_from NVARCHAR(50) NULL,
	datecompleted_to NVARCHAR(50) NULL

)