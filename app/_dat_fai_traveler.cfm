   <cfdocument format="PDF" filename="#tmp_file_name#" overwrite="Yes"
    marginLeft = ".1" 
    marginTop = ".1"
    marginRight = ".1"  >
    <cfdocumentitem type="footer"> 
    	<cfoutput>

            <table style="font-size:10px; color:##C3B8B8; width:100%;"><tr>
            <td align="left">
                HME 555024B (08/13)
            </td>
            <td align="center">
               HME CONFIDENTAL AND PROPRIETARY INFORMATION
            </td>
            <td align="right">
                Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#
            </td>
            </tr>
            </table>

		</cfoutput> 
     </cfdocumentitem> 
<cfset todayDate = Now()> 
<cfscript>		
		line_break = '';
		attachments = '';
		if(fai.attachments != ''){
			attachments_struct = deserializeJson(fai.attachments);
			for(i = 1; i LTE ArrayLen(attachments_struct); i++){
				attachments = attachments & line_break & attachments_struct[i].filename;  
				line_break = '<br>';      
			}
		}
		supplierreport = 'Yes';
		if(fai.supplierreport == 0)
			supplierreport = 'No';	
		ripassfail = ' fail';
		if(fai.ripassfail == 'PASS')
			ripassfail = ' pass';
		finalresults = '';
		if(fai.finalresults == 'PASSED')
			finalresults = ' pass';
		if(fai.finalresults == 'PASSED WITH CONDITIONS')
			finalresults = ' pass';
		if(fai.finalresults == 'FAILED')
			finalresults = ' fail';

</cfscript>
<cfoutput>
<style>
.bold{
	font-weight:bold;
	font-size:15px;
}

.title{
	color:##0813E9;
	font-size:13px;
}

.title2{
	color:##0813E9;
	font-size:13px;
}
.info{
	color:##000;
	font-size:13px;
	/*border:solid 1px red;*/
}
.width1{

	width:100px;
}

.po-color{
	background-color:##e6b9b8;
}

.ri-color{
	background-color:##CBC5C5;
}

.smt-color{
	background-color:##c4bd97;
}

.mfg-color{
	background-color:##fac090;
}

.eng-color{
	background-color:##000;
}

.eng-font-color{
	color:##EBF106;
}

.qa-color{
	background-color:##c3d69b;
}

.fr-color{
	background-color:##9A9696;
}

.title-dev{
	padding:2px 0px 1px 5px;
}

.val-dev{
	border:solid 1px ##D0C8C8; 
	padding:0px 0px 0px 1px;
	margin:1px 0px 1px 0px;
	
}

.first{
	margin:2px 0px 1px 0px;
}

.fail{
	color:##F00609;
}

.pass{
	color:##06F430;
}

.right-val_lenght{
	width:136px;
}


.right-val_lenght2{
	width:141px;
}
</style>
<body>
<form action="" method="POST" id="FAI_Report_Form" class="form-horizontal col-lg-12" role="form">
	<img style="height:30px; height:auto;" src=#localUrl(logoImage)#><br>
    <span style="font-size:15px; color:##CCC8C8;">First Article Inspection</span><br>
	<div align="center" style="font-size:25px;color:##0813E9;">PCB FIRST ARTICLE TRAVELER</div>
	<div align="right" style="font-size:15px;color:##0813E9;padding-right:10px;">FAI REF ## #fai.id#</div>
    <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; width:100%;">    
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border:solid grey 1px; font-size:13px;">
                    <tr>
                    	<td valign="top" style="width:90px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Part No.:</span>
                        <div align="center">#fai.itemnumber#</div>
                        </td>
                   	<td valign="top" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Rev:</span>
                        <div align="center">#fai.revision#</div>
                        </td>
                   	<td valign="top" style="width:300px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Description:</span>
                        <div>#fai.description#</div>
                        </td>
                   	<td valign="top" style="width:300px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Project:</span>
                        <div>#fai.project#</div>
                        </td>
                   </tr>
                </table>
             </td>
       </tr>    
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border-left:solid grey 1px; font-size:13px;border-right:solid grey 1px; font-size:13px;border-bottom:solid grey 1px; font-size:13px;">
                    <tr>
                    <td valign="top" style="width:100px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Date Code(s):</span>
                        <div align="center"></div>
                        </td>
                   	<td valign="top" style="width:100px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Factory Code:</span>
                        <div></div>
                        </td>
                   	<td valign="top" style="width:85px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">P.O.No.:</span>
                        <div align="center">#fai.po#</div>
                        </td>
                   	<td valign="top" style="width:70px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Qty Rec.:</span>
                        <div align="center">#fai.lotqtyrecd#</div>
                        </td>
                   	<td valign="top" style="width:70px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Qty Insp.:</span>
                        <div align="center">#fai.riqtyinspected#</div>
                        </td>
                   	<td valign="top" style="width:70px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Qty Rej.:</span>
                        <div align="center">#fai.riqtyrejected#</div>
                        </td>
                   	<td valign="top" style="width:150px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Inspector:</span>
                        <div align="center">#fai.inspector#</div>
                        </td>
                   	<td valign="top" style="width:70px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">ECO ##:</span>
                        <div align="center"></div>
                        </td>
                   </tr>
                </table>
             </td>
       </tr>    
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border-left:solid grey 1px; font-size:13px;border-right:solid grey 1px; font-size:13px;border-bottom:solid grey 1px; font-size:13px;">
                    <tr>
                    <td valign="top" style="width:290px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Supplier:</span>
                        <div align="center">#fai.distributormanufacturer#</div>
                        </td>
                    <td valign="top" style="width:250px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Buyer:</span>
                        <div>#fai.buyer#</div>
                        </td>
                    <td valign="top" style="width:290px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"><span style="font-size:12px;">Reason for FAI:</span>
                        <div>#fai.reasonforfai#</div>
                        </td>
                   </tr>
                </table>
             </td>
       </tr>    
 
        <tr>
            <td style="height:5px;"></td>
        </tr>
        
        <tr>
            <td align="center" style="background-color:##e6b9b8;color:##0813E9;">PCB FIRST ARTICLE TRAVELER</td>
        </tr>
        <tr>
            <td>
				<table cellpadding="0" cellspacing="0" style="font-size:15px;">
				<tr>
            		<td style="width:100px;"><span style=" text-decoration:underline;">FAI SCOPE</span>:</td>
            		<td style="width:50px;"><input type="checkbox"></td>
            		<td><span style=" font-weight:800;">Level I FAI</span><span style=" font-weight:400;"> - Receiving Inspection Only</span> &darr;</td>
                </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border:solid grey 1px; font-size:10px;">
                    <tr>
                    <td align="center" style="width:110px;padding:0px 3px 3px 3px;">Process</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">QTY</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Pass/Fail</td>
                    <td align="center" style="width:300px;padding:0px 3px 3px 3px;">Notes</td>
                    <td align="center" style="width:100px;padding:0px 3px 3px 3px;">New Level PN</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Signed</td>
                    <td align="center" style="width:80px;padding:0px 3px 3px 3px;">Date</td>
                   </tr>
                </table>            
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border-left:solid grey 1px; font-size:13px;border-right:solid grey 1px; font-size:13px;border-bottom:solid grey 1px; font-size:13px;">
                    <tr>
                    <td style="width:110px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">Rec. Insp.</span>
                     </td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td style="width:300px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:100px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:80px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                   </tr>
                </table>
             </td>
       </tr>   
 
        <tr>
            <td>
				<table cellpadding="0" cellspacing="0" style="font-size:15px;">
				<tr>
            		<td style="width:100px;"><span style=" text-decoration:underline;">FAI SCOPE</span>:</td>
            		<td style="width:50px;"><input type="checkbox"></td>
            		<td><span style=" font-weight:800;">Level II FAI</span><span style=" font-weight:400;"> - Receiving, SMT & THA</span> &darr;</td>
                </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border:solid grey 1px; font-size:10px;">
                    <tr>
                    <td align="center" style="width:110px;padding:0px 3px 3px 3px;">Process</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">QTY</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Pass/Fail</td>
                    <td align="center" style="width:300px;padding:0px 3px 3px 3px;">Notes</td>
                    <td align="center" style="width:100px;padding:0px 3px 3px 3px;">New Level PN</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Signed</td>
                    <td align="center" style="width:80px;padding:0px 3px 3px 3px;">Date</td>
                   </tr>
                </table>            
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border-left:solid grey 1px; font-size:13px;border-right:solid grey 1px; font-size:13px;border-bottom:solid grey 1px; font-size:13px;">
                    <tr>
                    <td style="width:110px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">SMT</span></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td style="width:300px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:100px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:80px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                   </tr>
                   <tr>
                    <td style="width:110px;border-top:solid grey 1px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">THA</span></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td style="width:300px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:100px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:80px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                   </tr>
                </table>
             </td>
       </tr>  
 
         <tr>
            <td>
				<table cellpadding="0" cellspacing="0" style="font-size:15px;">
				<tr>
            		<td style="width:100px;"><span style=" text-decoration:underline;">FAI SCOPE</span>:</td>
            		<td style="width:50px;"><input type="checkbox"></td>
            		<td><span style=" font-weight:800;">Level III FAI</span><span style=" font-weight:400;"> - Receiving SMT, THA, ATE/ICT testing & Top Level</span> &darr;</td>
                </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border:solid grey 1px; font-size:10px;">
                    <tr>
                    <td align="center" style="width:110px;padding:0px 3px 3px 3px;">Process</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">QTY</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Pass/Fail</td>
                    <td align="center" style="width:300px;padding:0px 3px 3px 3px;">Notes</td>
                    <td align="center" style="width:100px;padding:0px 3px 3px 3px;">New Level PN</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Signed</td>
                    <td align="center" style="width:80px;padding:0px 3px 3px 3px;">Date</td>
                   </tr>
                </table>            
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border-left:solid grey 1px; font-size:13px;border-right:solid grey 1px; font-size:13px;border-bottom:solid grey 1px; font-size:13px;">
                    <tr>
                    <td style="width:110px;padding:0px 3px 3px 0px;">
                     	<table cellpadding="0" cellspacing="0" style=" font-size:13px;">
                     	<tr>
                     		<td>
                    			<input type="checkbox">
                    		</td>
                    		<td align="center" style="padding-left:2px;">
                    			PCB Test<br>(ATE/ICT)
                    		</td>
                    	</tr>
                    	</table>
                    </td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td style="width:300px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:100px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:80px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                   </tr>
                   <tr>
                    <td style="width:110px;border-top:solid grey 1px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">Top Level Mfg</span></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td style="width:300px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:100px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:80px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                   </tr>
                   <tr>
                    <td style="width:110px;padding:0px 3px 3px 0px;border-top:solid grey 1px;"><input type="checkbox"><span style="padding-left:2px;">Top Level Test</span></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td style="width:300px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:100px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:80px;border-top:solid grey 1px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                   </tr>
                </table>
             </td>
       </tr> 
       
       
        <tr>
            <td>
				<table cellpadding="0" cellspacing="0" style="font-size:15px;">
				<tr>
            		<td style="width:100px;"><span style=" text-decoration:underline;">FAI SCOPE</span>:</td>
            		<td style="width:50px;"><input type="checkbox"></td>
            		<td><span style=" font-weight:800;">Level IV FAI</span><span style=" font-weight:400;"> - Full Build, Test and Environmental</span> &darr;</td>
                </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border:solid grey 1px; font-size:10px;">
                    <tr>
                    <td align="center" style="width:110px;padding:0px 3px 3px 3px;">Process</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">QTY</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Pass/Fail</td>
                    <td align="center" style="width:300px;padding:0px 3px 3px 3px;">Notes</td>
                    <td align="center" style="width:100px;padding:0px 3px 3px 3px;">New Level PN</td>
                    <td align="center" style="width:50px;padding:0px 3px 3px 3px;">Signed</td>
                    <td align="center" style="width:80px;padding:0px 3px 3px 3px;">Date</td>
                   </tr>
                </table>            
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border-left:solid grey 1px; font-size:13px;border-right:solid grey 1px; font-size:13px;border-bottom:solid grey 1px; font-size:13px;">
                    <tr>
                    <td style="width:110px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">QA Test</span>
                     </td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td style="width:300px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:100px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:50px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                    <td align="center" style="width:80px; border-left:solid grey 1px;padding:0px 3px 3px 3px;"></td>
                   </tr>
                </table>
             </td>
       </tr>        
       
        
        <tr>
            <td style="height:5px;"></td>
        </tr>
        
        <tr>
            <td align="center" style="background-color:##e6b9b8;color:##0813E9;">PCB FIRST ARTICLE DISPOSITION</td>
        </tr>
        <tr>
            <td style="height:5px;"></td>
        </tr>

        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; border-left:solid grey 1px; font-size:13px;border-right:solid grey 1px; font-size:13px;border-bottom:solid grey 1px;border-top:solid grey 1px; font-size:13px;">
                    <tr>
                        <td style="width:230px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">Approved</span>
                         </td>
                        <td style="width:305px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">Approved with Conditions (commnets required)</span>
                         </td>
                        <td style="width:215px;padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">Disapproved (commnets required)</span>
                         </td>
                   </tr>
                   <tr>
                        <td style="padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">Add Approved Supplier</span>
                         </td>
                        <td style="padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">Check FAI Box</span>
                         </td>
                        <td style="padding:0px 3px 3px 0px;"><input type="checkbox"><span style="padding-left:2px;">VAN Required</span>
                         </td>
                   </tr>
                   <tr>                       
                        <td valign="top" colspan="3" style="height:80px;padding:0px 3px 3px 0px;;border-top:solid grey 1px;"><span style="padding-left:2px;">Comments:</span>
                        <div align="left" style="padding-left:5px;"></div>
                         </td>                        
                   </tr>
                   <tr>
                        <td valign="top" colspan="2" style="height:50px;padding:0px 3px 3px 0px;border-top:solid grey 1px;border-right:solid grey 1px;"><span style="padding-left:2px;">Engineering Signed By:</span>
                        <div align="left" style="padding-left:5px;"></div>
                         </td>
                        <td valign="top" style="padding:0px 3px 3px 0px;;border-top:solid grey 1px;"><span style="padding-left:2px;">Date:</span>
                        <div align="left" style="padding-left:5px;"></div>
                         </td>
                   </tr>
                   <tr>
                        <td valign="top" colspan="2" style="height:50px;padding:0px 3px 3px 0px;border-top:solid grey 1px;border-right:solid grey 1px;"><span style="padding-left:2px;">QA Signed By:</span>
                        <div align="left" style="padding-left:5px;"></div>
                         </td>
                        <td valign="top" style="padding:0px 3px 3px 0px;;border-top:solid grey 1px;"><span style="padding-left:2px;">Date:</span>
                        <div align="left" style="padding-left:5px;"></div>
                         </td>
                   </tr>
                </table>
             </td>
       </tr>
  
    </table>
    </div>
</form>
<div class="col-lg-12">&nbsp;</div> 
</body>
</cfoutput> 
</cfdocument>


<cffunction name="localUrl" >
     <cfargument name="file" />
		 <cfset var fpath = ExpandPath(file)>
         <cfset var f="">
         <cfset f = createObject("java", "java.io.File")>
         <cfset f.init(fpath)>
     <cfreturn f.toUrl().toString()>
</cffunction>

