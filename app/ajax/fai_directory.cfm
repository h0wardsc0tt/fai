
<cfldap 
    action="query"
    server="powdc01.HME.COM"
    name="qry_getUser"
    start="DC=hme,DC=com"
	filter="(&(objectclass=user)(mail=*)(department=*))"
    maxRows = "100000"
    username="hme\hscott"
    password="January2019!"
	attributes = "cn,sn,mail,givenname,SamAccountname,physicalDeliveryOfficeName,department,memberOf"
>
 

<cfquery dbtype="query" name="hme_users">
    select cn,sn,mail,givenname,SamAccountname AS Account,physicalDeliveryOfficeName AS Office,department
    from qry_getUser
    where memberOf like '%HMEAll%'
    order by cn
</cfquery>
<cfquery dbtype="query" name="hme_departments">
    select distinct department
    from hme_users
    order by department
</cfquery>
<cfquery dbtype="query" name="hme_offices">
    select distinct upper(Office)
    from hme_users
    where Office != '' AND Office != 'Poaway'
    order by Office
</cfquery>

<cfscript>
	serializer = new lib.JsonSerializer();
	directory = StructNew();
	directory.users =  serializer.serialize(hme_users);
	directory.departments =  serializer.serialize(hme_departments);
	directory.offices =  serializer.serialize(hme_offices);
	writeOutput(serializer.serialize(directory));
	StructClear(directory);		

</cfscript>
