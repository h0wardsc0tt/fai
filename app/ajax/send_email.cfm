<cfinclude template="../serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfscript>		
    jsonEmail = deserializeJSON(ToString(getHTTPRequestData().content));	
</cfscript>

<cfset tmp_file_name = "#GetTempDirectory()#/#CreateUUID()#.pdf">
<cfset logoImage ='../images/hme-companies.png'>

<cfset results = "OK">
<cftry>
    <cfmail to="#jsonEmail.to#" cc="#jsonEmail.cc#" from="no-reply@hme.com" subject="#jsonEmail.subject#" type="html"> 
       <p> 
             <img src="cid:logo" alt="" /><br /> 
             <span style="font-size:15px; color:##CCC8C8;">First Article Inspection</span><br>
       </p> 
    
        #jsonEmail.emailcontent#
        <cfmailparam  
             file="#ExpandPath('../images/hme-companies.png')#" 
             contentid="logo"  
             disposition="inline" 
        /> 
		<cfif jsonEmail.reporttype eq 'completion_notification_report'>
            <cfquery name="qry_getattachments" datasource="#APPLICATION.FAI_Datasource#">
                SELECT filename,[file],prefix
                FROM fai_Attachments
                WHERE id = #jsonEmail.id# and isactive = 1 and filename IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#REReplace(REReplace(jsonEmail.attachments,"; ",",","all"),";",",","all")#" list="yes">)
            </cfquery>
            <cfloop query = "qry_getattachments">  
                <cfmailparam file="#replace(replace(filename," ","_","ALL"), ",", "_")#" content="#file#">  
            </cfloop>
        </cfif>
        
		<!---<cfif jsonEmail.reporttype eq 'completion_notification_report'>
			<cfscript>
                jsonFAI = StructNew();
                jsonFAI["id"] = jsonEmail.id;
                obj = CreateObject("component", "_cfcs.fai");
                fai = obj.searchByFAINo(argumentCollection = jsonFAI);
            </cfscript>
            <cfinclude template="../_dat_fai_completion_notification_report.cfm">
         	<cfmailparam file = "#tmp_file_name#"remove = "yes"> 
        </cfif>--->
        <cfif jsonEmail.reporttype eq 'closed_the_day_before_report'>
			<cfset fromdate = jsonEmail.fromdate> 
			<cfset todate = jsonEmail.todate> 
            <cfinclude template="../_dat_closed_the_day_before_report.cfm">
         	<cfmailparam file = "#tmp_file_name#"remove = "yes"> 
       </cfif>
        <cfif jsonEmail.reporttype eq 'work_in_progress_report'>
            <cfinclude template="../_dat_wip_report.cfm">
         	<cfmailparam file = "#tmp_file_name#"remove = "yes"> 
       </cfif>
        <cfif jsonEmail.reporttype eq 'awaiting_qe_report'>
            <cfinclude template="../_dat_awaiting_qe_report.cfm">
         	<cfmailparam file = "#tmp_file_name#"remove = "yes"> 
       </cfif>
        <cfif jsonEmail.reporttype eq 'engineering_wip_report'>
            <cfinclude template="../_dat_engineering_wip_report.cfm">
         	<cfmailparam file = "#tmp_file_name#"remove = "yes"> 
       </cfif>
        

    </cfmail>
    <cfcatch type="Application">
    	<cfset results = #cfcatch.detail#>
    	<!---<cflog text="#cfcatch.detail#" file="mail" type="Error" application="yes">--->
    </cfcatch>
</cftry>
<cfscript>
	jsonEmail.results = results;
	jsonEmail.from="#SESSION.User_Email#";
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(jsonEmail));;
</cfscript>