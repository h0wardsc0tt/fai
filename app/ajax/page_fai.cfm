<cfinclude template="../serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfscript>		
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	faiFilter = deserializeJSON(ToString(getHTTPRequestData().content));
		jsonFAI = StructNew();
		jsonFAI['pageNumber'] = faiFilter.pagenumber;
		jsonFAI['itemsPerPage'] = faiFilter.itemsperpage;
		if(faiFilter.sortcolumn != ''){
			jsonFAI['sortcolumn'] = faiFilter.sortcolumn;
			jsonFAI['sortorder'] = faiFilter.sortorder;
		}
		obj = CreateObject("component", "_cfcs.fai");
		switch(faiFilter.custom){
			case 'ewip':
			case 'wip' :
			case 'newtickets' :
			case 'rc'  :
			case 'rinc':
			case 'aqe' :
			case 'riic' :
			case 'wippcb'   :
			case 'wipnonpcb':
				jsonFAI['custom'] = faiFilter.custom;	
				returnSelect  = obj.custom(argumentCollection = jsonFAI);
				break;
			case 'fpy' :
				jsonFAI['custom'] = faiFilter.custom;	
				jsonFAI['supplier'] = faiFilter.supplier;
				returnSelect  = obj.custom(argumentCollection = jsonFAI);
				break;
			case 'cdb' :
				jsonFAI['custom'] = faiFilter.custom;	
				jsonFAI['fromdate'] = faiFilter.fromdate;
				jsonFAI['todate'] = faiFilter.todate;
				returnSelect  = obj.custom(argumentCollection = jsonFAI);
				break;
			case 'search' :
				jsonFAI['custom'] = faiFilter.custom;	
				if(faiFilter.datecompleted_from != '')
					jsonFAI['datecompleted_from'] = faiFilter.datecompleted_from;
				if(faiFilter.datecompleted_to != '')
					jsonFAI['datecompleted_to'] = faiFilter.datecompleted_to;
				if(faiFilter.needdate_from != '')
					jsonFAI['needdate_from'] = faiFilter.needdate_from; 
				if(faiFilter.needdate_to != '')
					jsonFAI['needdate_to'] = faiFilter.needdate_to;
				if(faiFilter.ricompletedate_from != '')
					jsonFAI['ricompletedate_from'] = faiFilter.ricompletedate_from;
				if(faiFilter.ricompletedate_to != '')
					jsonFAI['ricompletedate_to'] = faiFilter.ricompletedate_to;
				jsonFAI['itemnumber'] = faiFilter.itemnumber;
				jsonFAI['distributormanufacturer'] = faiFilter.distributormanufacturer;
				jsonFAI['finalresults'] = faiFilter.finalresults;
				jsonFAI['description'] = faiFilter.description;
				jsonFAI['inspector'] = faiFilter.inspector;
				jsonFAI['revision'] = faiFilter.revision;
				jsonFAI['project'] = faiFilter.project;
				
				returnSelect  = obj.custom(argumentCollection = jsonFAI);
				break;
		}
		returnResult = StructNew();
		returnResult['itemsperpage'] = faiFilter['itemsperpage'];
		returnResult['pagenumber'] = faiFilter['pagenumber'];
		if(returnSelect.RecordCount > 0)
			returnResult['count'] = returnSelect['count'];
		else
			returnResult['count'] = '0';
		deleteStatus = StructDelete(faiFilter,'storedProcedure','True');
		returnResult['returnSelect'] = returnSelect;	
		returnResult['faiFilter'] = StructCopy(faiFilter);	
		returnResult['page_start'] = ((returnResult['pagenumber'] - 1) * returnResult['itemsperpage']) + 1;	
		returnResult['page_end'] = returnResult['pagenumber'] * returnResult['itemsperpage'];	
 		StructClear(faiFilter);	
	}
	else
	{
		returnResult = StructNew();
	}
	
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnResult));	
</cfscript>
