				<!-- build:js ./js/fai_script.min.js -->
               	<script src="./js/1.12.0_jquery.min.js" type="text/javascript"></script>
                <script src="./js/3.3.5_bootstrap.min.js" type="text/javascript"></script>
                <script src="./js/1.11.4_jquery-ui.min.js" type="text/javascript"></script>
                <script src="./js/dragDropUpload.js" type="text/javascript"></script>
				<script src="./js/jquery.uploadfile.js" type="text/javascript"></script>
 				<script src="./js/fai.js" type="text/javascript"></script>
 				<script src="./js/leads.js" type="text/javascript"></script>
                <script src="./js/bootstrap-select.min.js" type="text/javascript"></script>
                <script src="./js/spin.min.js" type="text/javascript"></script>
                <script src="./js/spinnerOptions.js" type="text/javascript"></script>
                <script src="./js/fai__email.js" type="text/javascript"></script>
                <script src="./js/autosize.js" type="text/javascript"></script>
                <script src="./js/alasql.min.js" type="text/javascript"></script>
                <script src="./js/fai_onLoad.js" type="text/javascript"></script>
				<!-- endbuild -->
                <script src="./js/ckeditor/ckeditor.js" type="text/javascript"></script>
				<script src="./js/ckeditor/adapters/jquery.js" type="text/javascript"></script>
               
                <form name="pdfReport" id="pdfReport" method="post" action="./fai_pdf_report_generator.cfm" target="iTarget">
                    <input type="hidden" name="faiID" id="faiID" />
                    <input type="hidden" name="fai_report_req" id="fai_report_req"/>			
                    <input type="hidden" name="fai_report_from_date" id="fai_report_from_date"/>			
                    <input type="hidden" name="fai_report_to_date" id="fai_report_to_date"/>			
                    <input type="hidden" name="fai_report_storedProcedure" id="fai_report_storedProcedure"/>			
                    <input type="hidden" name="fai_report_sortorder" id="fai_report_sortorder"/>			
                    <input type="hidden" name="fai_report_sortColumn" id="fai_report_sortColumn"/>			
                    <input type="hidden" name="fai_report_custom" id="fai_report_custom"/>			
                    <input type="hidden" name="fai_filters" id="fai_filters"/>			
                </form>
                <iframe id="iTarget" name="iTarget" style="display:none;"></iframe>
	</body>
</html>


