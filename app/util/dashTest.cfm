<!---Avante YTD Shipments--->
<cfscript>
VARIABLES.Datasource_AV = "Avante";

QUERY = StructNew();
QUERY.QueryName = "qry_getMTD";
QUERY.Datasource = VARIABLES.Datasource_AV;
QUERY.CurrentMonth = "Y";
QUERY.Ship_Team = "CAN";
QUERY.OrderBy = "INV_AMT";

DateFind = CreateDate(2012,05,31);
DateFindb = CreateDate(2012,01,01);
thisTeam = 'SC';

thisGroup = "SC,DLR";
</cfscript>
<cfset CUST_NOT_IN = "CFA,MCD,SB,SD,PDLR,PFD,IDT,SVA,TB,TBU,NTGR">
<cfset CUST_IN = "AR,BK,BM,BR,CFA,CJ,DC,DFD,DQ,DT,EU,HD,JB,KFC,LJS,MCD,PAN,PE,SB,SD,SK,SL,TB,TCB,WE">

<cfquery name="qry_getYTD" datasource="#VARIABLES.Datasource_AV#">
	SELECT *
	FROM vSALESSHIP
	WHERE 0=0
	AND CUST_TYPE IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#CUST_IN#" list="yes">)
	AND CUR_MONTH_FLAG = 'Y'
	AND SHIP_TEAM = '#thisTeam#'
	ORDER BY SHIP_TEAM
</cfquery>

<cfquery name="qry_getTOT" dbtype="query">
	SELECT SUM(INV_AMT) AS INV_TOT
	FROM qry_getYTD
</cfquery>


<cfset thisMonth = 5>
<cfset YearQuota = 3200000>
<cfset MonthQuota = YearQuota/12>
<cfset YTDQuota = thisMonth * MonthQuota>

<cfquery name="qry_getDLR" dbtype="query">
	SELECT SUM(INV_AMT) AS INV_TOT
	FROM qry_getYTD
	WHERE 
		DLR_FLAG = 'Y'
	AND SHIP_TEAM = 'DLR'
</cfquery>

<cfquery name="qry_getTeam" dbtype="query">
	SELECT SUM(INV_AMT) AS INV_TOT
	FROM qry_getYTD
	WHERE 
		SHIP_TEAM = '#thisTeam#'
</cfquery>

<cfquery name="qry_getGroup" dbtype="query">
	SELECT INV_AMT, CUST_TYPE, SHIP_TEAM, ID
	FROM qry_getYTD
	WHERE 
		SHIP_TEAM = '#thisTeam#'
</cfquery>

<cfset YTDPrct = Evaluate(qry_getTeam.INV_TOT/YTDQuota)>

<cfoutput>#qry_getTOT.INV_TOT# = Direct - #qry_getTeam.INV_TOT# | Dealer - #qry_getDLR.INV_TOT# || #YTDPrct# || #YTDQuota#</cfoutput>

<cfdump var="#qry_getGroup#">

<cfdump var="#YTDPrct#">





