var uploadObj;
var fai_Id = '';
var json_fai = '';
var isFAI = false;
var default_sortorder = 'desc';
var default_sortcolumn = '[DATE COMPLETED]';
var fileUploadError = false;
var changed_flag = false;
var follow_through = false;

$(document).ready(function() {
	//report_page();
	//searchByFAINo_ajax(JSON.stringify({id : '2711'}));
	//console.log(location.href);
	$('#newtickets').html(json_dropdowns.tickets[0].value+' new requests');
	refreshnewrequests();
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/fai_directory.cfm",
		success: function (data) {	
			
			var directory = JSON.parse(data);				
			//console.log(directory);
			json_hme_users =JSON.parse(directory.users);
			build_address_book(json_hme_users, 'givenname');	
			json_save_address_book = contacts;	

		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
		}	   
	});	
	//security='4';	
	if(security == '1' || security == '3' || security == '4' || security == '5'){
		$('.level1').prop('disabled', true);
		$('.blockout_level_1').show();
		$('.level1-hide').addClass('always-hidden');
		if(security == '1' ||security == '3' || security == '5'){
			$('.level2').prop('disabled', true);
			$('.level3').prop('disabled', true);
			$('.blockout_level_2').show();
			$('.blockout_level_3').show();
			$('.level2-textarea').prop('readonly', true);
			$('.level3-textarea').prop('readonly', true);
			$('.level2-textarea').addClass('blockout');
			$('.level3-textarea').addClass('blockout');

		}
		$('.level4').prop('disabled', true);
		$('.level5').prop('disabled', true);
		$('.level6').prop('disabled', true);
		$('.blockout_level_4').show();
		$('.blockout_level_5').show();
		$('.blockout_level_6').show();
		$('.level5-textarea').prop('readonly', true);
		$('.level6-textarea').prop('readonly', true);
		$('.level5-textarea').addClass('blockout');
		$('.level6-textarea').addClass('blockout');
		$('#fai_new_btn').addClass('always-hidden');
		if(security == '1'){
			$('#fai_save_btn').addClass('always-hidden');
		}
		if(security == '3' || security == '5'){
			$('.blockout_level_1').hide();
			$('#buyer').prop('disabled', false);
			$('#needdate').prop('disabled', false);
		}

		if(security == '5'){
			$('.level1-textarea').addClass('blockout');
			$('.level1-textarea').prop('readonly', true);
			$('#buyer').prop('disabled', true);
		}
	}

	if(security == '2' || security == '6'){
		$('.level1').prop('disabled', true);
		$('.level1-hide').addClass('always-hidden');
		$('.level2').prop('disabled', true);
		if(security == '2'){
			$('.level3').prop('disabled', true);
		}
		if(security == '6'){
			$('.level5').prop('disabled', true);
		}
		$('.level4').prop('disabled', true);
		$('.level6').prop('disabled', true);

		$('.level1-textarea').prop('readonly', true);
		$('.level2-textarea').prop('readonly', true);
		if(security == '2'){
			$('.level3-textarea').prop('readonly', true);
		}
		if(security == '6'){
			$('.level5-textarea').prop('readonly', true);
		}
		$('.level4-textarea').prop('readonly', true);
		$('.level6-textarea').prop('readonly', true);
		$('.blockout_level_1').show();
		$('.blockout_level_2').show();
		if(security == '2'){
			$('.blockout_level_3').show();
		}
		if(security == '6'){
			$('.blockout_level_5').show();
		}
		$('.blockout_level_4').show();
		$('.blockout_level_6').show();


		$('.level1-textarea').addClass('blockout');
		$('.level2-textarea').addClass('blockout');
		if(security == '2'){
			$('.level3-textarea').addClass('blockout');
		}
		if(security == '6'){
			$('.level5-textarea').addClass('blockout');
		}
		$('.level4-textarea').addClass('blockout');
		$('.level6-textarea').addClass('blockout');

		$('#fai_new_btn').addClass('always-hidden');
	}

	$('form input[type=text]').change(function(e) {
		$(this).attr('changed','1');
		changed_flag = true;
		console.log(this)
	});
	$('form textarea').change(function(e) {
		$(this).attr('changed','1');
		changed_flag = true;
		console.log(this)
	});
	$('form select').change(function(e) {
		$(this).attr('changed','1');
		changed_flag = true;
		console.log(this)
	});
	$('form input[type=checkbox]').change(function(e) {
		$(this).attr('changed','1');
		console.log(this)
	});

	$('.expandable-element').height(0);
	$('#CSP_Per_Page').show();

	$('#fileuploader').hide();
	$('.row .fai-cols').addClass('col-xs-6');
	$('.row .fai-cols .control-label').addClass('col-xs-5');
	$('.row .fai-cols .form-group div').addClass('col-xs-7');
	$('.eng-left div').removeClass('col-xs-7');

	uploadObj = $("#fileuploader").uploadFile({
		url:"uploadAttachment.cfm",
		fileName:"File",
		dragdropWidth:"400px",
		dragDropStr: "<span><b>Drag & Drop Attachments Files Here</b></span>",
		dynamicFormData: function()
		{
			var data ={"id":fai_Id};
			return data;        
		},
		onSuccess:function(files,data,xhr,pd)
		{
			//console.log(data);
			if(xhr.statusText == 'OK'){
				var json_new_fileInfo = JSON.parse(data);
				if(json_new_fileInfo.results == 'OK'){
					json_fileInfo = JSON.parse(json_fai.attachments);
					console.log(json_fileInfo);
					json_fileInfo.push({guid: json_new_fileInfo.guid,filename:json_new_fileInfo.filename,isactive:json_new_fileInfo.isactive});
					populate_attachment_list(json_fileInfo);
					var index = json.map(function (id) { return id['id']}).indexOf(fai_Id);
					json[index].attachments = JSON.stringify(json_fileInfo);
					console.log(json_fileInfo);
				}
			}
		},
		onError: function(files,status,errMsg,pd)
		{
			fileUploadError = true;
		},
		afterUploadAll:function(obj)
		{
			//console.log(obj);
			if(!fileUploadError)
				uploadObj.reset();
			fileUploadError = false;						
		}																											
	});						

	$(".input-date" ).datepicker({});

	$('.modal-dialog').draggable({
		handle: ".modal-header"
	});

	$('#email_form').draggable({
		handle: "#email_header, .email-footer-buttons"
	});


	$( ".cal-field" ).click(function(e) {
		$(this).datepicker('setDate', null);
	});

	$( ".cal-field" ).datepicker({
		onSelect: function(){}	
	});

	$(".sortable" ).click(function(e) {
		sort_results($(this));
	});

	$('.header_title').removeClass('sorted');

	var sortedColumn = $('#' + $('#Sort_Column').val());
	var sortedColumnId = sortedColumn.prop('id');

	$('#' + sortedColumnId + ' span:first-child').addClass('sorted');
	sortedColumn.removeClass('sort').addClass($('#Sort_Order').val());	

	// Paginate to Next page
	$('#CSP_Next_Page').click(function(e) {
		console.log('#CSP_Next_Page');
		lead_paging(1);
		return false;
	});

	// Paginate to Previous page
	$('#CSP_Prev_Page').click(function(e) {
		lead_paging(-1);
		return false;
	});	
	// Toggle items per page
	$('#CSP_Per_Page').change(function(e) {
		$('#CSP_Current_Page').val(1);
		$('#fai_Search_Form').submit();
	});

	$('#filter_excel_export').click(function(e) {
		export_filter('excel');
	});	

	$('#filter_pdf_export').click(function(e) {
		export_filter('pdf');
	});	

		$('.selectpicker').on('hidden.bs.select', function (e) {
		var txt =  $(this).parent().find('.bs-searchbox input[type=text]').val();
		if(txt == '')return;
		/*
		if($(this).prop('id') == 'email_to' || $(this).prop('id') == 'email_cc'){
			if(!regExp_email.test(txt)){
				if($(this).prop('id') == 'email_to')
					display_email_validation('<span style="color:#ff0000;">Invalid "TO" email format</span>&nbsp;&nbsp;&nbsp;' + txt );
				else
					display_email_validation('<span style="color:#ff0000;">Invalid "CC" email format</span>&nbsp;&nbsp;&nbsp;' + txt );
				return;
			}
		}
		*/
		if($(this).prop('id') == 'email_to'){
			list = $("#email_to");
			var txt =  $(this).parent().find('.bs-searchbox input[type=text]').val();
			json_email_resp.to = json_email_resp.to + (json_email_resp.to == '' ? '' : ';') + txt;
			var arr = json_email_resp.to.split(';');
			arr.sort();
			var email_selected = '';
			if($(this).val() != null)
				email_selected = ($(this).val().join(';') + ';' + txt).split(';');
			else
				email_selected = txt.split(';');
			$(this).empty();
			$.each(arr, function(index, item) {
				if(item != '')
					list.append(new Option(item, item));
			});	
			list.selectpicker('val', email_selected);
			list.selectpicker('refresh')
			return;
		}
		if($(this).prop('id') == 'email_cc'){
			var txt =  $(this).parent().find('.bs-searchbox input[type=text]').val();
			list = $("#email_cc");
			json_email_resp.cc = json_email_resp.cc + (json_email_resp.cc == '' ? '' : ';') + txt;
			var arr = json_email_resp.cc.split(';');
			arr.sort();
			var email_selected = '';
			if($(this).val() != null)
				email_selected = ($(this).val().join(';') + ';' + txt).split(';');
			else
				email_selected = txt.split(';');
			$(this).empty();
			$.each(arr, function(index, item) {
				if(item != '')
					list.append(new Option(item, item));
			});	
			list.selectpicker('val', email_selected);
			list.selectpicker('refresh')
			return;
		}
		var txt =  $(this).parent().find('.bs-searchbox input[type=text]').val().toUpperCase();
		//console.log(txt);
		if(txt != ''){
			var obj, type, list, sel, multi=false;
			sel = $(this).prop('id');
			//console.log(sel);
			switch(sel){
				case 'projectengineer':
					obj = json_dropdowns.projengs;
					type = 'projectengineer';
					list = $("#projectengineer");
					multi = true;
					break;
				case 'buyer':
					obj = json_dropdowns.buyers;
					type = 'buyer';
					list = $("#buyer");
					break;
				case 'distributormanufacturer':
					return;
					break;
				case 'inspector':
					obj = json_dropdowns.inspectors;
					type = 'inspector';
					list = $("#inspector");
					break;
				case 'company1':
					obj = json_dropdowns.companyies;
					type = 'company';
					list = $("#company1");
					break;
				case 'approvedby':
					obj = json_dropdowns.approvedby;
					type = 'approvedby';
					list = $("#approvedby");
					break;
			}
			var index = obj.map(function (id) { return id['value'].toLowerCase(); }).indexOf(txt.toLowerCase());
			if(index < 0){
				var selected = $('#' + sel).val();
				var val = txt.toCamelCase();
				obj.push({key:type, value:val});
				obj.sort(sort_by('value', false, function(a){return a.toUpperCase()}));
				//console.log(val + '   ' + sel + '    ' + selected);
				if(selected != null && multi)
					selected.push(val);
				else
					selected = val;
				//console.log(val + '   ' + sel + '    ' + selected);
				$(this).empty();
				$.each(obj, function(index, item) {
				  list.append(new Option(item.value.toCamelCase(), item.value.toCamelCase()));
				});	
				//$('#' + sel).append(new Option(val, val));
				$('#' + sel).selectpicker('val', selected);
				$('#' + sel).selectpicker('refresh')
			}
		}
	});	

	var list = $("#projectengineer");
	$.each(json_dropdowns.projengs, function(index, item) {
	  list.append(new Option(item.value.toCamelCase(), item.value.toCamelCase()));
	});	
	list = $("#buyer");
	  list.append(new Option('', ''));
	  $.each(json_dropdowns.buyers, function(index, item) {
	  list.append(new Option(item.value.toCamelCase(), item.value.toCamelCase()));
	});	

	list = $("#fai_form #distributormanufacturer");
	  list.append(new Option('', ''));
	  $.each(json_dropdowns.suppliers, function(index, item) {
	  list.append(new Option(item.value, item.value));
	});	

	list = $("#a_manufacturer");
	  //list.append(new Option('', ''));
	  $.each(json_dropdowns.suppliers, function(index, item) {
		  list.append(new Option(item.value, item.value));
	  });		

	list = $("#company1");
	  list.append(new Option('', ''));
	  $.each(json_dropdowns.companyies, function(index, item) {
	  list.append(new Option(item.value.toCamelCase(), item.value.toCamelCase()));
	});	

	list = $("#approvedby");
	  list.append(new Option('', ''));
	  $.each(json_dropdowns.approvedby, function(index, item) {
	  list.append(new Option(item.value.toCamelCase(), item.value.toCamelCase()));
	});	

	list = $("#inspector");
	  list.append(new Option('', ''));
	  $.each(json_dropdowns.inspectors, function(index, item) {
	  list.append(new Option(item.value.toCamelCase(), item.value.toCamelCase()));
	});	

	$("#fai_form textarea").bind('paste', function(e) {
		var elem = $(this);						
		setTimeout(function() {
			elem.val(elem.val().replace(/[\u2018|\u2019|\u201A]/g, "'"));
			elem.val(elem.val().replace(/[\u201C|\u201D|\u201E]/g, '"'));
			elem.val(elem.val().replace(/\u2026/g, "..."));
			elem.val(elem.val().replace(/[\u2013|\u2014]/g, "-"));
		}, 100);
	});

	$('.numeric').keypress(function(e){	
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 8 || keyCode == 9) return true;					
		if(!$.isNumeric(String.fromCharCode(keyCode))){return false;}
	});	

	$('.fai-lookup').keypress(function(e){	
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 13 || keyCode == 8 || keyCode == 9){return true;}					
		if(!$.isNumeric(String.fromCharCode(keyCode))){return false;}
	});	

	$('.noEnterKey').keypress(function(e){	
		var keyCode = (e.keyCode ? e.keyCode : e.which);
		if(keyCode == 13){return false;}					
	});	
/*						
	setTimeout(function(){
		$('#email_form').hide();
		$('#email_form').css('visibility','visible');
		$('#email_form').css({
			position:'fixed',
			left: ($(window).width() - $('#email_form').outerWidth())/2,
			top: ($(window).height() - $('#email_form').outerHeight())/2
		});
		$('.email-blackout').height($(document).height());
	}, 3000);
*/						
	$.each(json, function(index, item) {
		if(item.attachments == '')
			item.attachments = '[]';
	});	

	//console.log(json);
	//console.log(json_dropdowns);


/*						$(function() {

		// Set to Page 1 if new search
		$('#Cust_Search_Submit').click(function(e) {
			$('#CSP_Current_Page').val(1);
		});

		$('#Lead_Prev_Page').click(function(e) {
			if(display_lead_index == 0){
				lead_paging(-1);
			}else{
				display_lead_index -= 1;
				show_lead(display_lead_index)
			}
		});

		$('#Lead_Next_Page').click(function(e) {
			//console.log(display_lead_index +'   '+ (json.length - 1));
			if(display_lead_index == json.length - 1){
				lead_paging(1);
			}
			else{
				display_lead_index += 1;
				show_lead(display_lead_index)
			}
		});						
	});							
*/
});
function refreshnewrequests(){
	setTimeout(function(){
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "ajax/refreshnewrequests.cfm",
			success: function (data) {	
				var _data = JSON.parse(data);
				$('#newtickets').html(_data[0].value + ' new requests');
				if($('#search_results_type').html() == 'Search Results (New Requests)'){
					var json_update = {};
					var leadFilter_update = JSON.parse(leadFilter);
					leadFilter_update.itemsperpage = $('#CSP_Per_Page').val();
					leadFilter_update.pagenumber = $('#CSP_Current_Page').val();
					leadFilter_update.sortorder = $('#Sort_Order').val();
					var sort_col;
					switch( $('#Sort_Column').val()){
						case 'h_id':
							sort_col = '[ID]';
							break;
						case 'h_description':
							sort_col = '[DESCRIPTION]';
							break;
						case 'h_distributormanufacturer':
							sort_col = '[DISTRIBUTOR/MANUFACTURER]';
							break;
						case 'h_revision':
							sort_col = '[revision]';
							break;
						case 'h_itemnumber':
							sort_col = '[ITEM NUMBER]';
							break;
						case 'h_datecompleted':
							sort_col = '[date completed]';
							break;
						case 'h_finalresults':
							sort_col = '[final results]';
							break;
						case 'h_needdate':
							sort_col = '[NEED DATE]';
							break;
						case 'h_ricompletedate':
							sort_col = '[RI COMPLETE DATE]';
							break;
						case 'h_reasonforfai':
							sort_col = '[REASON FOR FAI]';
							break;
					}
					leadFilter_update.sortcolumn = sort_col;
					json_update.leadFilter = leadFilter_update;
					console.log(leadFilter_update);
					page_fai_ajax(JSON.stringify(leadFilter_update));

				}
				refreshnewrequests();
			}	   
		});	
	}, 30000);
}
//./fai_pdf_report_generator.cfm
//./fai_completion_notification.cfm