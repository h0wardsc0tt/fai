Array.prototype.insert = function (index, item) {
  this.splice(index, 0, item);
};

String.prototype.toCamelCase = function () {
    return this.toLowerCase().replace(/\b[a-z]/g, function (letter) {
        return letter.toUpperCase();
    });
};

function isEnterKey(e){
	var keyCode = (e.keyCode ? e.keyCode : e.which);
	if(keyCode == 13){return true;}					
	return false;
}

function sortResults(prop, asc) {
    people = people.sort(function(a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
    showResults();
}

var sort_by = function(field, reverse, primer){
   var key = primer ? 
       function(x) {return primer(x[field])} : 
       function(x) {return x[field]};
   reverse = !reverse ? 1 : -1;
   return function (a, b) {
       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
     } 
}

function reset_form(){
	$('.search_form_fields .form-control').val('');
}

function show_fai_from_filter(id){
	var i = json.map(function (id) { return id['id']; }).indexOf(id);
	if(!isFAI)report_page();
	$('#fileuploader').show();
	fai_Id = id;
	json_fai = {};
	$.each(json[i], function(key, value) {
		json_fai[key] = value;
	});
	//json_fai = json[i];
	//console.log(JSON.parse(data[0].attachments));
	display_fai(json_fai);
}

function fai_Number_Lookup(){
	if(changed_flag && !follow_through){
		console.log('fai_Number_Lookup interrupt');
	}
	var id = $('#Search').val();
	if(id != '' && $.isNumeric(id)){
		var json = {id : id}
		searchByFAINo_ajax(JSON.stringify(json));
		//console.log(json);
	}
}

function searchByFAINo_ajax(data){
	page_spinner = document.getElementById('spinner_dev');
	var opts = spinnerOptions();
	page_spinner = new Spinner(opts).spin(page_spinner);
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/searchByFAINo.cfm",
		data: data,
		dataType: "json",
		success: function (data) {					
			page_spinner.stop();
			if (data != null) {
				if(data.length > 0){
					if(!isFAI)report_page();
					$('#fileuploader').show();
					console.log(data[0]);
					fai_Id = data[0].id;
					json_fai = data[0];
					display_fai(data[0]);
				}
				else{
					display_status_message('Invalid FAI ID');
				}
			}
			else{
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			page_spinner.stop();
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
			//console.log(thrownError);
        }
	});
}

function display_fai(key_pair){
	var obj;
	$('#fai_form #fileuploader_new_fai').css('display','none');
	report_page();
	$.each(key_pair, function(key, value) {
		if(key !== 'attachments' && !$('#fai_form #' + key).length){
		}else{
			//console.log(key);
			switch(key){
				case "pilot":
				case "supplierreport":
				case "rohscert":
				case "engineeringwock":
				case "previousevidck":
				case "scarck":
				case "limteddurationck":
				case "lotnumberonlyck":
				case "inspectallck":
				case "inspectcriticalck":
				case "inspectcircledck":
				case "oktodestroyck":
				case "ingorepkgrohsfitcheckck":
				case "ignorenotesck":
				case "visualck":
				case "sortingonlyck":
				case "contactuponreceiptck":
				case "removedtsck":
				case "reportrequiredck":
				case "igorefitcheckck":
				case "ignoretextureck":
				case "ignorepartmarketingck":
				case "rmtoengafterinspectck":
					if(value === '1'){
						$('#' + key).prop('checked', true);
					}else{	
						$('#' + key).prop('checked', false);
					}
					break;
				case "projectengineer":
					var arr = value.toCamelCase().split(';');
					$('#projectengineer').selectpicker('deselectAll');			
					$('#projectengineer').selectpicker('val', arr);
					//$('.selectpicker').selectpicker();
					break;
				case "buyer":
					$('#buyer').selectpicker('val', '');		
					$('#buyer').selectpicker('val', value.toCamelCase());
					break;
				case "company1":
					$('#company1').selectpicker('val', '');		
					$('#company1').selectpicker('val', value.toCamelCase());
					break;
				case "approvedby":
					$('#approvedby').selectpicker('val', '');		
					$('#approvedby').selectpicker('val', value.toCamelCase());
					break;
				case "inspector":
					$('#inspector').selectpicker('val', '');		
					$('#inspector').selectpicker('val', value.toCamelCase());
					break;
				case "distributormanufacturer":
					$('#distributormanufacturer').selectpicker('val', '');		
					$('#distributormanufacturer').selectpicker('val', value);
					break;
				case "attachments":
					$('#attachment_list').empty();
					populate_attachment_list(JSON.parse(value));
					break;
				default:			
					$('#fai_form #' + key).val(value);
					break;
			}
		}
    });

	reset_change_flag();
}

function reset_change_flag(){
	change_flag = false;
	follow_through= false;
	$('.level1').attr('changed','0');
	$('.level2').attr('changed','0');
	$('.level3').attr('changed','0');
	$('.level4').attr('changed','0');
	$('.level5').attr('changed','0');
	$('.level6').attr('changed','0');
	$('.level1-textarea').attr('changed','0');
	$('.level2-textarea').attr('changed','0');
	$('.level3-textarea').attr('changed','0');
	$('.level5-textarea').attr('changed','0');
	$('.level6-textarea').attr('changed','0');
}

function populate_attachment_list(attachments){
	console.log(attachments);
	$('#attachment_list').empty();
	var file_li = '';
	$.each(attachments, function(index, json_attachment) {
		var disable_checkbox = json_attachment.isactive === '1' ? '' : ' disabled ';
		file_li = "<li>";
		file_li += "<button" + (security !== '0' ? ' disabled' : '') + " class='btn btn-primary file-li-active-btn file-li-enable-disable-" + index + "' index=" + index + " onclick='enable_disable_attachment($(this));return false;'>" + (json_attachment.isactive == '0' ? 'Set Active' : 'Set Inactive') + "</button>";
		file_li += "<button" + (security !== '0' ? ' disabled' : '') + " class='btn btn-primary file-li-change-name-btn' index=" + index + " onclick='change_attachment_name($(this));return false;'>Re-Name</button>";
		//file_li += "<input type='checkbox'" + disable_checkbox + "class='file-li-checkbox'><a href='displayAttachment.cfm?guid=" + json_attachment.guid + "'>";
		file_li += "<a target='_blank' href='displayAttachment.cfm?guid=" + json_attachment.guid + "'>";
		file_li += "<div class='file-li-anchor  " + (json_attachment.isactive == '0' ? 'file-li-anchor-inactive-color' : '') + " file-li-filename-" + index + "'>" + json_attachment.filename + "</div></a></li>";
		$('#attachment_list').append(file_li);
	});
}

function change_attachment_name(obj){
	var attachment;
	var index = json.map(function (id) { return id['id']}).indexOf(fai_Id);
	if(index >= 0)
		attachment = JSON.parse(json[index].attachments)[obj.attr('index')];
	else
		attachment = JSON.parse(json_fai.attachments)[obj.attr('index')];
	var _json = {};
	//var attachment = JSON.parse(json[index].attachments)[obj.attr('index')];
	//var extension = attachment.filename.replace(/^.*\./, '');
	var filename = attachment.filename.substr(0, attachment.filename.lastIndexOf('.'))
	var fileInfo  = attachment.filename.split('.');
	$('#attachment_name').val(fileInfo[0]);
	$('#attachment_index').val(obj.attr('index'));
	$('#attachment_guid').val(attachment.guid);
	$('#fai_id').val(fai_Id);
	$('#attachment_ext').html('.' + fileInfo[1]);
	$('#change_attachchment_name_modal').modal('show');
}

function send_attachment_name_change(){
	var _json = {};
	_json.filename = $('#attachment_name').val() + $('#attachment_ext').html();
	_json.index = $('#attachment_index').val();
	_json.guid = $('#attachment_guid').val();
	_json.prefix = $('#attachment_ext').html();
	_json.id = $('#fai_id').val();
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/change_attachment_name.cfm",
		data: JSON.stringify(_json),
		dataType: "json",
		success: function (data) {					
			if (data != null) {
				_json = JSON.parse(data);
				var index = json.map(function (id) { return id['id']}).indexOf(_json.id);
				var attachments;
				if(index >= 0){
					attachments = JSON.parse(json[index].attachments);
				}else{
					attachments = JSON.parse(json_fai.attachments);
				}//var attachments = JSON.parse(json[index].attachments);
				attachments[_json.index].filename = _json.filename;
				if(index >= 0){
					json[index].attachments = JSON.stringify(attachments);
				}else{
					json_fai.attachments = JSON.stringify(attachments);
				}//json[index].attachments = JSON.stringify(attachments);
				$('.file-li-filename-' + _json.index).html(_json.filename);
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
        }	   
	});		
}

function enable_disable_attachment(obj){
	var attachment;
	var index = json.map(function (id) { return id['id']}).indexOf(fai_Id);
	if(index >= 0){
		attachment = JSON.parse(json[index].attachments)[obj.attr('index')];
	}else{
		attachment = JSON.parse(json_fai.attachments)[obj.attr('index')];
	}
	var _json = {};
	_json.index = obj.attr('index');
	_json.guid = attachment.guid;
	_json.isactive = (attachment.isactive === '1' ? '0' : '1');
	_json.id = fai_Id;
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/enable_disable_attachment.cfm",
		data: JSON.stringify(_json),
		dataType: "json",
		success: function (data) {					
			var attachments;
			if (data != null) {
				_json = JSON.parse(data);
				var index = json.map(function (id) { return id['id']}).indexOf(_json.id);
				if(index >= 0){
					attachments = JSON.parse(json[index].attachments);
				}else{
					attachments = JSON.parse(json_fai.attachments);
				}
				attachments[_json.index].isactive = _json.isactive;
				if(index >= 0){
					json[index].attachments = JSON.stringify(attachments);
				}else{
					json_fai.attachments = JSON.stringify(attachments);
				}
				$('.file-li-enable-disable-' + _json.index).html(_json.isactive === '0' ? 'Set Active' : 'Set Inactive');
				if(_json.isactive === '0'){
					$('.file-li-filename-' + _json.index).addClass('file-li-anchor-inactive-color');
				}else{
					$('.file-li-filename-' + _json.index).removeClass('file-li-anchor-inactive-color');
				}
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
        }	   
	});		
	
}

function report_page(){
	isFAI = true;	
	$('#fai_search').addClass('hidden').removeClass('visible');
	$('#fai_report').removeClass('hidden').addClass('visible');
	$('.report-page').removeClass('hidden').addClass('visible');
	$('.search-page').addClass('hidden').removeClass('visible');
	//if($('#faistatus').val() == 'CLOSED')
	//	$('.report-btn').removeClass('hidden');
	//else
	//	if(!$('.report-btn').hasClass('hidden'))
	//		$('.report-btn').addClass('hidden');

}

function search_page(){
	if(changed_flag && !follow_through){
		console.log('search_page interrupt');
	}
	isFAI = false;
	json_email_resp = save_json_email_resp;
	$('#fai_search').removeClass('hidden');
	$('#fai_report').addClass('hidden');
	$('.report-page').addClass('hidden');
	$('.search-page').removeClass('hidden');
	$('.report-btn').addClass('hidden');
}

function show_request_fai_part_number_modal(){
	if(changed_flag && !follow_through){
		console.log('show_request_fai_part_number_modal interrupt');
	}
	$('#fai_supplier_id').val('');
	$('#request_fai_part-number_modal').modal('show');
}

function fpy_by_supplier(){
	if($('#fai_supplier_id').val() !== ''){
		fai_dropdown_filter('fpy');
	}
}

function fai_dropdown_filter(menu_request){
	if(changed_flag && !follow_through){
		console.log('fai_dropdown_filter interrupt');
	}
	var s_json = {};
	search_page();
	switch(menu_request){
		case 'wip':
			//$('.header_title').removeClass('sorted');
			//$('.sortable').removeClass('sort').removeClass('desc').removeClass('asc');																												
			//$('.sortable').addClass('sort');														
			//$('#h_datecompleted').removeClass('sort').addClass('desc');
			//$('#Sort_Order').val('desc');
			//$('#h_datecompleted' + ' span:first-child').addClass('sorted');
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (Work In Progress)');
			break;
		case 'newtickets':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (New Requests)');
			break;
		case 'wipnonpcb':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (Work In Progress Non PCB\'s)');
			break;
		case 'wippcb':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (Work In Progress PCBIN\'s)');
			break;
		case 'aqe':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (Awaiting QE\'s)');
			break;
		case 'cdb':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.fromdate = $('#cdb_start_date').val();
			s_json.todate = $('#cdb_end_date').val();
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (Closed the Day Before ' + $('#cdb_start_date').val() + ' - ' + $('#cdb_end_date').val() + ')');
			break;
		case 'ewip':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (Engineering WIP)');
			break;
		case 'rinc':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (RI not Complete)');
			break;
		case 'rc':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results (Read to Close)');
			break;
		case 'riic':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			$('#search_results_type').html('Search Results RI Inspection Complete)');
			break;
		case 'fpy':
			s_json.storedProcedure = '';
			s_json.sortorder = '';
			s_json.sortColumn = '';
			s_json.custom = menu_request;
			s_json.supplier = $('#fai_supplier_id').val();
			$('#search_results_type').html('Search Results FPY by Supplier)');
			break;
	}
	s_json.itemsperpage = $('#CSP_Per_Page').val();
	s_json.pagenumber = '1';
	leadFilter = JSON.stringify(s_json);
	console.log(s_json);
	page_fai_ajax(leadFilter);
}

function clear_pdf_form(){
	$('#faiID').val('');
	$('#fai_report_storedProcedure').val('');
	$('#fai_report_req').val('');
	$('#fai_report_from_date').val('');					
	$('#fai_report_to_date').val('');					
	$('#fai_report_sortorder').val('');	
	$('#fai_report_sortColumn').val('');
	$('#fai_report_custom').val('');
}

function fai_traveler(){
	if(!isFAI){
		var _json = {};
		_json.reporttype = 'fai_traveler_pdf';
		request_fai_modal(_json,'FAI Traveler');
		return;
	}
	clear_pdf_form();
	$('#faiID').val(fai_Id);
	$('#fai_report_storedProcedure').val('');
	$('#fai_report_req').val('fai_traveler');

	$('#pdfReport').submit();}

function complete_notice(){
	if(!isFAI){
		var _json = {};
		_json.reporttype = 'completion_notification_report_pdf';
		request_fai_modal(_json,'Completion Notification Report');
		return;
	}

	clear_pdf_form();
	$('#faiID').val(fai_Id);
	$('#fai_report_storedProcedure').val('');
	$('#fai_report_req').val('completion_notification_report');

	$('#pdfReport').submit();
}
function complete_notice_continue(id){
	clear_pdf_form();
	console.log(id);
	$('#faiID').val(id);
	$('#fai_report_storedProcedure').val('fai_select');
	$('#fai_report_req').val('completion_notification_report');
	$('#pdfReport').submit();
}

function fai_traveler_continue(id){
	clear_pdf_form();
	console.log(id);
	$('#faiID').val(id);
	$('#fai_report_storedProcedure').val('');
	$('#fai_report_req').val('fai_traveler');
	$('#pdfReport').submit();
}

function cdb_modal_continue(req){
	var _json = {};
	switch($('#cdb_req').val()){
		case 'filter':
			fai_dropdown_filter(req);
		break;
		case 'pdf':
			$('#fai_report_req').val('closed_the_day_before_report');
			$('#fai_report_from_date').val($('#cdb_start_date').val());					
			$('#fai_report_to_date').val($('#cdb_end_date').val())	
			$('#fai_report_storedProcedure').val('');	
			$('#fai_report_sortorder').val('');	
			$('#fai_report_sortColumn').val('');
			$('#fai_report_custom').val('cdb');
			$('#pdfReport').submit();
		break;
		case 'email':
			_json.id = '';
			_json.fromdate = $('#cdb_start_date').val();
			_json.todate =$('#cdb_end_date').val();
			_json.reporttype ='closed_the_day_before_report';
			_json.storedProcedure = '';	
			_json.sortorder = '';
			_json.sortColumn = '';
			_json.custom = '';
			json_email_report(_json);
		break;
	}
}

function fai_report(req){
	switch(req){
		case 'work_in_progress_report':
			$('#fai_report_req').val('work_in_progress_report');
			$('#fai_report_custom').val('wip');
			$('#pdfReport').submit();
			break;
		case 'completed_by_ri_date_report':
			$('#fai_report_req').val('completed_by_ri_date_report');
			$('#fai_report_custom').val('cbrid');
			$('#pdfReport').submit();
			break;
	}
}


function email_fai_report(req){
	var _json = {};
	switch(req){
		case 'work_in_progress_report':
			_json.id = '';
			_json.reporttype = req;
			_json.storedProcedure = '';	
			_json.sortorder = '';
			_json.sortColumn = '';
			_json.custom = 'wip';
			json_email_report(_json);
		break;
		case 'completed_by_ri_date_report':
			_json.id = '';
			_json.reporttype = req;
			_json.storedProcedure = '';	
			_json.sortorder = '';
			_json.sortColumn = '';
			_json.custom= '';
			json_email_report(_json);
		break;
		case 'awaiting_qe_report':
			_json.id = '';
			_json.reporttype = req;
			_json.storedProcedure = '';	
			_json.sortorder = '';
			_json.sortColumn = '';
			_json.custom = '';
			json_email_report(_json);
		break;
		case 'engineering_wip_report':
			_json.id = '';
			_json.reporttype = req;
			_json.storedProcedure = '';	
			_json.sortorder = '';
			_json.sortColumn = '';
			_json.custom = '';
			json_email_report(_json);
		break;
	}
}

function cdb_modal_display(req){
	if(changed_flag && !follow_through){
		console.log('cdb_modal_display interrupt');
	}
	var d = new Date();
	var dbf = new Date();
	dbf.setDate(dbf.getDate() - 1);
	$('#cdb_start_date').datepicker("setDate",dbf);
	$('#cdb_end_date').datepicker("setDate",d);
	$('#cdb_req').val(req);
	$('#cdb_modal').modal('show');
}

function new_fai(){
	fai_Id = '';
	json_fai.attachments = [];
	$('#fai_form input[type=text]').val('');
	$('#fai_form select').val('');
	$('#fai_form textarea').val('');
	$('#fai_form .selectpicker').selectpicker('deselectAll');	
	$('#fai_form .selectpicker').selectpicker('refresh');
	$('#fai_form #attachment_list').empty();
	$('#fileuploader').hide();
	$('#fai_form input[type=checkbox]').attr('checked', false);
	$('#fai_form #fileuploader_new_fai').css('display','inline-block');
	$('#fai_form #faistatus').val('IN WORK');
	$('#fai_form #id').val('new fai');
	$('.report-btn').addClass('hidden');
	report_page();
	$('.new-fai').addClass('hidden');
}

var update;
function update_fai(){
	 update = extract_form();
	 console.log('update_fai');
	 console.log(update);
	//return;
	 var server_page = '';
	 switch($('#fai_form #id').val()){
		 case 'new fai':
		 	update.id = '0';
			json_fai = JSON.parse(JSON.stringify(update));
			update.storedProcedure = 'fai_Insert';
			server_page = 'fai_insert.cfm';
			break;
		 default:
			update.storedProcedure = 'fai_Update';
			server_page = 'fai_save.cfm';
			break;
		 break;
	 }
	 ajax_form_update(update, server_page);
}

function ajax_form_update(update, server_page){
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/" + server_page,
		data: JSON.stringify(update),
		dataType: "json",
		success: function (data) {					
			if (data != null) {
				console.log(data);
				show_update_status(data);
				//send_email_alert();
				reset_change_flag()
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
        }	   
	});	
}

function send_email_alert(){	
		var json = {}
		json.type = 'serial number';
		
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "ajax/send_email_alert.cfm",
			data: JSON.stringify(json),
			dataType: "json",
			success: function (data) {					
				if (data != null) {
					console.log(data);
				}
				else{
				}
			},
			error: function (jqXHR, ajaxOptions, thrownError) {
				//$('#login_iframe').attr('src', 'login.cfm');
				//$('#login_modal').modal('show');
			}	   
		});	
}

function show_update_status(data){
	if(data.results == 'OK'){
		if(data.sqlrequest == 'update'){	
			display_status_message('FAI Report #'  + data.idenity + ' Updated');
			check_completion_report_btn();
			var i = json.map(function (id) { return id['id']; }).indexOf(fai_Id);
			if(i >= 0){
				$.each(update, function(key, value) {
					json[i][key] = value;
				});
				updateDTC(json[i]);
			}
			else{
				updateDTC(update);
			}
		}else{
		 	json_fai.attachments = '[]';
			json_fai.id = data.idenity
			json.push(json_fai);
			report_page();
			$('#fai_form #id').val(data.idenity);
			$('#fai_form #fileuploader_new_fai').css('display','none');
			$('#fileuploader').show();
			fai_Id = data.idenity;
			display_status_message('FAI Report #'  + data.idenity + ' Created');
			send_new_fai_alert_email(json_fai);			
			updateDTC(json_fai);
		}
	}	
}

function updateDTC(json){
	if(json.datereceived != '' && json.datecompleted != ''){
		var datereceived = new Date(json.datereceived);
		var datecompleted = new Date(json.datecompleted);
		var days = (datecompleted - datereceived) / (1000 * 60 * 60 * 24);
		$('#daystocomplete').val(Math.round(days));
	}
	else{
		$('#daystocomplete').val('');		
	}
}

function send_new_fai_alert_email(obj){
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "./ajax/send_new_fai_alert_email.cfm",
		data: JSON.stringify(obj),
		dataType: "json",
		success: function (data) {					
			if (data != null) {
				console.log(data);
			}else{}
		}   
	});	
}

function extract_form(){
	var update = {};
	$('#fai_form input').each(function(){
		switch($(this).prop('type')){
			case 'checkbox':
				if($(this).prop('id') != ''){
					update[$(this).prop('id')] = $(this).prop('checked') ? '1' : '0';
				}
				break;
			case 'text':
				if($(this).prop('id') != ''){
 					update[$(this).prop('id')] = $(this).val();
				}
				break;
		}
	});
	$('#fai_form textarea').each(function(){
 		update[$(this).prop('id')] = $(this).val();
	});
	$('#fai_form select').each(function(){
		switch($(this).prop('type')){
			case 'select-multiple':
				if($(this).val() != null)
 					update[$(this).prop('id')] = $(this).val().join(';');
				else
					update[$(this).prop('id')] = '';
				break;
			default:
 				update[$(this).prop('id')] = $(this).val() == null ? '' : $(this).val();
				break;
		}
	});
	console.log(update);
	return update;		
}

function check_completion_report_btn(){
	if($('#faistatus').val() == 'CLOSED' && $('.report-btn').hasClass('hidden'))
		$('.report-btn').removeClass('hidden');
	else
		if($('#faistatus').val() != 'CLOSED')
			$('.report-btn').addClass('hidden');
}

function display_status_message(msg, color){
	var color = (typeof color !== 'undefined') ?  color : '#230CF1';
	var sb = $('#status_box');
	sb.css('color',color);
	sb.html(msg);
	sb.show(1000);
	setTimeout(function(){sb.hide(1000);}, 3000);
}

var save_json_email_resp;
function blank_email(){
	save_json_email_resp = json_email_resp;
	empty_email_form();
	json_email_resp = {};
	json_email_resp.to = '';
	json_email_resp.from = '';
	json_email_resp.subject = '';
	json_email_resp.attachments = '';
	json_email_resp.emailcontent = '';
	json_email_resp.id = '';
	json_email_resp.results = '';
	json_email_resp.reporttype = 'custom';
	console.log(json_email_resp);
	$('#rte_modal').modal('show');
}

function empty_email_form(){
	$('.email_textarea').val(''); 
	$('#email_ul_attachments_list').empty();
	close_email_validation();
	CKEDITOR.instances.modal_rte_textarea.setData('');
}

function continue_with_report(){
	var _json = JSON.parse($('#json_fai_modal').val());
	switch(_json.reporttype){
		case 'completion_notification_report':
			_json.id = $('#fai_report_id').val();
			json_email_report(_json); 
			break;
		case 'completion_notification_report_pdf':
			var id = $('#fai_report_id').val(); 
			if(id != '')
				complete_notice_continue(id);
			break;
		case 'fai_traveler_pdf':
			var id = $('#fai_report_id').val(); 
			if(id != '')
				fai_traveler_continue(id);
			break;
	}
}

function request_fai_modal(obj, report){
	$('#fai_report_reqested').html(report);
	$('#json_fai_modal').val(JSON.stringify(obj));
	$('#fai_report_id').val('');
	$('#request_fai_modal').modal('show');		
	setTimeout(function(){$('#fai_report_id').focus();}, 500);
}

function email_completion_notification_report(){
	var _json = {};
	if(!isFAI || fai_Id == ''){
		_json.reporttype = 'completion_notification_report';
		request_fai_modal(_json,'Completion Notification Report');
		return;
	}
	_json.reporttype = 'completion_notification_report';
	_json.id = fai_Id;
	json_email_report(_json);
}

var json_email_resp;
function json_email_report(obj){
	console.log(obj);
	empty_email_form();
	$('#rte_modal').modal('show');
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/fai_reports.cfm",
		data: JSON.stringify(obj),
		dataType: "json",
		success: function (data) {		
			json_email_resp = '';			
			console.log(data);
			if (data != null) {
				if(data.results == 'OK'){
					json_email_resp = data;
					if(data.hasOwnProperty('attachments')){
						var arr = data.attachments.split(';');
						var attachments = '';
						var attach = '';
						$.each(arr, function(index, item) {
							if(item != ''){
							   	attachments = '<li><div style="width:15px; display:inline-block;"><i class="attachment_add_remove fa fa-check attachment-attach"></i></div>';
							   	attachments += '<span>' + item + '</span></li>';
								$('#email_ul_attachments_list').append(attachments);
								attach += (attach === '' ? '' : ';') + item;
							}
						});	
						$('#send_attachments').val(attach).change();
						
						$('.attachment_add_remove').mouseover(function(e){
							this.style.cursor='pointer';
						});
					
						$('.attachment_add_remove').click(function(e){
							var attachments = '';
							if($(this).hasClass('fa-check')){
								$(this).removeClass('fa-check attachment-attach').addClass('fa-times attachment-unattach');
							}else{
								$(this).removeClass('fa-times attachment-unattach').addClass('fa-check attachment-attach');
							}
							
							$('.attachment_add_remove.attachment-attach').each(function() {
								attachments += (attachments == '' ? '' : '; ') + $(this).parents('li').children('span').html();
							});	
							$('#send_attachments').val(attachments).change();
						});
						
					}
					$('#send_to_address').val(data.to).change();
					$('#send_cc_address').val(data.cc).change();
					$('#send_subject').val(data.subject).change();
					CKEDITOR.instances.modal_rte_textarea.setData(data.emailcontent);;
				}
				else{
				}
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
        }	   
	});			
}

function re_login(){
	location.href=location.href;
}

function logout(){
	$('#login_iframe').attr('src', 'login.cfm?reset=yes');
	$('#login_modal').modal('show');	
}

function admin(){
	$('.errormsg').html('');
	$( '.admin-menu').hide();
	$('#admin_modal').modal('show');	
	admin_menu('admin_manufacturers');
	$("#a_manufacturer option:first").attr('selected','selected');
	adminSelectChanged($('#a_manufacturer'));
}

function adminSelectChanged(obj){
	$('.errormsg').html('');
	if($(obj).val() == 'New'){
		$(obj).parent().next().find('input[type=text]:first').val('');
		$(obj).parent().next().find('input[type=text]:first').attr('previousValue','');
		$(obj).parent().next().find('.main').html('Add');
		$(obj).parent().next().find('.main').show();
		$(obj).parent().next().find('.delete').hide();
	}
	else{
		$(obj).parent().next().find('input[type=text]:first').val($(obj).val());
		$(obj).parent().next().find('input[type=text]:first').attr('previousValue',$(obj).val());
		if($(obj).attr('id') == 'a_intervals'){
			$(obj).parent().next().find('.main').html('Delete');
			$(obj).parent().next().find('.main').show();						
		}
		else{
			$(obj).parent().next().find('.main').html('Change');
			$(obj).parent().next().find('.main').show();
			$(obj).parent().next().find('.delete').show();			
		}
	}
}

function admin_menu(item){
	$('.errormsg').html('');
	$( '.admin-menu').hide();
	var obj = $('#a_manufacturer');
	$(obj).val('New');
	$(obj).parent().next().find('input[type=text]:first').val('');
	$(obj).parent().next().find('input[type=text]:first').attr('previousValue','');
	$( '#' + item ).toggle();	
}

var tmpobj;
function admin_submit(obj){
	var json_admin = {};
	json_admin.previousValue = $(obj).prevAll("input[type=text]").attr('previousValue');
	json_admin.table = $(obj).prevAll("input[type=text]").attr('table');
	json_admin.cmd = $(obj).html();
	json_admin.newvalue = $(obj).prevAll("input[type=text]").val();
	tmpobj = obj;
	//console.log(json_admin);
	//return;
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/admin.cfm",
		data: JSON.stringify(json_admin),
		dataType: "json",
		success: function (data) {			
			console.log(data);

			if(data.results == 'OK'){
				if(data.status == '1'){
					$(tmpobj).parent().find('.errormsg').html('Update Successful.');
					$(tmpobj).parent().find('.errormsg').css('color','#337ab7');
				}
				else{
					$(tmpobj).parent().find('.errormsg').html(data.status);
					$(tmpobj).parent().find('.errormsg').css('color','red');
					return;
				}
				var obj = data.dropdown;
				switch(data.table){
					case 'manufacturers':
						var a_manufacturer = data.cmd == 'Add' ? $('#a_manufacturer').val() : $('#a_manufacturer').val() == data.previousvalue ? data.newvalue : $('#a_manufacturer').val();
						//var s_manufacturer = data.cmd == 'Add' ? $('#s_manufacturer').val() : $('#s_manufacturer').val() == data.previousvalue ? data.newvalue : $('#s_manufacturer').val();
						var manufacturer = data.cmd == 'Add' ? $('#manufacturer').val() : $('#manufacturer').val() == data.previousvalue ? data.newvalue : $('#manufacturer').val();

						$('#a_manufacturer').find("option:gt(0)").remove();
						//$('#s_manufacturer').find("option:gt(0)").remove();
						//$('#manufacturer').find("option:gt(1)").remove();
						var list = $(".manufacturer");
						$.each(obj, function(index, item) {
						  list.append(new Option(item.supplier, item.supplier));
						});	
						
						list = $("#fai_form #distributormanufacturer");
						var selectpickerval = $('#fai_form #distributormanufacturer').selectpicker('val');
						list.find('option').not(':first').remove();
					    $.each(obj, function(index, item) {
						  list.append(new Option(item.supplier, item.supplier));
						});
						$('#fai_form #distributormanufacturer').selectpicker('refresh');
						if(data.cmd == 'Change'){
							if(selectpickerval == data.previousvalue.toUpperCase()){
								$('#fai_form #distributormanufacturer').selectpicker('val',data.newvalue.toUpperCase());
							}else
								$('#fai_form #distributormanufacturer').selectpicker('val',selectpickerval);
							
							$('#Cust_Results tbody tr').each(function (index, item) {
								col = $(item).find("td[col=supplier]")
								if (col.is("td") && col.html() == data.previousvalue.toUpperCase()) {
									col.html(data.newvalue.toUpperCase());
								}
							});
							$.each(json, function(index, item) {
							  if(item['distributormanufacturer'] == data.previousvalue.toUpperCase())
								  item['distributormanufacturer'] = data.newvalue.toUpperCase();
							});

							console.log(json);							
						}
						
						if(data.cmd == 'Delete'){
							if(selectpickerval == data.previousvalue.toUpperCase())
								$('#fai_form #distributormanufacturer').selectpicker('val','');
							else
								$('#fai_form #distributormanufacturer').selectpicker('val',selectpickerval);
						}
						if(data.cmd == 'Add'){
							$('#fai_form #distributormanufacturer').selectpicker('val',selectpickerval);
						}

						$('#a_manufacturer').val(a_manufacturer);
						json_dropdowns.suppliers = obj;
						break;
				}
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
        }	   
	});			
}