
<cfoutput>
<div class="container-fluid" style="min-height: 100%;position:relative;">
    <form action="" method="POST" id="fai_search_form" class="form-horizontal col-lg-12" role="form">
        <div class="row">
            <div class="form-col fai-header-cols col-xs-3">
               <div class="form-group">
                   <label for="id" class="control-label col-xs-2">ID:</label>
                    <div class="fai-input col-xs-7">
                        <input id="id" name="id" type="text" style="width:80px;">
                    </div>
                </div>
            </div>
            <div class="form-col fai-header-cols col-xs-4">
                <div class="form-group">
                   <label for="faistatus" class="control-label col-xs-5">STATUS:</label>
                    <div class="fai-input">
                        <select class="form-control col-xs-7" id="faistatus" name="faistatus" style="width:100px;">
                            <option value="ON HOLD">In Work</option>
                            <option value="ON HOLD">On Hold</option>
                            <option value="ON HOLD">Closed</option>
                            <option value="ON HOLD">Voided</option>
                        </select>
                    </div>
                </div>
            </div>
           <div class="form-col fai-header-cols col-xs-5">
                <div class="form-group">
                   <label for="daystocomplete" class="control-label col-xs-6">DAYS&nbsp;TO&nbsp;COMP:</label>
                    <div class="fai-input col-xs-6">
                        <input id="daystocomplete" name="daystocomplete" type="text" style="width:40px;">
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-bottom:3px;">
            <div class="form-col receitp-info col-xs-12">
            (Level 1) RECEIPT INFORMATION
            </div>
        </div>
    	<div class="row">
            <div class="form-col fai-cols">
                <div class="form-group">
                    <label for="inspector" class="control-label">INSPECTOR :</label>
                    <div class="fai-input">
                        <select class="form-control" id="inspector" name="inspector">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">DATE RECEIVED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">RI COMPLETE DATE :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="company" class="control-label">COMPANY :</label>
                    <div class="fai-input">
                       <select class="form-control" id="company" name="company">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">PROJECT :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="buyer" class="control-label">BUYER :</label>
                    <div class="fai-input">
                        <select class="form-control" id="buyer" name="buyer">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="projecteng" class="control-label">**PROJ ENG :</label>
                    <div class="fai-input">
                       <select class="form-control" id="projecteng" name="projecteng">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">PO ## :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">RMO NUMBER :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">ITEM NUMBER :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">REVISION :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">DESCRIPTION :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">LOT QTY RECEIVED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">QTY INSPECTED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="distributormanufacturer" class="control-label">DIST / MFG :</label>
                    <div class="fai-input">
                      <select class="form-control" id="distributormanufacturer" name="distributormanufacturer">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label">RI QTY REJECTED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="50" name="Lead_City" id="Lead_City" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="reasonforfai" class="control-label">REASON FOR FAI :</label>
                    <div class="fai-input">
                      <select class="form-control red-border" id="reasonforfai" name="reasonforfai">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="faitype" class="control-label">FAI TYPE :</label>
                    <div class="fai-input">
                      <select class="form-control red-border" id="faitype" name="faitype">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="failevel" class="control-label">FAI LEVEL :</label>
                    <div class="fai-input">
                      <select class="form-control red-border" id="failevel" name="failevel">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pilot" class="control-label">PILOT? :</label>
                    <div class="fai-input">
                        <input type="checkbox" name="pilot" id="pilot" class="form-control"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ripassfail" class="control-label">RI PASS / FAIL :</label>
                    <div class="fai-input">
                     <select class="form-control red-border" id="ripassfail" name="ripassfail">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                
            </div>

            <div class="form-col fai-right-cols  col-xs-6">
                <div class="form-group" style="margin-bottom: 0px;">
                    <div class="upper-right" style="height:195px">
                        <div id="dragandrophandler">Drag & Drop Attachments Here</div>
                        <div id="status1"></div>
                    </div>
                </div>
                <div class="form-group red-font" style="font-size:10px;">
                	<span class="footmark" style="position:absolute; left:-15px;">** ENTER PROJECT ENGINEER NAME FOR ENGINEERING EVALUATION REQUESTS</span>
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                	<div align="center" style="text-align:center;background-color:##7f7f7f; color:##fff;">INSPECTION RESULTS</div>               	           
                </div>
                <div class="form-group form-group-inspectionresults">
                	<textarea id="inspectionresults" class="inspectionresults" name="inspectionresults"> 
                    </textarea>            
                </div>
            </div>

        </div>
        <div class="row" style="padding-bottom:3px;">
            <div class="form-col smt-tha-results col-xs-12">
            (LEVEL 2) SMT - THA RESULTS
            </div>
        </div>
        <div class="row" style="padding-bottom:3px;">
            <div class="form-col mfg-test-results col-xs-12">
            (LEVEL 3) MANUFACTURING AND TEST RESULTS
            </div>
        </div>
        <div class="row" style="padding-bottom:3px;">
            <div class="form-col lab-test-results col-xs-12">
            (LEVEL 4) QA TEST LAB RESULTS
            </div>
        </div>
        <div class="row" style="padding-bottom:3px;">
            <div class="form-col eng-only col-xs-12">
            (WHEN REQUIRED ONLY) ENGINEERING
            </div>
        </div>

	</form>
</div>
</cfoutput>



