
  <style type="text/css">
  .nicEdit-main{
	  height:300px;
	  overflow:auto !important;
  }
  .email-send-over{
	  border:solid 1px #1E06F0;
  }
  
  .email-send-out{
	  border:solid 1px #B4AFAF;
  }

  </style>
<div style="vertical-align:top;">
    <div style="display:inline-block; vertical-align:top;">
        <div onClick="send_message()" class="email-send-out" 
            onMouseOut="$(this).removeClass('email-send-over').addClass('email-send-out');" 
            onMouseOver="this.style.cursor='pointer';$(this).removeClass('email-send-out').addClass('email-send-over');" 
            style="height:45px; width:45px; display:inline-block;text-align:center;font-size:12px;">
        	<div style="padding-top:15px;">Send</div>
        </div>
        <div style="display:inline-block; vertical-align:top;">
            <div style="height:20px; width:45px;border:solid 1px #B4AFAF;font-size:12px; margin-bottom:5px;text-align:center;">
                <div style="padding-top:4px;">To..</div></div>
            <div style="height:20px; width:45px;border:solid 1px #B4AFAF;font-size:12px; margin-bottom:5px;text-align:center;">
                <div style="padding-top:4px;">CC..</div></div>
            <div style="height:20px; width:45px;font-size:12px; margin-bottom:5px;text-align:center;">
                <div style="padding-top:4px;">Subject</div></div>
            <div style="height:20px; width:45px;font-size:12px;text-align:center; margin-bottom:12px;">
                <div style="padding-top:8px;">Attached</div></div>
        </div>
    </div>
    <div style="display:inline-block;">
            <div><input type="text" class="email-to" style="height:22px; max-width:600px; min-width:600px;margin-bottom:5px;"/></div>
            <div><input type="text" class="email-cc" style="height:22px; max-width:600px; min-width:600px;margin-bottom:5px;"/></div>
            <div><input type="text" class="email-subject" style="height:22px; max-width:600px; min-width:600px;margin-bottom:5px;"/></div>
            <div style="height:22px; max-width:598px; min-width:598px;border:solid 1px #B4AFAF;">
            <i class="fa fa-file-excel-o"></i>;
            <i class="fa fa-file-word-o"></i>;
            <i class="fa fa-file-pdf-o"></i>;
            <i class="fa fa-file-image-o"></i></div>        
    </div>

<textarea type="text" id="email_textarea" 
	style="overflow-y:auto;height:300px; max-width:699px;min-width:699px; margin-top:5px; border:solid 1px #B4AFAF;"></textarea>
</div>
<script type="text/javascript">
function send_message(){
	var json = {};
	json.msg = $('.nicEdit-main').html();
	json.to = $('.email-to').val();
	json.cc = $('.email-cc').val();
	json.subject = $('.email-subject').val();
	send_json_email(JSON.stringify(json));
}

function send_json_email(data){
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/send_email.cfm",
		data: data,
		dataType: "json",
		success: function (data) {					
			if (data != null) {
				console.log(data);
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
			//setTimeout(function(){location.href=location.href;}, 300);	   
        }
	});	
}
</script>