         <div class="row" style="padding-bottom:3px;">
            <div class="form-col mfg-test-results mfg-test-results-bgcolor col-xs-12">
            (LEVEL 3) MANUFACTURING AND TEST RESULTS
            </div>
         </div>
     	<div class="row" style="position:relative;">
        <div class="blockout_level_3 blockout_section"></div>
           	<div class="form-col fai-cols">
                <div class="form-group">
                    <label for="dateinmanufacturing" class="control-label mfg-test-results-bgcolor">DATE IN MFG :</label>
                    <div class="fai-input">
                         <input type="text" autocomplete="off" maxlength="100" name="dateinmanufacturing" id="dateinmanufacturing" class="level3 form-control input-date"/>                    </div>
                </div>
                <div class="form-group">
                    <label for="manufacturingcompletedate" class="control-label mfg-test-results-bgcolor">MFG COMPLETE DATE :</label>
                    <div class="fai-input">
                        <input type="text" autocomplete="off" maxlength="100" name="manufacturingcompletedate" id="manufacturingcompletedate" class="level3 form-control input-date"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="manfpassfail" class="control-label mfg-test-results-bgcolor">MFG PASS / FAIL :</label>
                    <div class="fai-input">
                        <select class="level3 form-control" id="manfpassfail" name="manfpassfail">
                           <option value="select"></option>
                            <option value="PASS">PASS</option>
                            <option value="FAIL">FAIL</option>
                        </select>
                    </div>
                </div>
                  <div class="form-group">
                        <label for="manufacturingnewpn" class="control-label mfg-test-results-bgcolor">MFG NEW PN :</label>
                        <div class="fai-input">
                            <input type="text" maxlength="100" name="manufacturingnewpn" id="manufacturingnewpn" class="level3 form-control fai-input-color"/>
                        </div>
                    </div>
                    
                     <div class="form-group">
                        <label for="manufacturingnewpn2" class="control-label mfg-test-results-bgcolor">MFG NEW PN 2 :</label>
                        <div class="fai-input">
                            <input type="text" maxlength="100" name="manufacturingnewpn2" id="manufacturingnewpn2" class="level3 form-control fai-input-color"/>
                        </div>
                    </div>
                
             </div>
             <div class="form-col fai-right-cols  col-xs-6">
                <div class="form-group" style="margin-bottom:0px;">
                	<textarea id="manfresults" class="level3-textarea mfg_test-results" name="manfresults" style="overflow-y: auto"> 
                    </textarea>            
                </div>
             </div>
        </div>
