<cfsetting showdebugoutput="no">

<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">
<cfparam name="URL.store" default="">
<cfparam name="User_IsLoggedIn" default="false">

<!--- Verify Session --->
<!---<cfset currentURL = "#CGI.SERVER_NAME#" & "#CGI.PATH_INFO#">
<cfdump var="#currentURL#">--->
<cfif 
	StructKeyExists(SESSION, "Session_UID") 
	AND 
	StructKeyExists(SESSION, "User_UID")
	AND 
	StructKeyExists(SESSION, "IsLoggedIn")>
	
	<cfscript>
		checkSession = CreateObject("component", "_cfcs.SessionMgmt");
		statsSession = checkSession.verifySession(Session_UID=SESSION.Session_UID,User_UID=SESSION.User_UID);
	</cfscript>
	
	<cfif statsSession EQ 1>
        <cfset User_IsLoggedIn = true>
	</cfif>
<cfelse>
	<cfset noRedirect = "Login">
	<cfif NOT ListFind(noRedirect, URL.pg)>
		<cflocation url="./?pg=Login" addtoken="no">
		<cfabort>
	</cfif>
</cfif>


<cfswitch expression="#URL.pg#">
     <cfcase value="Login">
		<cfif NOT User_IsLoggedIn>
            <cfinclude template="./security/_dat_login_prep.cfm">
            <cfswitch expression="#URL.st#">
                <cfcase value="Validate">
                    <cfinclude template="./security/_tmp_login_check.cfm">
                </cfcase>
                <cfdefaultcase>
                    <cfinclude template="./security/_tmp_login_form.cfm">
                </cfdefaultcase>
            </cfswitch>
        <cfelse>
        	<cfinclude template="./security/_tmp_logout.cfm">
        </cfif>
    </cfcase>
    <cfcase value="Logout">
        <cfinclude template="./security/_tmp_logout.cfm">
    </cfcase>
    <cfcase value="faiReport">
		<cfinclude template="./serverSettings.cfm">
		<cfinclude template="./_tmp_HTML_Header.cfm">
		<cfinclude template="./_tmp_HTML_Navigation_LoggedIn.cfm">

		<cfinclude template="./_tmp_fai_display.cfm">
		<cfinclude template="./_tmp_HTML_Footer.cfm">
		<cfinclude template="./_tmp_fai_modal.cfm">
     </cfcase>
    <cfcase value="">
     	<cfset User_IsLoggedIn = false>
        <cflocation url="./?pg=Login" addtoken="no">
         <cfabort>
	</cfcase>
    <cfdefaultcase>
    	<cfif NOT User_IsLoggedIn>
            <cflocation url="./?pg=Login" addtoken="no">
            <cfabort>
        <cfelse>
        	<cflocation url="./?pg=faiReport" addtoken="no">
        </cfif>
    </cfdefaultcase>
</cfswitch>
