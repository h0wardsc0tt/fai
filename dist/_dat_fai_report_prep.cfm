<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="20">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">
<cfparam name="VARIABLES.Sort_Column" default="[DATE COMPLETED]">
<cfparam name="VARIABLES.Sort_Order" default="desc">
<cfparam name="VARIABLES.lookType" default="Search Results">

<cfparam name="FORM.Lead_Name" default="">
<cfparam name="FORM.Lead_City" default="">
<cfparam name="FORM.Lead_State" default="">
<cfparam name="FORM.Lead_Zip" default="">
<cfparam name="FORM.Lead_Email" default="">
<cfparam name="FORM.Lead_Company" default="">
<cfparam name="FORM.Lead_StoreNumber" default="">
<cfparam name="FORM.Lead_Interest" default="">
<cfparam name="FORM.Lead_Phone" default="">
<cfparam name="FORM.Lead_Type" default="">
<cfparam name="FORM.Lead_Date_To" default="">
<cfparam name="FORM.Lead_Date_From" default="">

<cfparam name="FORM.Sort_Column" default="h_datecompleted">
<cfparam name="FORM.Sort_Order" default="desc">

<cfscript>
	obj = CreateObject("component", "_cfcs.fai");
	serializer = new lib.JsonSerializer();

	CustomerCount = 0;
	leads_json = '';
	dropDowns_json = {};
	
	faiFilter = StructNew();
	faiFilter['storedProcedure'] = 'dbo.[fai_projEng]';
	projEngsReturn = obj.DropDowns(argumentCollection = faiFilter);
	
	faiFilter['storedProcedure'] = 'dbo.[fai_buyer]';
	buyersReturn = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[fai_suppliers]';
	suppliersReturn = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[fai_approvedby]';
	approvedbyReturn = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[fai_company]';
	companiesReturn = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[fai_inspector]';
	inspectorsReturn = obj.DropDowns(argumentCollection = faiFilter);

	faiFilter['storedProcedure'] = 'dbo.[fai_newtickets]';
	ticketsReturn = obj.DropDowns(argumentCollection = faiFilter);


	dropDowns = StructNew();	
	dropDowns['projEngs'] = projEngsReturn;
	dropDowns['buyers'] = buyersReturn;		
	dropDowns['suppliers'] = suppliersReturn;		
	dropDowns['approvedby'] = approvedbyReturn;		
	dropDowns['companyies'] = companiesReturn ;		
	dropDowns['inspectors'] = inspectorsReturn ;		
 	dropDowns['tickets'] = ticketsReturn ;		
    dropDowns_json = serializer.serialize(StructCopy(dropDowns));
 	StructClear(dropDowns);	
		
	faiFilter['storedProcedure'] = '';
	faiFilter['sortColumn'] = '';
	faiFilter['pageNumber'] = FORM.CSP_Current_Page;
	faiFilter['itemsPerPage'] = FORM.CSP_Per_Page;
	faiFilter['custom'] = 'wip';
	returnSelect = obj.custom(argumentCollection = faiFilter);

	leadFilter_json = serializer.serialize(StructCopy(faiFilter));
    leads_json = serializer.serialize(returnSelect);
	returnSelectResult = DeserializeJSON(SerializeJSON(returnSelect, 'true'));
	//writedump(returnSelectResult);
 	StructClear(faiFilter);		
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
	VARIABLES.Sort_Column = FORM.Sort_Column;
	VARIABLES.Sort_Order = FORM.Sort_Order;	
	VARIABLES.lookType = "Search Results (Work In Progress)";
	
	//hme_users_json = serializer.serialize(hme_users);
	//hme_departments_json = serializer.serialize(hme_departments);
	//hme_offices_json = serializer.serialize(hme_offices);
	
	fai_security = "0";
	
	/*fai_security = "1";
	switch(SESSION.Department){
		case 'Quality Assurance':
			fai_security = "0";
			break;
		case 'Engineering':
		case 'Manufacturing Engineering':
			fai_security = "2";
			break;
		case 'Information Technologies':
			//fai_security = "0";
			break;
		case 'Purchasing':
			fai_security = "3";
			break;
		case 'PCA':
		case 'Manufacturing':
		case 'Manufacturing Engineering MWM (654010)':
		case 'Manufacturing-Engineering MWM':
			fai_security = "4";
			break;
	}
	if("#SESSION.User_UID#" == "hscott"){
		fai_security = "0";
	}
	if("#SESSION.User_UID#" == "rfretz"){
		fai_security = "0";
	}
	if("#SESSION.User_UIDr#" == "jmcgraw"){
		fai_security = "4";
	}
	if("#SESSION.User_UID#" == "mvuong"){
		fai_security = "2";
	}
	if("#SESSION.User_UID#" == "jgillespie"){
		fai_security = "5";
	}
	if("#SESSION.User_UID#" == "scarter"){
		fai_security = "6";
	}*/
	
</cfscript>




