<!doctype html>
<html>
<head>
<script src="/js/1.12.0_jquery.min.js" type="text/javascript"></script>
<script>
var wip = {};
wip.emailcontent = '<div style="margin-left:5px; margin-right:5px"><p>Good Morning,</p><p>The list of items currently in First Article process is attached. PCB FAIs are also listed below.</p><p>If you have any questions or updates, please do not hesitate to contact myself or Michael Howard.</p><p>Regards,</p><p>David Day<br />QA Inspection Leader<br />x4058</p></div>';
	
wip.to="Materials@hme.com,QualityAssurance@hme.com,Gpapas@hme.com,mrimorin@hme.com,kmccardle@hme.com,triches@hme.com,alopez@hme.com,cnegrete@hme.com";
wip.cc = "hscott@hme.com";

wip.subject = "FAI's Currently In Work";
wip.attachments = "Work In Progress";
wip.custom = "wip";
wip.reporttype = "work_in_progress_report";
wip.sortcolumn = "";
wip.sortorder = "";
wip.storedprocedure = "";
wip.results = "OK";
wip.id = "";
	
$.ajax({
	type: "POST",
	contentType: "application/json; charset=utf-8",
	url: "ajax/send_email.cfm",
	data: JSON.stringify(wip),
	dataType: "json",
	success: function (data) {					
		//console.log(data);
		if (data != null) {
			if(data.results == "OK"){
			}
			else{
			}
		}
		else{
		}
	},
	error: function (jqXHR, ajaxOptions, thrownError) {
	}
});	
</script>
<meta charset="utf-8">
<title></title>
</head>

<body>
</body>
</html>
