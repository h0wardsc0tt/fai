<cfinclude template="./emailAddressSettings.cfm">
 <cfset subject ="Closed the day before report">
 <cfset to_addrs = #emailAddressToSendTo#>
 <cfset cc_addrs = "">

<cfsavecontent variable = "html"> 
<div style="margin:0px 5px 0px 5px;">
	<p style="font-size:12px;">Good morning,</p>
	<p style="font-size:12px;">Attached, please find the completed FAI # 2730 report for PN 369G092 Rev A GOT INTERFACE in which the item <span style="color:#F3090D;">FAILED</span>.</p>
	<p style="font-size:12px;">Final Disposition: <span style="color:#F3090D;">Failed DMR 309670</span></p>
	<p style="font-size:12px;">Recommended: <span style="font-weight:bold;">Accept as Nonconforming, Notify Supplier</span></p>
	<p style="font-size:12px;">Thanks,</p>
	<p style="font-size:12px;">David Day<br />x4058</p>
 </div>
</cfsavecontent>
<cfset emailcontent = html>