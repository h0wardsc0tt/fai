<cfinclude template="/serverSettings.cfm">
<cfsetting showdebugoutput="yes">


<cfset tmp_file_name = "#GetTempDirectory()#/#CreateUUID()#.pdf">
<cfset logoImage ='/images/hme-companies.png'>

<cfset results = "OK">
<cftry>
    <cfmail
      to="hscott@hme.com" cc="hscott@hme.com" from="no-reply@hme.com" subject="FAI's Currently In Work" type="html"> 
      <!---to="Materials@hme.com,QualityAssurance@hme.com,Gpapas@hme.com,mrimorin@hme.com,kmccardle@hme.com,triches@hme.com,alopez@hme.com,cnegrete@hme.com" cc="hscott@hme.com" from="no-reply@hme.com" subject="FAI's Currently In Work" type="html"> 
--->       
       <p> 
             <img src="cid:logo" alt="" /><br /> 
             <span style="font-size:15px; color:##CCC8C8;">First Article Inspection</span><br>
       </p> 
    
        <div style="margin-left:5px; margin-right:5px"><p>Good Morning,</p><p>The list of items currently in First Article process is attached. PCB FAIs are also listed below.</p><p>If you have any questions or updates, please do not hesitate to contact myself or Michael Howard.</p><p>Regards,</p><p>David Day<br />QA Inspection Leader<br />x4058</p></div>
        <cfmailparam  
             file="#ExpandPath('/images/hme-companies.png')#" 
             contentid="logo"  
             disposition="inline" 
        /> 
        <cfinclude template="/_dat_wip_report.cfm">
        <cfmailparam file = "#tmp_file_name#"remove = "yes"> 
    </cfmail>
    <cfcatch type="Application">
    	<cfset results = #cfcatch.detail#>
    </cfcatch>
</cftry>
