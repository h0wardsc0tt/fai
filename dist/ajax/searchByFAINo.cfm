<cfinclude template="../serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfscript>		
	returnSelect = StructNew();
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	jsonFAI = deserializeJSON(ToString(getHTTPRequestData().content));
    	if (IsDefined("jsonFAI.id") AND IsNumeric(jsonFAI.id)) {
			obj = CreateObject("component", "_cfcs.fai");
			returnSelect = obj.searchByFAINo(argumentCollection = jsonFAI);
			StructClear(jsonFAI);
		}
	}
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnSelect));
	//StructClear(returnSelect);		
</cfscript>
