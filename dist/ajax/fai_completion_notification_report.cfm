<!---
  <cfdocument format="PDF" filename="file.pdf" overwrite="Yes"
    marginLeft = ".1" 
    marginTop = ".1"
    marginRight = ".1"  >
    <cfdocumentitem type="footer"> 
    	<cfoutput>

            <table style="font-size:10px; color:##C3B8B8; width:100%;"><tr>
            <td align="left">
                HME 555006
            </td>
            <td align="center">
               HME CONFIDENTAL AND PROPRIETARY INFORMATION
            </td>
            <td align="right">
                Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#
            </td>
            </tr>
            </table>

		</cfoutput> 
     </cfdocumentitem>
 --->
 <cfinclude template="../serverSettings.cfm">
  
 <cfinclude template="../serverSettings.cfm">
  <cfdocument format="PDF" filename="#GetTempDirectory()#/FAI_#fai.id#_Completion_Notification.pdf" overwrite="Yes"
    marginLeft = ".1" 
    marginTop = ".1"
    marginRight = ".1"  >
    <cfdocumentitem type="footer"> 
    	<cfoutput>

            <table style="font-size:10px; color:##C3B8B8; width:100%;"><tr>
            <td align="left">
                HME 555006
            </td>
            <td align="center">
               HME CONFIDENTAL AND PROPRIETARY INFORMATION
            </td>
            <td align="right">
                Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#
            </td>
            </tr>
            </table>

		</cfoutput> 
     </cfdocumentitem> 

  <!--- Set Defaults --->
<cfset todayDate = Now()> 

<cfscript>		
		//obj = CreateObject("component", "_cfcs.fai");
		//json = StructNew();
		//json["id"] = "2664";//"2585";
		//fai = obj.searchByFAINo(argumentCollection = json);
		line_break = '';
		attachments = '';
		attachments_struct = deserializeJson(fai.attachments);
		for(i = 1; i LTE ArrayLen(attachments_struct); i++){
       		attachments = attachments & line_break & attachments_struct[i].filename;  
			line_break = '<br>';      
        }
		supplierreport = 'Yes';
		if(fai.supplierreport == 0)
			supplierreport = 'No';	
		ripassfail = ' fail';
		if(fai.ripassfail == 'PASS')
			ripassfail = ' pass';
		finalresults = '';
		if(fai.finalresults == 'PASSED')
			finalresults = ' pass';
		if(fai.finalresults == 'PASSED WITH CONDITIONS')
			finalresults = ' pass';
		if(fai.finalresults == 'FAILED')
			finalresults = ' fail';

</cfscript>


<cfoutput>

<style>

.bold{
	font-weight:bold;
	font-size:15px;
}

.title{
	color:##0813E9;
	font-size:13px;
}

.title2{
	color:##0813E9;
	font-size:13px;
}
.info{
	color:##000;
	font-size:13px;
	/*border:solid 1px red;*/
}
.width1{

	width:100px;
}

.po-color{
	background-color:##e6b9b8;
}

.ri-color{
	background-color:##CBC5C5;
}

.smt-color{
	background-color:##c4bd97;
}

.mfg-color{
	background-color:##fac090;
}

.eng-color{
	background-color:##000;
}

.eng-font-color{
	color:##EBF106;
}

.qa-color{
	background-color:##c3d69b;
}

.fr-color{
	background-color:##9A9696;
}

.title-dev{
	padding:2px 0px 1px 5px;
}

.val-dev{
	border:solid 1px ##D0C8C8; 
	padding:0px 0px 0px 1px;
	margin:1px 0px 1px 0px;
	
}

.first{
	margin:2px 0px 1px 0px;
}

.fail{
	color:##F00609;
}

.pass{
	color:##06F430;
}

.right-val_lenght{
	width:136px;
}


.right-val_lenght2{
	width:141px;
}
</style>
    
<!--- Customer Search Form --->
<body>
<form action="" method="POST" id="FAI_Report_Form" class="form-horizontal col-lg-12" role="form">
	<div align="center" style="font-size:25px;color:##0813E9;">FIRST ARTICLE INSPECTION REPORT</div>
    <div class="form-group" style="background-color:##CBC5C5; text-align:center; margin:0px 5px 10px 5px;">
        <span style="color:##333351;">#DateFormat(todayDate, "full")#&nbsp;&nbsp;&nbsp;</span>
        <span style="color:##333351;">#TimeFormat(todayDate, "full")#</span>
    </div>
    <div class="form-group" style="margin:0px 5px 10px 5px;">
    <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; width:100%;">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                    	<td style="width:10px; padding:0; margin:0;"></td>
                        <td class="title2" style="padding:0px 5px 0px 5px;">ID: </td>
                        <td class="info" style="width:200px;">#fai.id#&nbsp;</td>
                        <td style="width:10px;">
                        <td class="title2" style="padding:0px 5px 0px 0px;">COMPANY:</td>
                        <td class="info" style="width:200px;">#fai.company1#&nbsp;</td>
                        <td style="width:10px;">
                        <td  class="title2" style="padding:0px 5px 0px 0px;">STATUS: </td>
                        <td class="info">#fai.faistatus#&nbsp;</td>
                   </tr>
                </table>
             </td>
         </tr>
        <tr>
            <td align="center" style="background-color:##CBC5C5; height:3px;"></td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                    	<td style="width:10px;"></td>
                        <td class="title2" style="padding-left:5px; width:210px;">REASON FOR FAI</td>
                        <td class="info" style="width:210px;"><div class="val-dev">#fai.reasonforfai#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title2" style="width:160px;">DAYS TO COMPLETE</td>
                        <td class="info right-val_lenght2"><div class="val-dev">#fai.daystocomplete#&nbsp;</div></td>
                   </tr>
                   <tr>
                    	<td style="width:10px;"></td>
                        <td class="title2" style="padding-left:5px;">FAI TYPE</td>
                        <td class="info"><div class="val-dev">#fai.faitype#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title2">SUPPLIER REPORT</td>
                        <td class="info"><div class="val-dev">#supplierreport#</div></td>
                   </tr>
                   <tr>
                    	<td style="width:10px;"></td>
                        <td class="title2" style="padding-left:5px;">FAI LEVEL</td>
                        <td colspan="4" class="info"><div class="val-dev">#fai.failevel#&nbsp;</div></td>

                   </tr>
                   <tr>
                    	<td style="width:10px;"></td>
                        <td class="title2" valign="top" style="padding-left:5px;">ATTACHMENTS</td>
                        <td colspan="4" class="info"><div class="val-dev">#attachments#&nbsp;</div></td>

                   </tr>
                </table>
             </td>
       </tr>
        <tr>
            <td style="height:5px;"></td>
        </tr>
        <tr>
            <td align="center" style="background-color:##e6b9b8;color:##0813E9;">PO RECEIPT INFORMATION</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                        <td class="title" style="width:210px; padding:0;margin:0;"><div class="po-color title-dev">DATE RECEIVED</div></td>
						<td style="width:10px;">
                        <td class="info" style="width:210px;"><div class="val-dev first">#fai.datereceived#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title" style="width:160px;"><div class="po-color title-dev">PO ##</div></td>
 						<td style="width:10px;">
                       <td class="info right-val_lenght"><div class="val-dev first">#fai.po#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td class="title"><div class="po-color title-dev">ITEM NUMBER</div></td>
						<td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.itemnumber#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title"><div class="po-color title-dev">RMO NUMBER</div></td>
 						<td style="width:10px;">
                       <td class="info"><div class="val-dev">#fai.rmonumber#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td class="title"><div class="po-color title-dev">REVISION</div></td>
						<td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.revision#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title"><div class="po-color title-dev">BUYER</div></td>
                        <td style="width:10px;">
                       <td class="info"><div class="val-dev">#fai.buyer#&nbsp;</div></td>

                   </tr>
                   <tr>
                        <td class="title"><div class="po-color title-dev">DISTRIBUTOR /MFG</div></td>
                        <td style="width:10px;">
                        <td colspan="5" class="info"><div class="val-dev">#fai.distributormanufacturer#&nbsp;</div></td>

                   </tr>
                   <tr>
                        <td class="title" valign="top"><div class="po-color title-dev">DESCRIPTION</div></td>
                        <td style="width:10px;">
                        <td colspan="5" class="info"><div class="val-dev">#fai.description#&nbsp;</div></td>

                   </tr>
                </table>
             </td>
       </tr>       
        <tr>
            <td style="height:5px;"></td>
        </tr>
        <tr>
            <td align="center" style="background-color:##CBC5C5;color:##0813E9;">(LEVEL 1) RECEIVING INSPECTION RESULTS</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                        <td class="title" style="width:210px; padding:0;margin:0;"><div class="ri-color title-dev">INSPECTOR</div></td>
                        <td style="width:10px;">
                        <td class="info" style="width:210px;"><div class="val-dev first">#fai.inspector#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title" style="width:160px;"><div class="ri-color title-dev">DATE RECEIVED</div></td>
                        <td style="width:10px;">
                        <td class="info right-val_lenght"><div class="val-dev">#fai.datereceived#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td class="title"><div class="ri-color title-dev">RI COMPLETE DATE</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.faitype#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title"><div class="ri-color title-dev">LOT QTY RECD</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.lotqtyrecd#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td class="title"><div class="ri-color title-dev">RI QTY INSPECTED</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.ricompletedate#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title"><div class="ri-color title-dev">RI QTY REJECTED</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.riqtyrejected#&nbsp;</div></td>

                   </tr>
                   <tr>
                        <td class="title"><div class="ri-color title-dev">RI PASS / FAI</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev #ripassfail#">#fai.ripassfail#</div></td>

                   </tr>
                   <tr>
                        <td class="title" valign="top" style="padding-top:1px;"><div class="ri-color title-dev">INSPECTION RESULTS</div></td>
                        <td style="width:10px;">
                        <td colspan="5" class="info" style="width:500px;">
                        	<div class="val-dev">#REReplace(fai.rievalutionresults,"\r\n|\n\r|\n|\r","<br>","all")#&nbsp;</div></td>

                   </tr>
                </table>
             </td>
       </tr> 
        <tr>
            <td style="height:5px;"></td>
        </tr>
       <tr>
            <td align="center" style="background-color:##c4bd97;color:##0813E9;">(LEVEL 2) SMT - THA RESULTS</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                        <td class="title" style="width:210px;"><div class="smt-color title-dev">DATE IN SMT</div></td>
                        <td style="width:10px;">
                        <td class="info" style="width:210px;"><div class="val-dev first">#fai.dateinsmt#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title" style="width:160px;"><div class="smt-color title-dev">SMT COMPLETE DATE</div></td>
                        <td style="width:10px;">
                        <td class="info right-val_lenght"><div class="val-dev">#fai.smtcompletedate#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td class="title"><div class="smt-color title-dev">SMT NEW PN</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.smtnewpn#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title"><div class="smt-color title-dev">SMT PASS / FAI</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.smtpassfail#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td  class="title" valign="top" ><div class="smt-color title-dev">SMT - THA RESULTS</div></td>
                        <td style="width:10px;">
                        <td colspan="5" class="info" style="width:500px;">
                        	<div class="val-dev">#REReplace(fai.smtresults,"\r\n|\n\r|\n|\r","<br>","all")#&nbsp;</div></td>

                   </tr>
                </table>
             </td>
       </tr> 
        <tr>
            <td style="height:5px;"></td>
        </tr>
       <tr>
            <td align="center" style="background-color:##fac090;color:##0813E9;">(LEVEL 3) MANUFACTURING AND TEST RESULTS</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                        <td class="title" style="width:210px;"><div class="mfg-color title-dev">DATE IN MANUFACTURING</div></td>
                        <td style="width:10px;">
                        <td class="info" style="width:210px;"><div class="val-dev first">#fai.dateinmanufacturing#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title" style="width:160px;"><div class="mfg-color title-dev">MFG COMPLETE DATE</div></td>
                        <td style="width:10px;">
                        <td class="info right-val_lenght"><div class="val-dev">#fai.manufacturingcompletedate#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td class="title"><div class="mfg-color title-dev">MANUFACTURING NEW PN</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.manufacturingnewpn#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title"><div class="mfg-color title-dev">MFG PASS / FAIL</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.manfpassfail#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td  class="title" valign="top"><div class="mfg-color title-dev">MANUFACTURING RESULTS</div></td>
                        <td style="width:10px;">
                        <td colspan="5" class="info" style="width:500px;">
                        	<div class="val-dev">#REReplace(fai.manfresults,"\r\n|\n\r|\n|\r","<br>","all")#&nbsp;</div></td>

                   </tr>
                </table>
             </td>
       </tr>        
        <tr>
            <td style="height:5px;"></td>
        </tr>
        <tr>
            <td align="center" style="background-color:##c3d69b;color:##0813E9;">(LEVEL 4) QA TEST LAB RESULTS</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                        <td class="title" style="width:210px;"><div class="qa-color title-dev">DATE IN QA LAB</div></td>
                        <td style="width:10px;">
                        <td class="info" style="width:210px;"><div class="val-dev first">#fai.dateinqalab#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title" style="width:160px;"><div class="qa-color title-dev">QA LAB DATE COMP</div></td>
                        <td style="width:10px;">
                        <td class="info right-val_lenght"><div class="val-dev">#fai.qalabdatecomplete#&nbsp;</div></td>
                     </tr>
                     <tr>   
                        <td class="title"><div class="qa-color title-dev">QA TEST LAB PASS / FAIL</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.qatestlabpassfail#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title"><div class="qa-color title-dev">QA LAB REPORT ##</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.qatestlabreport#&nbsp;</div></td>
                   </tr>
                 </table>
             </td>
       </tr>        
        <tr>
            <td style="height:5px;"></td>
        </tr>
       <tr>
            <td align="center" style="background-color:##000;color:##EBF106;">(WHEN REQUIRED ONLY) ENGINEERING REVIEW</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                        <td class="title" style="width:210px;"><div class="eng-color title-dev eng-font-color">DATE IN ENGINEERING</div></td>
                        <td style="width:10px;">
                        <td class="info" style="width:210px;"><div class="val-dev first">#fai.dateinengineering#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title" style="width:160px;"><div class="eng-color title-dev eng-font-color">ENG DATE COMPLETE</div></td>
                        <td style="width:10px;">
                        <td class="info right-val_lenght"><div class="val-dev">#fai.engineeringdatecomplete#&nbsp;</div></td>
                     </tr>
                     <tr>   
                        <td class="title"><div class="eng-color title-dev eng-font-color">ENG APPROVER</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev">#fai.engapprover#</td>
                   </tr>
                   <tr>
                        <td  class="title" valign="top"><div class="eng-color title-dev eng-font-color">ENGINEERING RESULTS</div></td>
                        <td style="width:10px;">
                        <td colspan="5" class="info" style="width:500px;"><div class="val-dev">#fai.engineeringresults#&nbsp;</div></td>

                   </tr>
                 </table>
             </td>
       </tr>        
        <tr>
            <td style="height:5px;"></td>
        </tr>
      <tr>
            <td align="center" style="background-color:##9A9696;color:##0813E9;">FINAL RESULTS</td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="padding:0; margin:0;">
                    <tr>
                        <td class="title" style="width:210px;"><div class="fr-color title-dev">APPROVED BY</div></td>
                        <td style="width:10px;">
                        <td class="info" style="width:210px;"><div class="val-dev first">#fai.approvedby#&nbsp;</div></td>
                        <td style="width:10px;">
                        <td class="title" style="width:160px;"><div class="fr-color title-dev">DATE COMPLETED</div></td>
                        <td style="width:10px;">
                        <td class="info right-val_lenght"><div class="val-dev">#fai.datecompleted#&nbsp;</div></td>
                     </tr>
                     <tr>   
                        <td class="title"><div class="fr-color title-dev">FINAL RESULTS</div></td>
                        <td style="width:10px;">
                        <td class="info"><div class="val-dev#finalresults#">#fai.finalresults#&nbsp;</div></td>
                   </tr>
                   <tr>
                        <td  class="title" valign="top" style="padding-top:1px;"><div class="fr-color title-dev">COMMENTS</div></td>
                        <td style="width:10px;">
                        <td colspan="5" class="info" style="width:500px;">
                        <div class="val-dev">#REReplace(fai.closurecomments,"\r\n|\n\r|\n|\r","<br>","all")#&nbsp;</div></td>

                   </tr>
                 </table>
             </td>
       </tr>      
    </table>
    </div>
</form>
<div class="col-lg-12">&nbsp;</div> 
</body>
</cfoutput> 
</cfdocument>


<!--- 

<cfheader name="Content-Disposition" value="attachment;filename=FAI #fai.id# Completion Notification.pdf">
<cfcontent type="application/octet-stream" file="#expandPath('.')#\file.pdf" deletefile="Yes">

 --->

