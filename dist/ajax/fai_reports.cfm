<cfinclude template="../serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfset jsonFAI = deserializeJSON(ToString(getHTTPRequestData().content))>
<cfscript>		
	//if (#FindNoCase('application/json', cgi.content_type)# > 0)
	//{
   		//jsonFAI = deserializeJSON(ToString(getHTTPRequestData().content));	
		obj = CreateObject("component", "_cfcs.faiReports");
		switch(jsonFAI.reporttype){
			case 'completion_notification_report':
				returnObj = obj.report_Attachment(argumentCollection = jsonFAI);
				returnEmailInfo = obj.GenerateReport(argumentCollection = jsonFAI);
				returnEmailInfo["attachments"] = returnObj.attachments;// & 'Completion Notification Report.pdf';
				returnEmailInfo["id"] = jsonFAI.id;
				returnEmailInfo["reporttype"] = jsonFAI.reporttype;
				break;
			case 'closed_the_day_before_report':
				json = StructNew();
				json['reporttype'] = jsonFAI.reporttype;
				json['fromdate'] = jsonFAI.fromdate;
				json['todate'] = jsonFAI.todate;
				returnObj = obj.GenerateReport(argumentCollection = json);
				StructClear(json);
				jsonFAI['subject'] = returnObj.subject;
				jsonFAI['emailcontent'] = returnObj.emailcontent;
				jsonFAI['to'] = returnObj.to;
				jsonFAI['cc'] = returnObj.cc;
				jsonFAI['results'] = returnObj.results;
				jsonFAI['attachments'] =  "";
				returnEmailInfo = StructCopy(jsonFAI);
				break;
			case 'work_in_progress_report':
				json = StructNew();
				json['reporttype'] = jsonFAI.reporttype;
				returnObj = obj.GenerateReport(argumentCollection = json);
				StructClear(json);
				jsonFAI['subject'] = returnObj.subject;
				jsonFAI['emailcontent'] = returnObj.emailcontent;
				jsonFAI['to'] = returnObj.to;
				jsonFAI['cc'] = returnObj.cc;
				jsonFAI['results'] = returnObj.results;
				jsonFAI['attachments'] =  "Work In Progress";
				returnEmailInfo = StructCopy(jsonFAI);
				break;
			case 'awaiting_qe_report':
				json = StructNew();
				json['reporttype'] = jsonFAI.reporttype;
				returnObj = obj.GenerateReport(argumentCollection = json);
				StructClear(json);
				jsonFAI['subject'] = returnObj.subject;
				jsonFAI['emailcontent'] = returnObj.emailcontent;
				jsonFAI['to'] = returnObj.to;
				jsonFAI['cc'] = returnObj.cc;
				jsonFAI['results'] = returnObj.results;
				jsonFAI['attachments'] =  "Awaiting QE's";
				returnEmailInfo = StructCopy(jsonFAI);
				break;
			case 'engineering_wip_report':
				json = StructNew();
				json['reporttype'] = jsonFAI.reporttype;
				returnObj = obj.GenerateReport(argumentCollection = json);
				StructClear(json);
				jsonFAI['subject'] = returnObj.subject;
				jsonFAI['emailcontent'] = returnObj.emailcontent;
				jsonFAI['to'] = returnObj.to;
				jsonFAI['cc'] = returnObj.cc;
				jsonFAI['results'] = returnObj.results;
				jsonFAI['attachments'] =  "Engineering Work In Progress Report";
				returnEmailInfo = StructCopy(jsonFAI);
				break;
			case 'completed_by_ri_date_report':
				json = StructNew();
				json['reporttype'] = jsonFAI.reporttype;
				returnObj = obj.GenerateReport(argumentCollection = json);
				StructClear(json);
				jsonFAI['subject'] = returnObj.subject;
				jsonFAI['emailcontent'] = returnObj.emailcontent;
				jsonFAI['to'] = returnObj.to;
				jsonFAI['cc'] = returnObj.cc;
				jsonFAI['results'] = returnObj.results;
				jsonFAI['attachments'] =  "";
				returnEmailInfo = StructCopy(jsonFAI);
				break;
		}
		StructClear(jsonFAI);		
	/*
	}
	else
	{
		returnEmailInfo = StructNew();
		returnEmailInfo["id"] = jsonFAI.id
		returnEmailInfo["reporttype"] = jsonFAI.reporttype
		returnEmailInfo['subject'] = "";
		returnEmailInfo['emailcontent'] = "";
		returnEmailInfo['results'] = "Failed";
	}
	*/
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnEmailInfo));
		
</cfscript>
