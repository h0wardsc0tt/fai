<cfinclude template="../serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfscript>		
    jsonEmail = deserializeJSON(ToString(getHTTPRequestData().content));
	buyer_email = '';
	company_email = '';
	inspector =  '';
	switch(jsonEmail.company1){
		case 'Clear-Com':
			company_email = 'fcarisey@hme.com';
			break;
		case 'Hme':
			company_email = 'rgaboury@hme.com';
			break;
	}

	switch(jsonEmail.buyer){
		case 'Ann.Hoang':
			buyer_email = 'Ann Hoang<AHoang@hme.com><ahoang@hme.com>';
			break;
		case 'A.Hoang':
			buyer_email = 'Ann Hoang<AHoang@hme.com><ahoang@hme.com>';
			break;
		case 'W. LeBlanc':
			buyer_email = 'Whitney LeBlanc<WLeBlanc@hme.com>';
			break;
		case 'J. HARRIS':
			buyer_email = 'Jen Harris<JHarris@hme.com>';
			break;
		case 'C. Rutman':
			buyer_email = 'Christopher Rutman<CRutman@hme.com>';
			break;
		case 'K. Early':
			buyer_email = 'Ken Early<KEarly@hme.com>';
			break;
		case 'B. Yocum':
			buyer_email = 'Bill Yocum<BYocum@hme.com>';
			break;
		case 'M. Dayos':
			buyer_email = 'M. Dayos<mdayos@hme.com>';
			break;
		case 'K. Barrett':
			buyer_email = 'Kevin Barrett<KevinB@hme.com>';
			break;
	}
	
	switch(jsonEmail.inspector){
		case 'C. KARJANIS':
 			inspector = '';
 		 	break;
		case 'D. DAY':
 			inspector = 'David Day<DDay@hme.com>';
 		 	break;
		case 'G. LAROYA':
 			inspector = '';
 		 	break;
		case 'G.LAROYA':
 			inspector = '';
 		 	break;
		case 'J. SANFORD':
 			inspector = 'James Sanford<jsanford@hme.com>';
 		 	break;
		case 'M. BRUSILOVSKI':
 			inspector = 'Maya Brusilovski<mayab@hme.com>';
 		 	break;
		case 'M. HOWARD':
		 	inspector = 'Michael Howard<mhoward@hme.com>';
		 	break;
		case 'R. ADAMSON':
 			inspector = 'Robinette Adamson<radamson@hme.com>';
 		 	break;
		case 'R. GABOURY':
 			inspector = 'Rich Gaboury<RichG@hme.com>';
 		 	break;
		case 'S. HOANG':
 			inspector = '';
 		 	break;
		case 'S. HOPF':
 			inspector = 'Steve Hopf<shopf@hme.com>';
 		 	break;
		case 'F. CARISEY':
 			inspector = 'Franck Carisey<fcarisey@hme.com>';
 		 	break;
	}
	
	cc = 'Antonio Castro<ACastro@hme.com>; Franck Carisey<fcarisey@hme.com>; Rich Gaboury<RichG@hme.com>; Jason Morgan<JMorgan@hme.com>; Ricci Fretz<rfretz@hme.com>; Julee Gillespie<JGillespie@hme.com>; Tatiana Kent<tkent@hme.com>; David Day<DDay@hme.com>; Michael Howard<mhoward@hme.com>';
	
	if (#FindNoCase(inspector, cc)# > 0){
		inspector = '';
	}
	else{
		cc = inspector & ';' & cc; 
	}
//cc='';
//buyer_email='hscott@hme.com';
</cfscript>

<cfset tmp_file_name = "#GetTempDirectory()#/#CreateUUID()#.pdf">
<cfset logoImage ='../images/hme-companies.png'>

<cfset results = "OK">
<cftry>
    <cfmail to=#buyer_email# cc=#cc# from="no-reply@hme.com" subject="First Article Inspection Created Supplier #jsonEmail.distributormanufacturer#, PN #jsonEmail.itemnumber#" type="html"> 
       <p> 
             <img src="cid:logo" alt="" /><br /> 
             <span style="font-size:15px; color:##CCC8C8;">First Article Inspection</span><br>
       </p>     
       Hi -<br><br>
        Receiving inspection has created a First Article Inspection of part number #jsonEmail.itemnumber#, for supplier #jsonEmail.distributormanufacturer#.<br><br>
        
        Please visit http://faiadmin.hme.com/fai to add a specified NEED DATE for this inspection, if required.  Otherwise, please allow a standard 14-day lead time to perform an accurate and thorough inspection.<br><br>
        
        For any questions about this inspection , please contact #jsonEmail.inspector# directly or QA Inspection Lead David Day or QA Inspection Supervisor Michael Howard.<br><br>
        
        Thank you,<br><br>
        
        HME INSPECTION SERVICES

        <cfmailparam  
             file="#ExpandPath('../images/hme-companies.png')#" 
             contentid="logo"  
             disposition="inline" 
        /> 
    </cfmail>
    <cfcatch type="Application">
    	<cfset results = #cfcatch.detail#>
    </cfcatch>
</cftry>
<cfscript>
	returnResult = StructNew();
	returnResult['results'] = results;
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnResult));
</cfscript>