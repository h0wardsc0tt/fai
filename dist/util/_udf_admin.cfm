<cfsilent>
<cfscript>
	function get_userDepartment(member) {
		Userdepartment = "";
		departmentList = "executive,it department,sales,marketing,engineering,hr";
		memberArray = ListToArray(member);
		
		for(i=1;i <= ArrayLen(memberArray); i=i+1) {	
			for(x=1;x <= ListLen(departmentList); x=x+1) {
				thisDepartment = "cn=" & ListGetAt(departmentList, x);
	 			if(FindNoCase(thisDepartment, memberArray[i]) GT 0) {
					Userdepartment = thisDepartment;
					break;
				}
			}
			if(Userdepartment IS NOT "") {
				break;
			}
		}
		
		Userdepartment = ReplaceNoCase(Userdepartment, "cn=", "");
		return Userdepartment;
	}
	function check_Template(request) {
		requestArray = ListToArray(request, "\");
		lastElement = ArrayLen(requestArray);
		pageRequested = requestArray[lastElement];
		passfail = "";
		
		if(Left(pageRequested,1) IS "_") {
			passfail = "nobrowse";
		}
		
		return passfail;
	}
	function get_userDepartmentCompany(member,email) {
		USERinfo = StructNew();
		Userdepartment = "";
		departmentList = "executive,it department,sales,marketing,engineering,hr";
		Usercompany = "";
		companyList = "hme.com,clearcom.com,ceinstl.com";
		memberArray = ListToArray(member);
		
		for(i=1;i <= ArrayLen(memberArray); i=i+1) {	
			for(x=1;x <= ListLen(departmentList); x=x+1) {
				thisDepartment = "cn=" & ListGetAt(departmentList, x);
	 			if(FindNoCase(thisDepartment, memberArray[i]) GT 0) {
					Userdepartment = thisDepartment;
					break;
				}
			}
			if(Userdepartment IS NOT "") {
				break;
			}
		}
		for(x=1;x<=ListLen(companyList);x=x+1) {
			if(FindNoCase(ListGetAt(companyList, x), email) GT 0) {
				Usercompany = ListGetAt(companyList, x);
				break;
			}
		}
		if(Usercompany IS NOT "") {
			switch(Usercompany) {
				case "hme.com":
					USERinfo.Company = "HME";
				break;
				case "clearcom.com":
					USERinfo.Company = "Clear-Com";
				break;
				case "ceinstl.com":
					USERinfo.Company = "CE Repairs";
				break;
				default:
					USERinfo.Company = "HME";
			}
		} else {
			USERinfo.Company = "";
		}
		
		Userdepartment = ReplaceNoCase(Userdepartment, "cn=", "");
		USERinfo.Department = Userdepartment;

		return USERinfo;
	}
</cfscript>
</cfsilent>