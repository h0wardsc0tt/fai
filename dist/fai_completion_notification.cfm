<cfinclude template="../serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfparam name="FORM.faiID" default="">
<cfparam name="FORM.fai_report_req" default="">
<cfparam name="FORM.fai_report_from_date" default="">
<cfparam name="FORM.fai_report_to_date" default="">
<cfparam name="FORM.fai_report_storedProcedure" default="">
<cfparam name="FORM.fai_report_sortorder" default="">
<cfparam name="FORM.fai_report_sortColumn" default="">
<cfparam name="FORM.fai_report_custom" default="">

<cfscript>	
	jsonFAI = StructNew();
	jsonFAI["id"] = FORM.faiID;
	//jsonFAI["fromdate"] = FORM.fai_report_from_date;
	//jsonFAI["todate"] = FORM.fai_report_to_date;
	jsonFAI["storedProcedure"] = FORM.fai_report_storedProcedure;
	jsonFAI["sortorder"] = FORM.fai_report_sortorder;
	jsonFAI["sortColumn"] = FORM.fai_report_sortColumn;
	jsonFAI["custom"] = FORM.fai_report_custom;
	obj = CreateObject("component", "_cfcs.fai");
	fai = obj.searchByFAINo(argumentCollection = jsonFAI);
	//attachments_struct = deserializeJson(fai.attachments);
</cfscript>
<cfinclude template="_dat_fai_completion_notification_report.cfm">
<cfheader name="Content-Disposition" value="inline; filename=FAI #fai.id# Completion Notification.pdf">
<cfcontent reset="Yes" type="application/pdf" file="#GetTempDirectory()#/FAI_#fai.id#_Completion_Notification.pdf" deletefile="Yes">     

<!---<cfheader name="Content-Disposition" value="attachment;filename=FAI #fai.id# Completion Notification.pdf">
<cfcontent type="application/octet-stream" file="#GetTempDirectory()#/FAI_#fai.id#_Completion_Notification.pdf" deletefile="Yes">--->

<!---
<cfmail to="hscott@hme.com" from="foo@foo.com" subject="Your PDF">
<cfloop from="1" to="#ArrayLen(attachments_struct)#" index="i"> 
	<cfset fileAttachment = obj.get_Attachment(attachments_struct[i].guid)>
	<cfmailparam file="#replace(replace(fileAttachment.fileName," ","_","ALL"), ",", "_")#" content="#fileAttachment.file#"> 
</cfloop>     
<cfmailparam file = "#GetTempDirectory()#/FAI_2666_Completion_Notification.pdf" remove = "yes"> 
	Attached please find your cool PDF. Enjoy! 
    sdasd
    asd
</cfmail>
--->
