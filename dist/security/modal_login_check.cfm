<cfsetting showdebugoutput="no">
<cfsilent>
	<cfset domain = "powdc01.hme">
    <cfset FORM.Username = REReplace(FORM.Username, "\@.*$", "")>
	<cfscript>
		if(FORM.Username IS NOT "") {
			if(REFindNoCase('[\(\)\*\|;\"@=\?<>:\/\\,]', FORM.Username) GT 0) { 
				ERR.ErrorFound = true;
				ERR.ErrorMessage = "Invalid credentials supplied. Please try again";
			}
		} else {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = "Invalid credentials supplied. Please try again";
		}
		if(FORM.Password IS NOT "") {
			if(Len(FORM.Password) LT 4 OR ReFindNoCase("(cn=|[ ])", FORM.Password) GT 0) {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = "Invalid credentials supplied. Please try again";
			}
		} else {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = "Invalid credentials supplied. Please try again";
		}
	</cfscript>
</cfsilent>

<cfif StructKeyExists(SESSION,"loginAttempts") AND StructKeyExists(LOGIN,"maxAttempts")>
	<cfif SESSION.loginAttempts GTE LOGIN.maxAttempts>   
		<cfinclude template="./modal_login_form.cfm">
		<cfexit method="exittemplate">
	</cfif>
<cfelse>
	<cfset ERR.ErrorFound = true>
	<cfset ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Login Attempt Invalid. The system administrator has been notified")>
	<cfmail to="Web_Support@hme.com" from="no-reply@hme.com" subject="Sales Portal: Bad Login Attempt">
		Bad Login Attempt:
		#CGI.REMOTE_ADDR#
		#CGI.HTTP_USER_AGENT#
	</cfmail>
	<cfinclude template="./modal_login_form.cfm">
	<cfexit method="exittemplate">
	<cfabort>
</cfif>

<cfif ERR.ErrorFound>
	<cfset SESSION.loginAttempts = SESSION.loginAttempts + 1>
	<cfinclude template="./modal_login_form.cfm">
	<cfexit method="exittemplate">
</cfif>

<cftry>
    <cfldap 
        action="query"
        server="powdc01.HME.COM"
        name="qry_getUser"
        start="DC=hme,DC=com"
        filter="(&(objectclass=user)(SamAccountName=#FORM.Username#))"
        username="hme\#FORM.Username#"
        password="#FORM.Password#"
        attributes = "cn,o,l,st,sn,c,mail,telephonenumber,givenname,homephone,streetaddress,postalcode,SamAccountname,physicalDeliveryOfficeName,department,memberOf">
    
    <cfset UserRoleList = "## HMEAll, ## HMECanada, ## HSCALL">
    <cfset UserRole = "">
    
<!---    <cfif ReFindNoCase("(## HMEAll)" , qry_getUser.memberOf) GT 0 OR ReFindNoCase("(## HMECanada)" , qry_getUser.memberOf) GT 0 OR ReFindNoCase("(## HSCAll)" , qry_getUser.memberOf) GT 0>
--->    
        <cfloop list="#UserRoleList#" index="Role">
            <cfif ListContains(qry_getUser.memberOf, Role) NEQ 0>
                <cfset UserRole = ListAppend(UserRole, Role)>
            </cfif>
        </cfloop>
        
        <cfif ListLen(UserRole) GT 1>
            <cfset UserRole = ListDeleteAt(UserRole, 1)>
        </cfif>

        <cfscript>
            SESSION.IsLoggedIn = true;
            SESSION.loginAttempts = 0;
            SESSION.User_UID = qry_getUser.SamAccountname;
            SESSION.User_Name = qry_getUser.cn;
            SESSION.User_Department = UserRole;
            SESSION.User_Dept = qry_getUser.department;
            SESSION.User_Info = qry_getUser;
            SESSION.User_Email = qry_getUser.mail;
            CreateSession = CreateObject("component", "_cfcs.SessionMgmt");
            NewUser_Session = CreateSession.initSession(User_UID=SESSION.User_UID, User_Name=SESSION.User_Name, User_Department=SESSION.User_Department, User_Email=SESSION.User_Email);						
        </cfscript>
         
<!---    <cfelse>
        <cfset ERR.ErrorFound = true>
        <cfset ERR.ErrorMessage = "Login fail. You do not have access to view this page.">
    </cfif>
--->    <cfcatch type="any">
        <cfset ERR.ErrorFound = true>
        <cfset ERR.ErrorMessage = "Login fail. The username or password you entered was not found in our system.">
    </cfcatch>
</cftry>

<cfif ERR.ErrorFound>
	<cfset SESSION.loginAttempts = SESSION.loginAttempts + 1>
	<cfinclude template="./modal_login_form.cfm">
	<cfexit method="exittemplate">
<cfelse>
	<script>
		parent.closeModelLogin()
	</script>
</cfif>


