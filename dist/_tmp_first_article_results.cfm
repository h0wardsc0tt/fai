        <div class="row" style="padding-bottom:3px;">
            <div class="form-col fai-results fai-results-bgcolor col-xs-12">
            FIRST ARTICLE FINAL RESULTS
            </div>
          </div>
    	<div class="row" style="position:relative;">
        <div class="blockout_level_6 blockout_section"></div>
           <div class="form-col fai-cols">
                <div class="form-group">
                    <label for="finalresults" class="control-label fai-results-bgcolor">FINAL RESULTS :</label>
                    <div class="fai-input">
                            <select class="level6 form-control" id="finalresults" name="finalresults" style="display:inline-block; padding:0">
                                <option value=""></option>
                                <option value="PASSED">PASSED</option>
                                <option value="FAILED">FAILED</option>
                                <option value="PASSED WITH CONDITIONS">PASSED WITH CONDITIONS</option>
                                <option value="VOIDED">VOIDED</option>
                            </select>
                            <span style="font-size:8px;">DMR REF# </span><input type="text" name="dmrref" id="dmrref" class="level6 form-control numeric" style="display:inline-block; padding:0;"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="approvedby" class="control-label fai-results-bgcolor">APPROVED BY :</label>
                    <div class="fai-input">
                        <select class="level6 form-control selectpicker" id="approvedby" name="approvedby"
                        	data-size="5" data-live-search="true"  data-selected-text-format="count > 2">
                         </select>
                    </div>
                </div>
                <div class="form-group">
                   <label for="datecompleted" class="control-label fai-results-bgcolor">DATE COMPLETED :</label>
                   <div class="fai-input">
                          <input type="text" autocomplete="off" maxlength="100" name="datecompleted" id="datecompleted" 
                          class="level6 form-control input-date"/>
                    </div>
                </div>
                <div class="form-group">
                   <label for="datesenttoqe" class="control-label fai-results-bgcolor">DATE SENT TO QE :</label>
                   <div class="fai-input">
                          <input type="text" autocomplete="off" maxlength="100" name="datesenttoqe" id="datesenttoqe" 
                          class="level6 form-control input-date"/>
                    </div>
                </div>
             </div>
             <div class="form-col fai-right-cols  col-xs-6">
                <div class="level6 form-group" style="margin-bottom:0px;">
                	<textarea id="closurecomments" class="level6-textarea test-results" name="closurecomments" style="overflow-y: auto"> 
                    </textarea>            
                </div>
             </div>
        </div>
