        <div class="row" style="padding-bottom:3px;">
            <div class="form-col lab-test-results lab-test-results-bgcolor col-xs-12">
            (LEVEL 4) QA TEST LAB RESULTS
            </div>
         </div>
     	<div class="row" style="position:relative;">
        <div class="blockout_level_4 blockout_section"></div>
            <div class="form-col fai-cols">
				<div class="form-group">
				<label for="dateinqalab" class="control-label lab-test-results-bgcolor">DATE IN QA LAB :</label>
				<div class="fai-input">
					  <input type="text" autocomplete="off" maxlength="100" name="dateinqalab" id="dateinqalab" 
					  class="level4 form-control input-date"/>
				</div>
				</div>
				<div class="form-group">
				<label for="qalabdatecomplete" class="control-label lab-test-results-bgcolor">QA LAB DATE COMPLETE :</label>
				<div class="fai-input">
					 <input type="text" autocomplete="off" maxlength="100" name="qalabdatecomplete" id="qalabdatecomplete" 
					 class="level4 form-control input-date"/>
				</div>
				</div>
				<div class="form-group">
				<label for="qatestlabpassfail" class="control-label lab-test-results-bgcolor">QA TEST LAB PASS / FAIL :</label>
				<div class="fai-input">
					<select class="level4 form-control" id="qatestlabpassfail" name="qatestlabpassfail">
					   <option value="select"></option>
						<option value="PASS">PASS</option>
						<option value="FAIL">FAIL</option>
					</select>
				</div>
				</div>
				<div class="form-group">
				<label for="qatestlabnewpn" class="control-label lab-test-results-bgcolor">QA TEST LAB NEW PN :</label>
				<div class="fai-input">
					<input type="text" maxlength="100" name="qatestlabnewpn" id="qatestlabnewpn" class="level4 form-control fai-input-color"/>
				</div>
				</div>
				<div class="form-group">
				<label for="qatestlabnewpn2" class="control-label lab-test-results-bgcolor">QA TEST LAB NEW PN 2 :</label>
				<div class="fai-input">
					<input type="text" maxlength="100" name="qatestlabnewpn2" id="qatestlabnewpn2" class="level4 form-control fai-input-color"/>
				</div>
				</div>
				<div class="form-group">
				<label for="qatestlabreport" class="control-label lab-test-results-bgcolor">QA TEST LAB REPORT #  :</label>
				<div class="fai-input">
					<input type="text" maxlength="100" name="qatestlabreport" id="qatestlabreport" class="level4 form-control fai-input-color"/>
				</div>
				</div>
				
				<div class="form-group">
					<label for="scar" class="control-label lab-test-results-bgcolor">SCAR OR QUARANTINE # :</label>
					<div class="fai-input" style="position: relative;padding-left: 40px;">
						<input type="checkbox" id="scarck" style="height: 20px;width: 20px;position: absolute;left: 15px;">

						<input type="text" maxlength="100" name="scar" id="scar" class="level4 form-control fai-input-color"/>
					</div>
				</div>                   
				<div class="form-group">
					<label for="lotnumberonly" class="control-label lab-test-results-bgcolor">THIS IS FOR LOT # ONLY :</label>
					<div class="fai-input" style="position: relative;padding-left: 40px;">
						<input type="checkbox" id="lotnumberonlyck" style="height: 20px;width: 20px;position: absolute;left: 15px;">

						<input type="text" maxlength="100" name="lotnumberonly" id="lotnumberonly" class="level4 form-control fai-input-color"/>
					</div>
				</div>                   
				<div class="form-group">
					<label for="limtedduration" class="control-label lab-test-results-bgcolor">FOR THIS MANY RECEIPTS :</label>
					<div class="fai-input" style="position: relative;padding-left: 40px;">
						<input type="checkbox" id="limteddurationck" style="height: 20px;width: 20px;position: absolute;left: 15px;">

						<input type="text" maxlength="100" name="limtedduration" id="limtedduration" class="level4 form-control fai-input-color"/>
					</div>
				</div>                   
                   
                    
              </div>
             <div class="form-col qa-lab-report col-xs-6">               
                 <div class="form-col qa-lab-report col-xs-12" style="padding-left:0;">
					<div class="form-col col-xs-6" style="padding-left:0;">
						<!---<div class="form-group" style="margin-bottom: 0px;">               	       
						   <div class="fai-input">
								<input type="checkbox" id="scarck" style="height: 20px;width: 20px;">
								<input type="text" maxlength="100" name="scar" id="scar" placeholder="SCAR or Quarantine #"  
									class="level4 form-control noEnterKey"/>
							</div>
						</div>--->
						<!---<div class="form-group" style="margin-bottom: 0px;">               	       
						   <div class="fai-input">
								<input type="checkbox" id="limteddurationck" style="height: 20px;width: 20px;">
								<input type="text" maxlength="100" name="limtedduration" id="limtedduration" placeholder="For this many receipts" class="level4 form-control noEnterKey"/>
							</div>
						</div>
						<div class="form-group" style="margin-bottom: 0px;">               	       
						   <div class="fai-input">
								<input type="checkbox" id="lotnumberonlyck" style="height: 20px;width: 20px;">
								<input type="text" maxlength="100" name="lotnumberonly" id="lotnumberonly" placeholder="This Lot # Only" class="level4 form-control noEnterKey"/>
							</div>
						</div>--->
						<div class="form-group" style="margin-bottom: 0px;">               	       
						   <div class="fai-input">
								<div style="display:inline-block;">
								 <input class="form-check-input" type="checkbox" id="visualck" style="height: 20px;width: 20px;">
								</div>
								<div style="display:inline-block;">
                                 <label class="form-check-label" for="visualck" style="font-weight: normal;">100% Visual Inspection</label>
								</div>
							</div>
							<div>
								<div style="display:inline-block;">
								 <input class="form-check-input" type="checkbox" id="sortingonlyck" style="height: 20px;width: 20px;">
								</div>
								<div style="display:inline-block;">
                                 <label class="form-check-label" for="sortingonlyck" style="font-weight: normal;">Sorting Only</label>
								</div>
							</div>
						</div>
						
					</div>
               
					<div class="form-col col-xs-6" style="padding-left:0;">
						<div class="form-group" style="margin-bottom: 0px;">     	       
<!---						    <div>
								<div style="display:inline-block;">
								 <input class="form-check-input" type="checkbox" id="sortingonlyck" style="height: 20px;width: 20px;">
								</div>
								<div style="display:inline-block;">
                                 <label class="form-check-label" for="sortingonlyck" style="font-weight: normal;">Sorting Only</label>
								</div>
							</div>--->
						    <div>
								<div style="display:inline-block;">
								 <input class="form-check-input" type="checkbox" id="contactuponreceiptck" style="height: 20px;width: 20px;">
								</div>
								<div style="display:inline-block;">
                                 <label class="form-check-label" for="contactuponreceiptck" style="font-weight: normal;">Contact upon receipt</label>
								</div>
							</div>
						    <div>
								<div style="display:inline-block;">
								 <input class="form-check-input" type="checkbox" id="reportrequiredck" style="height: 20px;width: 20px;">
								</div>
								<div style="display:inline-block;">
                                 <label class="form-check-label" for="reportrequiredck" style="font-weight: normal;">Inspection Report Required</label>
								</div>
							</div>
						    <div>
								<div style="display:inline-block;">
								 <input class="form-check-input" type="checkbox" id="removedtsck" style="height: 20px;width: 20px;">
								</div>
								<div style="display:inline-block;">
                                 <label class="form-check-label" for="removedtsck" style="font-weight: normal;">Remove From Dock-to-Stock</label>
								</div>
							</div>
						</div>
					</div>
               
               
               
                </div>
             </div>
        </div>
