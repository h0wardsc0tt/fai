<cfscript>
	json_id = qry_searchByFAINo.id;
	company_email = '';
	buyer_email = '';
	color = '##000000';
	if(qry_searchByFAINo.finalresults == 'PASSED')	
		color = '##01BF0B';	
	if(qry_searchByFAINo.finalresults == 'FAILED')	
		color = '##F3090D';
	switch(qry_searchByFAINo.company1){
		case 'Clear-Com':
			company_email = 'tjordan@hme.com';
			break;
		case 'Hme':
			company_email = 'rgaboury@hme.com';
			break;
	}
	switch(qry_searchByFAINo.buyer){
		case 'A.Hoang':
			buyer_email = 'ahoang@hme.com';
			break;
		case 'W. LeBlanc':
				buyer_email = 'wleblanc@hme.com';
				break;
		case 'J. HARRIS':
				buyer_email = 'jharris@hme.com';
				break;
		case 'C. Rutman':
				buyer_email = 'crutman@hme.com';
				break;
		case 'K. Early':
				buyer_email = 'kearly@hme.com';
				break;
		case 'B. Yocum':
				buyer_email = 'byocum@hme.com';
				break;
		case 'M. Dayos':
				buyer_email = 'mdayos@hme.com';
				break;
		case 'K. Barrett':
				buyer_email = 'kevinB@hme.com';
				break;
	}
</cfscript>
<cfsavecontent variable = "html">
<cfoutput> 
<div style="margin:0px 5px 0px 5px;">
	<p style="font-size:12px;">Hi #qry_searchByFAINo.buyer# -</p>
	<p style="font-size:12px;">Attached, please find the completed FAI ## #qry_searchByFAINo.id# report for PN #qry_searchByFAINo.itemnumber# Rev #qry_searchByFAINo.revision# #qry_searchByFAINo.distributormanufacturer# in which the item <span style="color:#color#;">#qry_searchByFAINo.finalresults#</span>.</p>
	<p style="font-size:12px;">Thanks,</p>
	<p style="font-size:12px;">David Day<br />x4058</p>
 </div>
 </cfoutput>
</cfsavecontent>

 <cfset emailcontent = html>
 <cfset subject = "FAI ## #qry_searchByFAINo.id# Completed: PN #qry_searchByFAINo.itemnumber# Rev #qry_searchByFAINo.revision# #qry_searchByFAINo.distributormanufacturer# (Results:#qry_searchByFAINo.finalresults#)">
 <cfif buyer_email neq ''>
 	<cfset to_addrs = company_email & ';' & buyer_email>
  <cfelse>
 	<cfset to_addrs = company_email>
 </cfif>
 <!---<cfset cc_addrs = "JGillespie@hme.com; tkent@hme.com; nsoule@clearcom.com; krobinson@hme.com; triches@hme.com; rfretz@hme.com; mhoward@hme.com; lpresutti@hme.com; jmorgan@hme.com">--->
 <cfset cc_addrs = "## FAI Group Email">
  
  



 