        <div class="row" style="padding-bottom:3px;">
            <div class="form-col smt-tha-results smt-tha-results-bkcolor col-xs-12">
            (LEVEL 2) SMT - THA RESULTS
            </div>
        </div>
    	<div class="row" style="position:relative;">
        <div class="blockout_level_2 blockout_section"></div>
            <div class="form-col fai-cols">
                <div class="form-group">
                    <label for="smtqtyreceived" class="control-label smt-tha-results-bkcolor">SMT QTY RECEIVED :</label>
                    <div class="fai-input">
                        <input type="text" maxlength="100" name="smtqtyreceived" id="smtqtyreceived" class="level2 form-control fai-input-color numeric"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="dateinsmt" class="control-label smt-tha-results-bkcolor">DATE IN SMT :</label>
                     <div class="fai-input">
                        <input type="text" autocomplete="off" maxlength="100" name="dateinsmt" id="dateinsmt" class="level2 form-control input-date"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="smtcompletedate" class="control-label smt-tha-results-bkcolor">SMT COMPLETE DATE :</label>
                    <div class="fai-input">
                        <input type="text" autocomplete="off" maxlength="100" name="smtcompletedate" id="smtcompletedate" class="level2 form-control input-date"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="smtpassfail" class="control-label smt-tha-results-bkcolor">SMT PASS / FAIL :</label>
                    <div class="fai-input">
                        <select class="level2 form-control" id="smtpassfail" name="smtpassfail">
                           <option value="select"></option>
                            <option value="PASS">PASS</option>
                            <option value="FAIL">FAIL</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="thapassfail" class="control-label smt-tha-results-bkcolor">THA PASS / FAIL :</label>
                    <div class="fai-input">
                        <select class="level2 form-control" id="thapassfail" name="thapassfail">
                           <option value="select"></option>
                            <option value="PASS">PASS</option>
                            <option value="FAIL">FAIL</option>
                        </select>
                    </div>
                </div>
                   <div class="form-group">
                        <label for="smtnewpn" class="control-label smt-tha-results-bkcolor">SMT NEW PN :</label>
                        <div class="fai-input">
                            <input type="text" maxlength="100" name="smtnewpn" id="smtnewpn" class="level2 form-control fai-input-color"/>
                        </div>
                    </div>
                    
                     <div class="form-group">
                        <label for="smtnewpn2" class="control-label smt-tha-results-bkcolor">SMT NEW PN 2 :</label>
                        <div class="fai-input">
                            <input type="text" maxlength="100" name="smtnewpn2" id="smtnewpn2" class="level2 form-control fai-input-color"/>
                        </div>
                    </div>
             </div>                         
             
             <div class="form-col fai-right-cols  col-xs-6">
                <div class="form-group" style="margin-bottom:0px;">
                	<textarea id="smtresults" class="level2-textarea smtresults" name="smtresults" style="overflow-y: auto"> 
                    </textarea>            
                </div>
             </div>
         </div>
