 <meta name=viewport content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta http-equiv="x-dns-prefetch-control" content="on" />
<cfsetting showdebugoutput="no">
<cfinclude template="./serverSettings.cfm">
        <link rel="icon" href="favicon.ico?v=4">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<!---<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">--->
        <link rel="stylesheet" href="./css/bootstrap-select.min.css">
        <link rel="stylesheet" href="./css/main.css">
        <link rel="stylesheet" href="./css/animate.css">
        <link rel="stylesheet" href="./css/materialize.css">
        <link rel="stylesheet" href="./css/dragDropUpload.css">
        <link rel="stylesheet" href="./css/uploadfile.css">
        <link rel="stylesheet" href="./css/jquery.multiselect.css">
        <link rel="stylesheet" href="./css/fai.css">
		<link rel="stylesheet" href="./css/jquery-te-1.4.0.css">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
                <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
                <script type="text/javascript" src="./js/dragDropUpload.js?v=1"></script>
				<!---<script src="http://hayageek.github.io/jQuery-Upload-File/4.0.10/jquery.uploadfile.min.js"></script>--->
				<script src="./js/jquery.uploadfile.js"></script>
 				<script src="./js/fai.js"></script>
 				<script src="./js/leads.js"></script>
				<!---<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>--->
                <script src="./js/bootstrap-select.min.js"></script>
                <script src="./js/autosize.js"></script>
                <!---<script src="//cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>--->
                <!---<script src="//cdn.ckeditor.com/4.6.0/full/ckeditor.js"></script>--->
                
                <script src="./js/ckeditor/ckeditor.js"></script>
				<script src="./js/ckeditor/adapters/jquery.js"></script>
                
                <!---<script src="js/ckeditor_plugin/ckeditor.js"></script>
				<script src="js/ckeditor_plugin/sample.js"></script>--->
                <!---<script src="http://cdn.jsdelivr.net/alasql/0.3/alasql.min.js"></script>--->
                <script src="./js/alasql.min.js"></script>
                <script type="text/javascript" src="./js/spin.min.js?v=1"></script>
                <script type="text/javascript" src="./js/spinnerOptions.js?v=1"></script>

  <style>
  textarea {
    box-sizing: border-box;
    resize: none;
	overflow:hidden;	 
}
 .email-text{
	 min-width:600px;
 }

.email-send-over{
  border:solid 1px #1E06F0;
}
  
.email-send-out{
  border:solid 1px #B4AFAF;
}
  
.email_send_btn_active{
	background-color:#B4AFAF;
	color:#fff;
}
	
.to-box{
	width:50px;
	text-align:center;
	padding-top:4px;
	display:inline-block;
	vertical-align:top;
	border:solid 1px #B4AFAF;
	font-size:12px;

}
.subject-box{
	text-align:center;
	padding-top:4px;
	display:inline-block;
	vertical-align:top;
	font-size:12px;
}

.attached-box{
	text-align:center;
	padding-top:4px;
	display:inline-block;
	vertical-align:top;
	font-size:12px;
}

.email-no-padding{
	padding-left:0;
	padding-right:0;
}

.email-container {
    /*
    box-shadow: 0 0 30px black;
	*/
	display:none;
	height:auto; 
	width:605px; 
	border:solid 1px #000;
	position:absolute; 
	background-color:#EFEDED;
	z-index:9000;
	box-shadow: 0 0 30px #000;
	padding:0px 0px 3px 0px;
}

.white-bkg{
	 background-color:#fff;
}

.emp{
	font-size:12px;
}

.bottom-border{
	border-bottom:solid 1px #ddd;
}

.left-border{
	border-left:solid 1px #ddd;
}

.right-border{
	border-right:solid 1px #ddd;
}

.col-padding{
	padding-left:5px;
}

.user-selected-color{
	background-color:#331088;
	color:#fff;
}
.mouseOver{
	background-color:#F0EAEA;
}

.attachment-attach{
	color:#09F776;
}
.attachment-unattach{
	color:#ff0000;
}

.email-validatrion-error{
	position:absolute; 		
	width:698px;
	z-index:9000; 
	background-color:#fff;	
	margin:0px 5px 0px 2px;
	/*	
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;
	
	
	padding:3px 5px 0px 13px;
	text-align:left;
	font-size:14px;
	*/
}

.email_close_btn{
	position:absolute;
	top:0;
	left:680px; 
	display:none;
}

  </style>
<div valign="top; margin-right:80px;" onclick="$('#rte_modal').modal('show')"; 
                style=" text-align:center;width:40px;padding-top:4px;display:inline-block;vertical-align:top;font-size:12px;margin-right: 10px;">Attached</div>


<div id="rte_modal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content" style="width:800px;height: auto;">
                <div id="email_header" class="modal-header">
                    <div style="color:#fff; text-align:left;">
                        <i style="font-size:23px;" class="fa fa-envelope"></i>
                        <div style="display:inline-block; padding-left:10px; font-size:20px;">FAI Email</div>
                    </div>
                </div>
                
                     <div class="email-validatrion-error">
                        <div class="expandable-element" style="height: 0px;">
                            <div style="border:solid 1px #ff0000;color:#1910EF">
                                <div class="email_close_btn" onMouseOver="this.style.cursor='pointer';"><i title="close" 
                                style="color:#dddddd" onClick="close_email_validation();" class="fa fa-times"></i></div>
                                <div style="padding:10px 5px 10px 5px; text-align:left;" class="error-message">You must provide a security question answer</div>
                             </div>
                        </div>
                    </div>                    
                
                <div class="modal-body text-center" style="padding:5px 5px 5px 5px;">                
                    <div class="row" style="vertical-align:top;text-align:left; margin-right:0;">
                        <div style="vertical-align:top;" class="col-xs-1">
                            <div onClick="send_message()" class="email-send-button email-send-out" 
                             	title="Send Email"
                                onMouseOut="$(this).removeClass('email-send-over').addClass('email-send-out');" 
                                onMouseOver="this.style.cursor='pointer';$(this).removeClass('email-send-out').addClass('email-send-over');" 
                                style="height:45px; width:45px; display:inline-block;text-align:center;font-size:12px;">
                                <div style="padding-top:15px;">Send</div>
                            </div>
                        </div>
                        
                        <div style="display:inline-block;line-height: 1.3; font-size:12px;" class="col-xs-11">
                            <div class="row">
                                <div valign="top" class="col-xs-1 email-no-padding" onclick="showAddress_book();">
                                    <div onMouseOver="this.style.cursor='pointer';" title="HME Employee Directory" valign="top" class="to-box">To..</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_to_address" class="email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                </div>                                                                                                
                            </div>
                             <div class="row" style="margin-top: 4;">
                               <div valign="top" class="col-xs-1 email-no-padding" onclick="showAddress_book();">
                                    <div onMouseOver="this.style.cursor='pointer';" title="HME Employee Directory" valign="top" class="to-box">Cc..</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_cc_address" class="email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                </div>                                 
                            </div>
                            <div class="row" style="margin-top: 4;">
                                <div onMouseOver="this.style.cursor='pointer';" valign="top" class="col-xs-1 email-no-padding">
                                    <div valign="top" class="subject-box">Subject</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_subject" class="email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                </div>                                 
                            </div>
                            <div class="row" style="margin-top: 4;margin-bottom: 4;">
                                <div valign="top" class="col-xs-1 email-no-padding">
                                    <div valign="top" class="attached-box">Attached</div>
                                </div>
                                <div class="col-xs-11 email-no-padding">
                                    <textarea id="send_attachments" class="email-to"  spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;">hscott@hme.com;hscott@currentevent.com;foo@foo.com</textarea>
                                    <div id="email_attachments_list" class="email-to" style="display:none;position:absolute; border:solid 1px #ddd;min-height:auto; width:100%; margin-top:1px; z-index:9000;background-color:#fff;">
                                        <ul id="email_ul_attachments_list" style="padding:0; margin:3px 3px 3px 3px; border:solid 1px #337ab7;">
                                            <li>
                                                <div style="width:15px; display:inline-block;">
                                                    <i class="attachment_add_remove fa fa-check attachment-attach"></i>
                                                </div>
                                                <span>hscott@hme.com</span>
                                            </li>
                                             <li>
                                                 <div style="width:15px; display:inline-block;">
                                                    <i class="attachment_add_remove fa fa-check attachment-attach"></i>
                                                 </div>
                                                 <span>hscott@currentevent.com</span>
                                             </li> 
                                             <li>
                                                 <div style="width:15px; display:inline-block;">
                                                    <i class="attachment_add_remove fa fa-check attachment-attach"></i>
                                                 </div>
                                                 <span>foo@foo.com</span>
                                             </li>                                      
                                         </ul>                               
                                    </div>
                                </div>  
                                <div id="email_contacts" class="email-container">
                                    <div id="email_contact_header" style="height:25px;width:100%; background-color:#337ab7; color:#fff; padding:6px 0px 0px 5px;">Address List</div>
                                    <div style="padding:0px 3px 0px 3px;">
                                    <div id="email_contact_search" style="height:25px;width:100%;margin:4px 0px 0px 0px;padding:0px 0px 0px 0px;">
                                        <div style="display:inline-block;">
                                        	<input id="address_book_rb" class="search-type" value="address_book" name="search_type" type="radio" checked>
                                        </div>
                                       <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:20px;">
                                        	Address List
                                        </div>

                                      	<div style="display:inline-block;">
                                        	<input id="address_book_search_rb" class="search-type" value="address_search" name="search_type" type="radio">
                                        </div>
                                       <div id="list_search" style="display:inline-block; vertical-align: bottom;margin-right:10px;">
                                        	Search:<input id="search_address_book" disabled type="text" style="width:100px;">
                                        </div>
                                       <div class="search-request-types" style="display:inline-block;">                                       
                                           <div style="display:inline-block;">
                                                <input id="address_book_rb" value="by_firstname" class="search-request" name="search_request" type="radio" checked>
                                            </div>
                                           <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:5px;">
                                                By First Name
                                            </div>
                                           <div style="display:inline-block;">
                                                <input id="address_book_rb" value="by_lastname" class="search-request" name="search_request" type="radio">
                                            </div>
                                           <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:5px;">
                                                By Last Name
                                            </div>
                                           <div style="display:inline-block;">
                                                <input id="address_book_rb" value="by_department" class="search-request" name="search_request" type="radio">
                                            </div>
                                           <div style="display:inline-block; vertical-align:bottom; padding-bottom:2px;margin-right:5px;">
                                                By Department
                                            </div>
										</div>

                                        
                                    </div>
                                    <div id="email_contact_book" style="height:250px;width:600px;margin:0px 0px 0px 0px; background-color:#fff; padding:0px 0px 0px 5px; overflow-y: scroll;">
                         
                                    </div>
                                    <div id="email_contact_selected" style="height:50px;width:100%;margin:5px 0px 5px 0px;  background-color:#EFEDED; padding:0px 0px 0px 5px;">
                                       <div class="row">
                                           <div class="col-xs-12">
                                                <div valign="top" class="col-xs-2 email-no-padding">
                                                    <button id="get_to_address" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">To -></button>
                                                </div>
                                                <div class="col-xs-10 email-no-padding">
                                                    <textarea id="email_to_selected" class="email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                                </div>                                 
                                            </div>
                                      </div>
                                      <div class="row" style="margin-top:5px;">
                                           <div class="col-xs-12">
                                                <div valign="top" class="col-xs-2 email-no-padding">
                                                    <button id="get_cc_address" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">Cc -></button>
                                                </div>
                                                <div class="col-xs-10 email-no-padding">
                                                    <textarea id="email_cc_selected" class="email-to" spellcheck='false' data-autoresize rows="1" style="min-height: 20px; width:100%;"></textarea>
                                                </div>                                 
                                            </div>
                                       </div>

										<div class="row" style="margin-top:5px;">
                                           <div class="col-xs-12">
                                                <div  style="text-align:right;">
                                                               <button id="address_book_ok_btn" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">OK</button>
                                                               <button id="address_book_cancel_btn" type="button" class="btn btn-default" style="height:20px;padding:0;font-size:12px; width:80px;">Cancel</button>
                                                </div>
                                            </div>
                                       </div>

                                    </div> 
                                    </div>                                
                                </div>                             
                            </div>
                       </div>
                    </div>
                    <div class="row">                   
                        <div class="col-xs-12">
                            <textarea type="text" id="modal_rte_textarea" name="modal_rte_textarea" class="modal_rte_textarea"
                            style="overflow-y:auto;height:300px; width:699px;margin-top:5px; border:solid 1px #B4AFAF;"></textarea>
                        </div>
                    </div>
           		</div>
                <div class="modal-footer email-footer-buttons">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<cfoutput>
<cfldap 
    action="query"
    server="powdc01.HME.COM"
    name="qry_getUser"
    start="DC=hme,DC=com"
	filter="(&(objectclass=user)(mail=*)(department=*))"
    maxRows = "100000"
    username="hme\hscott"
    password="December2018!"
	attributes = "cn,sn,mail,givenname,SamAccountname,physicalDeliveryOfficeName,department,memberOf"
>
 

<cfquery dbtype="query" name="hme_users">
    select cn,sn,mail,givenname,SamAccountname AS Account,physicalDeliveryOfficeName AS Office,department
    from qry_getUser
    where memberOf like '%HMEAll%'
    order by cn
</cfquery>
<cfquery dbtype="query" name="hme_departments">
    select distinct department
    from hme_users
    order by department
</cfquery>
<cfquery dbtype="query" name="hme_offices">
    select distinct upper(Office)
    from hme_users
    where Office != '' AND Office != 'Poaway'
    order by Office
</cfquery>
<cfscript>	
	obj = CreateObject("component", "_cfcs.fai");
	serializer = new lib.JsonSerializer();
	hme_users_json = serializer.serialize(hme_users);
	hme_departments_json = serializer.serialize(hme_departments);
	hme_offices_json = serializer.serialize(hme_offices);
</cfscript>

<script type="text/javascript">

var json_hme_users = JSON.parse(JSON.stringify(#hme_users_json#));
var json_hme_departments = JSON.parse(JSON.stringify(#hme_departments_json#));
var json_offices_json = JSON.parse(JSON.stringify(#hme_offices_json#));
var json_save_address_book;
var json_save_address_search;

</script>
</cfoutput>

<script type="text/javascript">

var to_address_array = [];
var cc_address_array = [];
var send_to_address_array = [];
var send_cc_address_array = [];

$.fn.modal.Constructor.prototype.enforceFocus = function() {
     modal_this = this
     $(document).on('focusin.modal', function (e) {
         // Fix for CKEditor + Bootstrap IE issue with dropdowns on the toolbar
         // Adding additional condition '$(e.target.parentNode).hasClass('cke_contents cke_reset')' to
         // avoid setting focus back on the modal window.
         if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
             && $(e.target.parentNode).hasClass('cke_contents cke_reset')) {
             modal_this.$element.focus()
         }
     })
 };
 
$(document).ready(function() {

	console.log(json_hme_users);
	$('#rte_modal').draggable({
		handle: "#email_header, .email-footer-buttons"
	});
	
	$('#email_contacts').draggable({
		handle: "#email_contacts, #email_contact_header"
	});
	
	var editorElement = CKEDITOR.document.getById( 'modal_rte_textarea' );
	$('#modal_rte_textarea').val('<p style="margin-left:0in; margin-right:0in"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:16.5pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#3670b1">&larr; all posts</span></span></span></span></span></p>');

	CKEDITOR.replace( 'modal_rte_textarea', {
	    customConfig: '/js/ckeditor/editorConfig.js'
	});
	
	//CKEDITOR.replace( 'modal_rte_textarea');


//initSample();
	
	$('.modal-content').resizable({
		//alsoResize: ".modal-dialog",
		minHeight: 300,
		minWidth: 800,
		handles: 'e, w'
	});
	
	//autosize(document.querySelectorAll('.email-text'));
	
	jQuery.each(jQuery('textarea[data-autoresize]'), function() {
		var offset = this.offsetHeight - this.clientHeight;
		var resizeTextarea = function(el) {
		   jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
		};
		jQuery(this).on('keyup input paste propertychange change', function() {resizeTextarea(this); }).removeAttr('data-autoresize');
	});
	
	
	$('.attachment_add_remove').mouseover(function(e){
		this.style.cursor='pointer';
	});
	
	$('#send_attachments').click(function(e){
		$(this).blur();		
		//if($(this).val() == '') return;
		var count = $("#email_ul_attachments_list").children().length;
		//console.log(count);
		if(count <= 0) return;
		$('#email_attachments_list').toggle('slow', function() {
		});	
	});
	
	$('#email_attachments_list').mouseleave(function(e){
		//if (e===undefined) e= window.event;
		//var target= 'target' in e? e.target : e.srcElement;
		//if (target!==this){return;}	
		$(this).toggle('slow');	
	});
	
	$('.attachment_add_remove').click(function(e){
		var attachments = '';
		if($(this).hasClass('fa-check')){
			$(this).removeClass('fa-check attachment-attach').addClass('fa-times attachment-unattach');
		}else{
			$(this).removeClass('fa-times attachment-unattach').addClass('fa-check attachment-attach');
		}
		
		$('.attachment_add_remove.attachment-attach').each(function() {
			attachments += (attachments == '' ? '' : '; ') + $(this).parents('li').children('span').html();
		});	
		$('#send_attachments').val(attachments).change();
	});
	
	$('#address_book_cancel_btn').click(function(e){
		$('#email_contacts').hide();
	});
	
	$('#address_book_ok_btn').click(function(e){
		var to_addr = $('#send_to_address').val();
		var cc_addr = $('#send_cc_address').val();
		to_addr = to_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		cc_addr = cc_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		var select_to_addr = $('#email_to_selected').val();
		var select_cc_addr =  $('#email_cc_selected').val();
		select_to_addr = select_to_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		select_cc_addr = select_cc_addr.replace(/\s/g,'').replace(/,/g,';').replace(/;/g,'; ')
		var addr_list = '';
		if($('#email_to_selected').val() != ''){
			addr_list = (to_addr == '' ? select_to_addr : '; ' + select_to_addr);
			$('#send_to_address').val(to_addr + addr_list).change();
		}
		if(select_cc_addr != ''){
			addr_list = (cc_addr == '' ? select_cc_addr : '; ' + select_cc_addr);
			$('#send_cc_address').val(cc_addr + addr_list).change();
		}
		$('#email_to_selected').val('').change();
		$('#email_cc_selected').val('').change();
		$('#email_contacts').hide();
	});
	
	$('#get_to_address').click(function(e){	
		var obj = $('#email_to_selected');
		var email_list = '';
		to_address_array = obj.val().replace(/\s/g,'').replace(/,/g,';').split(';')
		send_to_address_array = $('#send_to_address').val().replace(/\s/g,'').replace(/,/g,';').split(';');
		send_cc_address_array = $('#send_cc_address').val().replace(/\s/g,'').replace(/,/g,';').split(';');
		$('#email_contact_book .user-selected-color').each(function(i, obj) {
			//console.log($(obj).prop("classList"));
			if($(obj).hasClass('user-selected-color')){
				if(jQuery.inArray($(obj).attr('email'), to_address_array) < 0 
					&& jQuery.inArray($(obj).attr('email'), cc_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_to_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_cc_address_array) < 0
				)
					to_address_array.push($(obj).attr('email'));
				$(obj).removeClass('user-selected-color');
			}
		});
		//$('.tr-emp').removeClass('user-selected-color');
		obj.val('');
		$.each(to_address_array, function(index, item) {
			if(item != '')
				email_list += (email_list == '' ? item : '; ' + item);
		});	
		obj.val(email_list).change();	
	});
	
	$('#get_cc_address').click(function(e){	
		var obj = $('#email_cc_selected');
		var email_list = '';
		cc_address_array = obj.val().replace(/\s/g,'').replace(/,/g,';').split(';')
		$('#email_contact_book .user-selected-color').each(function(i, obj) {
			if($(obj).hasClass('user-selected-color')){
				if(jQuery.inArray($(obj).attr('email'), to_address_array) < 0 
					&& jQuery.inArray($(obj).attr('email'), cc_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_to_address_array) < 0
					&& jQuery.inArray($(obj).attr('email'), send_cc_address_array) < 0
				)
					cc_address_array.push($(obj).attr('email'));
				$(obj).removeClass('user-selected-color');
			}
		});
		obj.val('');
		$.each(cc_address_array, function(index, item) {
			if(item != '')
				email_list += (email_list == '' ? item : '; ' + item);
		});	
		obj.val(email_list).change();	
	});	

	$('.search-request-types').hide();
	
	$('#search_address_book').keyup(function(e){
		setTimeout(function(){
			if($('#search_address_book').val() != ''){
				autocomplete($('#search_address_book').val());
			}
		}, 10);
		
	});
	
	$('.search-type').click(function(e){
		switch($(this).val()){
			case 'address_book' :
				$('#email_contact_book').html('');
				$('#email_contact_book').append(json_save_address_book);
				set_contact_properties();	
				$('#search_address_book').prop('disabled',true)
				$('.search-request-types').hide();
				break;
			case 'address_search' :
				$('#email_contact_book').html('');
				$('#search_address_book').val('');
				$('#search_address_book').prop('disabled',false)
				$('.search-request-types').show();
				break;
		}
	});	

	$('.search-request').click(function(e){
		if($('#search_address_book').val() != ''){
				autocomplete($('#search_address_book').val());
		}
	});
	
	build_address_book(json_hme_users, 'givenname');	
	json_save_address_book = contacts;	
});

function set_contact_properties(){
	$('.emp-td').click(function(e){	
		toggle_emp_select($(this));
	});	
	$('.tr-emp').mouseover(function(e){	
		if(!$(this).hasClass('user-selected-color'))
			$(this).addClass('mouseover');
		this.style.cursor = 'pointer';
	
	});	
	$('.tr-emp').mouseout(function(e){	
		$(this).removeClass('mouseover');
	});
}

function showAddress_book(email_contacts){
	$('#email_to_selected').val('').change();
	$('#email_cc_selected').val('').change();
	$('#email_contacts').show();
}


function send_message(){
//CKEDITOR.instances.modal_rte_textarea.setData('<p style="margin-left:0in; margin-right:0in"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:16.5pt"><span style="font-family:&quot;Arial&quot;,sans-serif"><span style="color:#3670b1">&larr; all postsxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</span></span></span></span></span></p>');

	if($('.email-send-button').hasClass('email_send_btn_active')) return;
	json_email_resp = {};
	json_email_resp.to = '';
	json_email_resp.from = '';
	json_email_resp.subject = '';
	json_email_resp.attachments = '';
	json_email_resp.emailcontent = '';
	json_email_resp.id = '';
	json_email_resp.results = '';
	json_email_resp.reporttype = 'custom';
	var editorElement = CKEDITOR.document.getById( 'modal_rte_textarea' );
	json_email_resp.emailcontent = CKEDITOR.instances.modal_rte_textarea.getData();
	json_email_resp.to = $('#send_to_address').val();
	json_email_resp.cc = $('#send_cc_address').val();
	json_email_resp.subject = $('#send_subject').val();
	json_email_resp.attachments = $('#send_attachments').val();
	json_email_resp.from = 'hscott@hme.com';
	console.log(json_email_resp);
	send_json_email(JSON.stringify(json_email_resp));
}

var email_spinner;
function send_json_email(data){
	console.log(data);
	$('.email-send-button').addClass('email_send_btn_active');
	email_spinner = document.getElementById('email_header');
	var opts = spinnerOptions();
	email_spinner = new Spinner(opts).spin(email_spinner);
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/send_email.cfm",
		data: data,
		dataType: "json",
		success: function (data) {					
			email_spinner.stop();
			$('.email-send-button').removeClass('email_send_btn_active');
			if (data != null) {
				if(data.results == "OK"){
					//$('.email-element').hide();
					//display_status_message('email sent');
				}
				else{
					display_email_validation(data.results);
				}
				console.log(data);
			}
			else{
			}
		},
		error: function (jqXHR, ajaxOptions, thrownError) {
			email_spinner.stop();
			$('.email-send-button').removeClass('email_send_btn_active');
			$('#login_iframe').attr('src', 'login.cfm');
			$('#login_modal').modal('show');
			//setTimeout(function(){location.href=location.href;}, 300);	   
        }
	});	
}

function toggle_emp_select(obj){
	tr_obj = obj.closest('tr');
	if(tr_obj.hasClass('user-selected-color')){
		tr_obj.removeClass('user-selected-color');
		tr_obj.addClass('mouseover');
	}else{
		tr_obj.addClass('user-selected-color');
		tr_obj.removeClass('mouseover');
	}
}

function getOffset( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}

function autocomplete(val){
	 var searchReq = 'fn';
	switch($('.search-request-types input[name=search_request]:checked').val()){
		case 'by_firstname':
		 	searchReq = 'givenname';
			break;
		case 'by_lastname':
		 	searchReq = 'sn';
			break;
		break;
		case 'by_department':
		 	searchReq = 'department';
		break;
	}
	console.log($('.search-request-types input[name=search_request]:checked').val());
	var res = alasql('select cn,mail,department,office,givenname,sn from ? where ' + searchReq + ' like "' + val + '%"  order by ' + searchReq + ' asc',[json_hme_users]);    
	build_address_book(res, searchReq);
}

var contacts = '';	
function build_address_book(json_list, searchReq){
	$('#email_contact_book').html('');	
	contacts =  '<table class="emp emp_x">';
	contacts += '<tr style="position:absolute; background-color:#fff; width:579px;">';
	contacts += '<td class="col-padding right-border bottom-border" style="width:203px;">Name</td>';
	contacts += '<td class="col-padding bottom-border" style="width:200px;">Department</td>';
	contacts += '<td class="col-padding left-border bottom-border" style="width:200px;">Location</td></tr>';
	contacts += '<tr><td style="width:200px;">&nbsp;</td>';
	contacts += '<td style="width:200px;">&nbsp;</td>';
	contacts += '<td style="width:200px;">&nbsp;</td></tr>';
	$.each(json_list, function(index, item) {
		contacts += '<tr id="tr-emp_' + index + '" class="tr-emp"  email="'+ item.cn + '<' + item.mail + '>">';
		if(searchReq == 'sn')
			contacts += '<td id="cd-emp_' + index + '" class="cn emp-td col-padding right-border" style="width:200px;">' + item.sn + ' '  + item.givenname + '</td>';
		else
			contacts += '<td id="cd-emp_' + index + '" class="cn emp-td col-padding right-border" style="width:200px;">' + item.givenname + ' '  + item.sn + '</td>';
		contacts += '<td id="dept-emp_' + index + '" class="dept emp-td col-padding" style="width:200px;">' + item.department + '</td>';
		contacts += '<td id="loc-emp_' + index + '" class="loc emp-td col-padding left-border" style="width:200px;">' + item.office + '</td></tr>';
	});
	contacts += '</table>';	
	$('#email_contact_book').append(contacts);
	set_contact_properties();	

	//console.log($('#email_contact_book').innerWidth())
	//console.log($('.emp_x').css('width'))
}

function display_email_validation(msg){
	var w = ($('.modal-content').width() - $('.email-validatrion-error').width()) /2;
	$('.email-validatrion-error').css('left',w);
	$('.email-validatrion-error .error-message').html(msg);
	$('.email-validatrion-error .expandable-element').height($('.email-validatrion-error .error-message').prop('scrollHeight')+2);
	$('.email-validatrion-error .email_close_btn').show();
}

function close_email_validation(){
	$('.email-validatrion-error .expandable-element').height(0);
	$('.email-validatrion-error .email_close_btn').hide();
}
</script>

