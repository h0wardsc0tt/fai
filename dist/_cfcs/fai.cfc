<cfcomponent> 
   	<cffunction name="Admin" output="false" returntype="any" access="public">
        <cfargument type="string" name="cmd" dbvarname="@cmd" required="yes">
        <cfargument type="string" name="newvalue" dbvarname="@newvalue" required="no">      
		<cfargument type="string" name="previousValue" dbvarname="@previousValue" required="no">
		<cfargument type="string" name="table" dbvarname="@table" required="yes">

        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="admin" datasource="#APPLICATION.FAI_Datasource#" >               
            	<cfif isDefined("Arguments.cmd")>
                    <cfprocparam
                    	cfsqltype="cf_sql_char"
                     	type="In"
                   		value=#Arguments.cmd#
                        null="#NOT len(trim(Arguments.cmd))#">
                <cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_char"
                      	type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.newvalue")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.newvalue#
                        null="#NOT len(trim(Arguments.newvalue))#">
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                                        
				<cfif isDefined("Arguments.previousValue")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.previousValue#
                        null="#NOT len(trim(Arguments.previousValue))#">
              <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
              
				<cfif isDefined("Arguments.table")>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							value=#Arguments.table#
							null="#NOT len(trim(Arguments.table))#">
				<cfelse>
						<cfprocparam
							cfsqltype="cf_sql_char"
							type="In"
							null="Yes">
				</cfif> 
                                                                                                              
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="status"
                    >
                                        
                <cfprocresult name="admin">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
			returnResult['cmd'] = Arguments.cmd;
			returnResult['newvalue'] = Arguments.newvalue;
			returnResult['previousValue'] = Arguments.previousValue;
			returnResult['table'] = Arguments.table;
			returnResult['dropdown'] = admin;
			returnResult['status'] = status;
		</cfscript>  

        <cfreturn returnResult />
    </cffunction>
 

   <cffunction name="filter_export" output="false" returntype="query" access="public">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="yes">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="yes">
        <cfargument type="string" name="fromDate" dbvarname="@fromDate" required="no">
        <cfargument type="string" name="toDate" dbvarname="@toDate" required="no">
        <cfargument type="string" name="supplier" dbvarname="@supplier" required="no">
        <cfargument type="string" name="custom" dbvarname="@custom" required="no">
        <cfargument type="string" name="sortcolumn" dbvarname="@sortcolumn" required="no">
        <cfargument type="string" name="sortorder" dbvarname="@sortorder" required="no">
        <cfargument type="string" name="datecompleted_from" dbvarname="@datecompleted_from" required="no">
        <cfargument type="string" name="datecompleted_to" dbvarname="@datecompleted_to" required="no">
        <cfargument type="string" name="needdate_from" dbvarname="@needdate_from" required="no">
        <cfargument type="string" name="needdate_to" dbvarname="@needdate_to" required="no">
        <cfargument type="string" name="ricompletedate_from" dbvarname="@ricompletedate_from" required="no">
        <cfargument type="string" name="ricompletedate_to" dbvarname="@ricompletedate_to" required="no">
        <cfargument type="string" name="itemnumber" dbvarname="@itemnumber" required="no">
        <cfargument type="string" name="distributormanufacturer" dbvarname="@distributormanufacturer" required="no">
        <cfargument type="string" name="finalresults" dbvarname="@finalresults" required="no">
        <cfargument type="string" name="description" dbvarname="@description" required="no">
        <cfargument type="string" name="inspector" dbvarname="@inspector" required="no">
        <cfargument type="string" name="revision" dbvarname="@revision" required="no">
        <cfargument type="any" name="id" dbvarname="@id" required="no">


        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="fai_filter_export" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.pageNumber#>                
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.itemsPerPage#> 
                                     
				<cfif isDefined("Arguments.fromDate")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.fromDate#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                <cfif isDefined("Arguments.toDate")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.toDate#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                <cfif isDefined("Arguments.supplier")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.supplier#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif>

                <cfif isDefined("Arguments.custom")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.custom#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

 
               <cfif isDefined("Arguments.sortcolumn")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortcolumn#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                
               <cfif isDefined("Arguments.sortorder")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortorder#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

                <cfif isDefined("Arguments.datecompleted_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.datecompleted_from#"
                        null="#NOT len(trim(Arguments.datecompleted_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.datecompleted_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.datecompleted_to#"
                        null="#NOT len(trim(Arguments.datecompleted_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                   
                <cfif isDefined("Arguments.needdate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.needdate_from#"
                        null="#NOT len(trim(Arguments.needdate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.needdate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.needdate_to#"
                        null="#NOT len(trim(Arguments.needdate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                   
                <cfif isDefined("Arguments.ricompletedate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.ricompletedate_from#"
                        null="#NOT len(trim(Arguments.ricompletedate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.ricompletedateate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.ricompletedateate_to#"
                        null="#NOT len(trim(Arguments.ricompletedateate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                   
                <cfif isDefined("Arguments.itemnumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.itemnumber#"
                        null="#NOT len(trim(Arguments.itemnumber))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                 
                <cfif isDefined("Arguments.distributormanufacturer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.distributormanufacturer#"
                        null="#NOT len(trim(Arguments.distributormanufacturer))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                     
                <cfif isDefined("Arguments.finalresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.finalresults#"
                        null="#NOT len(trim(Arguments.finalresults))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                  
                <cfif isDefined("Arguments.description")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.description#"
                        null="#NOT len(trim(Arguments.description))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                       
                <cfif isDefined("Arguments.inspector")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.inspector#"
                        null="#NOT len(trim(Arguments.inspector))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.revision")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.revision#"
                        null="#NOT len(trim(Arguments.revision))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
           
            	<cfif isDefined("Arguments.id")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
               	<cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 
                                      
                <cfprocresult name="filter_export">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
                <cfdump var='"results"'>	
            </cfcatch>
		</cftry>
        <cfreturn filter_export />
    </cffunction>

   <cffunction name="custom" output="false" returntype="query" access="public">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="yes">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="yes">
        <cfargument type="string" name="fromDate" dbvarname="@fromDate" required="no">
        <cfargument type="string" name="toDate" dbvarname="@toDate" required="no">
        <cfargument type="string" name="supplier" dbvarname="@supplier" required="no">
        <cfargument type="string" name="custom" dbvarname="@custom" required="no">
        <cfargument type="string" name="sortcolumn" dbvarname="@sortcolumn" required="no">
        <cfargument type="string" name="sortorder" dbvarname="@sortorder" required="no">


        <cfargument type="string" name="datecompleted_from" dbvarname="@datecompleted_from" required="no">
        <cfargument type="string" name="datecompleted_to" dbvarname="@datecompleted_to" required="no">
        <cfargument type="string" name="needdate_from" dbvarname="@needdate_from" required="no">
        <cfargument type="string" name="needdate_to" dbvarname="@needdate_to" required="no">
        <cfargument type="string" name="ricompletedate_from" dbvarname="@ricompletedate_from" required="no">
        <cfargument type="string" name="ricompletedate_to" dbvarname="@ricompletedate_to" required="no">
        <cfargument type="string" name="itemnumber" dbvarname="@itemnumber" required="no">
        <cfargument type="string" name="distributormanufacturer" dbvarname="@distributormanufacturer" required="no">
        <cfargument type="string" name="finalresults" dbvarname="@finalresults" required="no">
        <cfargument type="string" name="description" dbvarname="@description" required="no">
        <cfargument type="string" name="inspector" dbvarname="@inspector" required="no">
        <cfargument type="string" name="revision" dbvarname="@revision" required="no">
        <cfargument type="any" name="project" dbvarname="@project" required="no">
        <cfargument type="any" name="id" dbvarname="@id" required="no">


        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="fai_custom" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.pageNumber#>                
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.itemsPerPage#> 
                                     
				<cfif isDefined("Arguments.fromDate")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.fromDate#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                <cfif isDefined("Arguments.toDate")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.toDate#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                <cfif isDefined("Arguments.supplier")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.supplier#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif>

                <cfif isDefined("Arguments.custom")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.custom#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

 
               <cfif isDefined("Arguments.sortcolumn")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortcolumn#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 
                
               <cfif isDefined("Arguments.sortorder")>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        value=#Arguments.sortorder#>
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_char"
                        null="Yes">
                </cfif> 

                <cfif isDefined("Arguments.datecompleted_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.datecompleted_from#"
                        null="#NOT len(trim(Arguments.datecompleted_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.datecompleted_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.datecompleted_to#"
                        null="#NOT len(trim(Arguments.datecompleted_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                   
                <cfif isDefined("Arguments.needdate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.needdate_from#"
                        null="#NOT len(trim(Arguments.needdate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.needdate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.needdate_to#"
                        null="#NOT len(trim(Arguments.needdate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                   
                <cfif isDefined("Arguments.ricompletedate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.ricompletedate_from#"
                        null="#NOT len(trim(Arguments.ricompletedate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.ricompletedateate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.ricompletedateate_to#"
                        null="#NOT len(trim(Arguments.ricompletedateate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                   
                <cfif isDefined("Arguments.itemnumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.itemnumber#"
                        null="#NOT len(trim(Arguments.itemnumber))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                 
                <cfif isDefined("Arguments.distributormanufacturer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.distributormanufacturer#"
                        null="#NOT len(trim(Arguments.distributormanufacturer))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                     
                <cfif isDefined("Arguments.finalresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.finalresults#"
                        null="#NOT len(trim(Arguments.finalresults))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                  
                <cfif isDefined("Arguments.description")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.description#"
                        null="#NOT len(trim(Arguments.description))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                       
                <cfif isDefined("Arguments.inspector")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.inspector#"
                        null="#NOT len(trim(Arguments.inspector))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.revision")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.revision#"
                        null="#NOT len(trim(Arguments.revision))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.project")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.project#"
                        null="#NOT len(trim(Arguments.project))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
           
            	<cfif isDefined("Arguments.id")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
               	<cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 
                                      
                <cfprocresult name="fai_custom">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn fai_custom />
    </cffunction>

   <cffunction name="wip" output="false" returntype="query" access="public">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="yes">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="yes">
        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="fai_wip" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.pageNumber#>                
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.itemsPerPage#>                  
                <cfprocresult name="fai_wip">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn fai_wip />
    </cffunction>
    
   <cffunction name="fpy" output="false" returntype="query" access="public">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="yes">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="yes">
        <cfargument type="any" name="supplier" dbvarname="@supplier" required="yes">
        <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="fai_fpy" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.pageNumber#>                
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value=#Arguments.itemsPerPage#>                 
                <cfprocparam
                    cfsqltype="cf_sql_char"
                    value=#Arguments.supplier#>                 
                <cfprocresult name="qry_fpy">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_fpy />
    </cffunction>

   <cffunction name="engineering_wip" output="false" returntype="query" access="public">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="yes">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="yes">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="fai_ewip" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="In"
                    value=#Arguments.pageNumber#>                
                <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="In"
                    value=#Arguments.itemsPerPage#>                 
                <cfprocresult name="qry_engineering_wip">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_engineering_wip />
    </cffunction>

	<cffunction name="enableDisableAttachment" returntype="any" output="false"  access="public">
        <cfargument type="string" name="index"  required="yes">
        <cfargument type="string" name="guid" dbvarname="@guid" required="yes">
        <cfargument type="string" name="isactive" required="yes">
        <cfargument type="string" name="id" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="fai_enableDisableAttachment" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocparam
                    cfsqltype="cf_sql_bit"
                    value="#Arguments.isactive#">                  
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.guid#">                  
            </cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	returnResult = StructNew();
			returnResult['guid'] = "#Arguments.guid#";
			returnResult['index'] = "#Arguments.index#";
			returnResult['isactive'] = "#Arguments.isactive#";
			returnResult['id'] = "#Arguments.id#";
			returnResult['results'] = results;
			serializer = new lib.JsonSerializer();
			response = serializer.serialize(returnResult);
			StructClear(returnResult);
		 </cfscript>
        <cfreturn response />
    </cffunction>   

	<cffunction name="changeAttachmentName" returntype="any" output="false"  access="public">
        <cfargument type="string" name="filename" dbvarname="@fileName" required="yes">
        <cfargument type="string" name="index"  required="yes">
        <cfargument type="string" name="guid" dbvarname="@guid" required="yes">
        <cfargument type="string" name="prefix" required="yes">
        <cfargument type="string" name="id" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="fai_changeAttachmentName" datasource="#APPLICATION.FAI_Datasource#" >         
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.fileName#">                  
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.guid#">                  
            </cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	returnResult = StructNew();
			returnResult['filename'] = "#Arguments.filename#";
			returnResult['guid'] = "#Arguments.guid#";
			returnResult['index'] = "#Arguments.index#";
			returnResult['prefix'] = "#Arguments.prefix#";
			returnResult['id'] = "#Arguments.id#";
			returnResult['results'] = results;
			serializer = new lib.JsonSerializer();
			response = serializer.serialize(returnResult);
			StructClear(returnResult);
		 </cfscript>
        <cfreturn response />
    </cffunction>
    
	<cffunction name="DropDowns" output="false" returntype="query" access="public">    
        <cfargument type="string" name="storedProcedure" required="yes">
            <cfstoredproc procedure="#Arguments.storedProcedure#" datasource="#APPLICATION.FAI_Datasource#" >
  				<cfprocresult name="qry_getSelect">  
           </cfstoredproc>
         <cfreturn qry_getSelect />
	</cffunction>     

	<cffunction name="AddNewAttachment" returntype="string" output="false"  access="public" description="Add New Attachment">
        <cfargument type="string" name="id"  dbvarname="@id" required="yes">
        <cfargument type="binary" name="file"  dbvarname="@file" required="yes">
        <cfargument type="string" name="fileName" dbvarname="@fileName" required="yes">
        <cfargument type="string" name="prefix" dbvarname="@prefix" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="addNewAttachment" datasource="#APPLICATION.FAI_Datasource#" >         
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value="#Arguments.id#">
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.fileName#">
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.prefix#">
               <cfprocparam
                  cfsqltype="cf_sql_blob"
                  value="#Arguments.file#"> 

 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  
            </cfcatch>
		</cftry>	
		<cfreturn results />
	</cffunction>     

    <cffunction name="get_Attachment" output="false" returntype="query" access="public" description="Return binary file">
    	<cfargument type="string" name="guid" dbvarname="@guid" required="yes">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="getAttachment" datasource="#APPLICATION.FAI_Datasource#" >         
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    value="#Arguments.guid#">
               <cfprocresult name="qry_getAttachment">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#">  	
            </cfcatch>
		</cftry>
        <cfreturn qry_getAttachment />
    </cffunction>
 
     <cffunction name="searchByFAINo" output="false" returntype="query" access="public" description="Return FAI Row">
    	<cfargument type="string" name="id" dbvarname="@id" required="yes">
         <cfset results = "OK">  
		 <cftry>
            <cfstoredproc procedure="qrySearchByFAINo" datasource="#APPLICATION.FAI_Datasource#" >         
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    value="#Arguments.id#">
               <cfprocresult name="qry_searchByFAINo">  
 			</cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
		</cftry>
        <cfreturn qry_searchByFAINo />
    </cffunction> 
    
	<cffunction name="insertAttachment" returntype="string" output="false"  access="public" description="Insert Attachment">
        <cfargument type="string" name="id" dbvarname="@id" required="yes">
        <cfargument type="binary" name="file"  dbvarname="@file" required="yes">
        <cfargument type="string" name="fileName" dbvarname="@fileName" required="yes">
        <cfargument type="string" name="prefix" dbvarname="@prefix" required="yes">
        <cfset results = "OK">  
        <cftry>
            <cfstoredproc procedure="dbo.uploadAttachment" datasource="#APPLICATION.FAI_Datasource#" >         
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="In"
                    value=#Arguments.id#>
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="In"
                    value="#Arguments.fileName#">                  
                <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="In"
                    value="#Arguments.prefix#">                  
                <cfprocparam
                    cfsqltype="cf_sql_blob"
                    type="In"
                    value="#Arguments.file#"> 
               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="guid"
                    >
            </cfstoredproc>
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	returnResult = StructNew();
			returnResult['id'] = "#Arguments.id#";
			returnResult['guid'] = guid;
			returnResult['filename'] = "#Arguments.fileName#";
			returnResult['prefix'] = "#Arguments.prefix#";
			returnResult['results'] = results;
			returnResult['isActive'] = '1';
			serializer = new lib.JsonSerializer();
			response = serializer.serialize(returnResult);
			StructClear(returnResult);
		 </cfscript>
        <cfreturn response />
    </cffunction>

	<cffunction name="FAISelect" output="false" returntype="any" access="public" description="Select a FAI or filtered list">
        <cfargument type="any" name="id" dbvarname="@id" required="no">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="no">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="no">
        <cfargument type="string" name="storedProcedure" required="yes">
        <cfargument type="string" name="s_description" dbvarname="@s_description" required="no">
        <cfargument type="string" name="s_distributormanufacturer" dbvarname="@distributormanufacturer" required="no">
        <cfargument type="string" name="s_inspector" dbvarname="@s_inspector" required="no">
        <cfargument type="string" name="s_itemnumber" dbvarname="@s_itemnumber" required="no">
        <cfargument type="string" name="s_needdate_from" dbvarname="@s_needdate_from" required="no">
        <cfargument type="string" name="s_needdate_to" dbvarname="@s_needdate_to" required="no">
        <cfargument type="string" name="s_datecompleted_from" dbvarname="@s_datecompleted_from" required="no">
        <cfargument type="string" name="s_datecompleted_to" dbvarname="@s_datecompleted_to" required="no">
        <cfargument type="string" name="s_revision" dbvarname="@s_revision" required="no">
        <cfargument type="string" name="s_finalresults" dbvarname="@s_finalresults" required="no">
        <cfargument type="string" name="s_ricompletedate_from" dbvarname="@s_ricompletedate_from" required="no">
        <cfargument type="string" name="s_ricompletedateate_to" dbvarname="@s_ricompletedateate_to" required="no">
        <cfargument type="string" name="sortColumn" dbvarname="@sortColumn" required="no">
        <cfargument type="string" name="sortOrder" dbvarname="@sortOrder" required="no">
        <cfargument type="string" name="custom" dbvarname="@custom" required="no">
                                
        <cfset results = "OK">
		<cftry>
 			<cfstoredproc procedure="#Arguments.storedProcedure#" datasource="#APPLICATION.FAI_Datasource#" >  
            
            	<cfif isDefined("Arguments.id")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
               	<cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.pageNumber")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.pageNumber#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>

				<cfif isDefined("Arguments.itemsPerPage")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.itemsPerPage#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>
                    
                <cfif isDefined("Arguments.s_description")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_description#"
                        null="#NOT len(trim(Arguments.s_description))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_distributormanufacturer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_distributormanufacturer#"
                        null="#NOT len(trim(Arguments.s_distributormanufacturer))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_inspector")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_inspector#"
                        null="#NOT len(trim(Arguments.s_inspector))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_itemnumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_itemnumber#"
                        null="#NOT len(trim(Arguments.s_itemnumber))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_needdate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_needdate_from#"
                        null="#NOT len(trim(Arguments.s_needdate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_needdate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_needdate_to#"
                        null="#NOT len(trim(Arguments.s_needdate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.s_datecompleted_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_datecompleted_from#"
                        null="#NOT len(trim(Arguments.s_datecompleted_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_datecompleted_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_datecompleted_to#"
                        null="#NOT len(trim(Arguments.s_datecompleted_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_finalresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_finalresults#"
                        null="#NOT len(trim(Arguments.s_finalresults))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_ricompletedate_from")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_ricompletedate_from#"
                        null="#NOT len(trim(Arguments.s_ricompletedate_from))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_ricompletedateate_to")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_ricompletedateate_to#"
                        null="#NOT len(trim(Arguments.s_ricompletedateate_to))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.s_revision")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.s_revision#"
                        null="#NOT len(trim(Arguments.s_revision))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.sortColumn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.sortColumn#"
                        null="#NOT len(trim(Arguments.sortColumn))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

               <cfif isDefined("Arguments.sortOrder")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.sortOrder#"
                        null="#NOT len(trim(Arguments.sortOrder))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

               <cfif isDefined("Arguments.custom")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.custom#"
                        null="#NOT len(trim(Arguments.custom))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

            	<cfprocresult name="qry_getSelect">     
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
		</cfscript>  
           
        <cfreturn qry_getSelect />
	</cffunction>      

	<cffunction name="FAIUpdate" output="false" returntype="any" access="public" description="Update or Create FAI">
        <cfargument type="string" name="storedProcedure" required="yes">
        <cfargument type="string" name="approvedby" dbvarname="@approvedby" required="no">
        <cfargument type="string" name="attachments" dbvarname="@attachments" required="no">
        <cfargument type="string" name="buyer" dbvarname="@buyer" required="no">
        <cfargument type="string" name="closurecomments" dbvarname="@closurecomments" required="no" default="">
        <cfargument type="string" name="comments" dbvarname="@comments" required="no">
        <cfargument type="string" name="company1" dbvarname="@company1" required="no">
        <cfargument type="string" name="currentlocation" dbvarname="@currentlocation" required="no">
        <cfargument type="string" name="currentlylocatedasof" dbvarname="@currentlylocatedasof" required="no">
        <cfargument type="string" name="currentstatus" dbvarname="@currentstatus" required="no">
        <cfargument type="string" name="datecode" dbvarname="@datecode" required="no">
        <cfargument type="string" name="datecompleted" dbvarname="@datecompleted" required="no">
        <cfargument type="string" name="dateinengineering" dbvarname="@dateinengineering" required="no">
        <cfargument type="string" name="dateinmanufacturing" dbvarname="@dateinmanufacturing" required="no">
        <cfargument type="string" name="dateinqalab" dbvarname="@dateinqalab" required="no">
        <cfargument type="string" name="dateinsmt" dbvarname="@dateinsmt" required="no">
        <cfargument type="string" name="datereceived" dbvarname="@datereceived" required="no">
        <cfargument type="string" name="datesenttoqe" dbvarname="@datesenttoqe" required="no">
        <cfargument type="string" name="daystocomplete" dbvarname="@daystocomplete" required="no">
        <cfargument type="string" name="description" dbvarname="@description" required="no">
        <cfargument type="string" name="distributormanufacturer" dbvarname="@distributormanufacturer" required="no">
        <cfargument type="string" name="dmrref" dbvarname="@dmrref" required="no">
        <cfargument type="string" name="engapprover" dbvarname="@engapprover" required="no">
        <cfargument type="string" name="engineeringdatecomplete" dbvarname="@engineeringdatecomplete" required="no">
        <cfargument type="string" name="engineeringemail" dbvarname="@engineeringemail" required="no">
        <cfargument type="string" name="engineeringresults" dbvarname="@engineeringresults" required="no">
        <cfargument type="string" name="engqtyreceived" dbvarname="@engqtyreceived" required="no">
        <cfargument type="string" name="failevel" dbvarname="@failevel" required="no">
        <cfargument type="string" name="faistatus" dbvarname="@faistatus" required="no">
        <cfargument type="string" name="faitype" dbvarname="@faitype" required="no">
        <cfargument type="string" name="finalresults" dbvarname="@finalresults" required="no">
        <cfargument type="string" name="id" dbvarname="@id" required="no">
        <cfargument type="string" name="inspector" dbvarname="@inspector" required="no">
        <cfargument type="string" name="itemnumber" dbvarname="@itemnumber" required="no">
        <cfargument type="string" name="lotqtyrecd" dbvarname="@lotqtyrecd" required="no">
        <cfargument type="string" name="manfpassfail" dbvarname="@manfpassfail" required="no">
        <cfargument type="string" name="manfresults" dbvarname="@manfresults" required="no">
        <cfargument type="string" name="manufacturingcompletedate" dbvarname="@manufacturingcompletedate" required="no">
        <cfargument type="string" name="manufacturingnewpn" dbvarname="@manufacturingnewpn" required="no">
        <cfargument type="string" name="manufacturingnewpn2" dbvarname="@manufacturingnewpn2" required="no">
        <cfargument type="string" name="manufacturingqtycompleted" dbvarname="@manufacturingqtycompleted" required="no">
        <cfargument type="string" name="manufacturingqtyrecieved" dbvarname="@manufacturingqtyrecieved" required="no">
        <cfargument type="string" name="needdate" dbvarname="@needdate" required="no">
        <cfargument type="string" name="pilot" dbvarname="@pilot" required="no">
        <cfargument type="string" name="po" dbvarname="@po" required="no">
        <cfargument type="string" name="project" dbvarname="@project" required="no">
        <cfargument type="string" name="projectengineer" dbvarname="@projectengineer" required="no" default="">
        <cfargument type="string" name="qadate" dbvarname="@qadate" required="no">
        <cfargument type="string" name="qalabdatecomplete" dbvarname="@qalabdatecomplete" required="no">
        <cfargument type="string" name="qalabqtycomplete" dbvarname="@qalabqtycomplete" required="no">
        <cfargument type="string" name="qalabqtyreceived" dbvarname="@qalabqtyreceived" required="no">
        <cfargument type="string" name="qatestlabnewpn" dbvarname="@qatestlabnewpn" required="no">
        <cfargument type="string" name="qatestlabnewpn2" dbvarname="@qatestlabnewpn2" required="no">
        <cfargument type="string" name="qatestlabpassfail" dbvarname="@qatestlabpassfail" required="no">
        <cfargument type="string" name="qatestlabreport" dbvarname="@qatestlabreport" required="no">
        <cfargument type="string" name="reasonforfai" dbvarname="@reasonforfai" required="no">
        <cfargument type="string" name="revision" dbvarname="@revision" required="no">
        <cfargument type="string" name="ricompletedate" dbvarname="@ricompletedate" required="no">
        <cfargument type="string" name="rievalutionresults" dbvarname="@rievalutionresults" required="no" default="">
        <cfargument type="string" name="ripassfail" dbvarname="@ripassfail" required="no">
        <cfargument type="string" name="riqtyinspected" dbvarname="@riqtyinspected" required="no">
        <cfargument type="string" name="riqtyrejected" dbvarname="@riqtyrejected" required="no">
        <cfargument type="string" name="rmonumber" dbvarname="@rmonumber" required="no">
        <cfargument type="string" name="rohscert" dbvarname="@rohscert" required="no">
        <cfargument type="string" name="smtcompletedate" dbvarname="@smtcompletedate" required="no">
        <cfargument type="string" name="smtnewpn" dbvarname="@smtnewpn" required="no">
        <cfargument type="string" name="smtnewpn2" dbvarname="@smtnewpn2" required="no">
        <cfargument type="string" name="smtpassfail" dbvarname="@smtpassfail" required="no">
        <cfargument type="string" name="smtqty" dbvarname="@smtqty" required="no">
        <cfargument type="string" name="smtqtycompleted" dbvarname="@smtqtycompleted" required="no">
        <cfargument type="string" name="smtqtyreceived" dbvarname="@smtqtyreceived" required="no">
        <cfargument type="string" name="smtresults" dbvarname="@smtresults" required="no">
        <cfargument type="string" name="supplierreport" dbvarname="@supplierreport" required="no">
        <cfargument type="string" name="thapassfail" dbvarname="@thapassfail" required="no">
        <cfargument type="string" name="tharesults" dbvarname="@tharesults" required="no">
        
        <cfargument type="string" name="reportrequiredck" dbvarname="@reportrequiredck" required="no">
        <cfargument type="string" name="engineeringwock" dbvarname="@engineeringwock" required="no">
        <cfargument type="string" name="previousevidck" dbvarname="@previousevidck" required="no">
        <cfargument type="string" name="scarck" dbvarname="@scarck" required="no">
        <cfargument type="string" name="limteddurationck" dbvarname="@limteddurationck" required="no">
        <cfargument type="string" name="lotnumberonlyck" dbvarname="@lotnumberonlyck" required="no">
        <cfargument type="string" name="inspectallck" dbvarname="@inspectallck" required="no">
        <cfargument type="string" name="inspectcriticalck" dbvarname="@inspectcriticalck" required="no">
        <cfargument type="string" name="inspectcircledck" dbvarname="@inspectcircledck" required="no">
        <cfargument type="string" name="oktodestroyck" dbvarname="@oktodestroyck" required="no">
        <cfargument type="string" name="ingorepkgrohsfitcheckck" dbvarname="@ingorepkgrohsfitcheckck" required="no">
        <cfargument type="string" name="ignorenotesck" dbvarname="@ignorenotesck" required="no">
        <cfargument type="string" name="visualck" dbvarname="@visualck" required="no">
        <cfargument type="string" name="sortingonlyck" dbvarname="@sortingonlyck" required="no">
        <cfargument type="string" name="contactuponreceiptck" dbvarname="@contactuponreceiptck" required="no">
        <cfargument type="string" name="removedtsck" dbvarname="@removedtsck" required="no">
        <cfargument type="string" name="igorefitcheckck" dbvarname="@igorefitcheckck" required="no">
        
        <cfargument type="string" name="engineeringwo" dbvarname="@engineeringwo" required="no">
        <cfargument type="string" name="previousevid" dbvarname="@previousevid" required="no">
        <cfargument type="string" name="limtedduration" dbvarname="@limtedduration" required="no">
        <cfargument type="string" name="lotnumberonly" dbvarname="@lotnumberonly" required="no">
        <cfargument type="string" name="scar" dbvarname="@scar" required="no">
        <cfargument type="string" name="ignoretextureck" dbvarname="@ignoretextureck" required="no">
        <cfargument type="string" name="ignorepartmarketingck" dbvarname="@ignorepartmarketingck" required="no">
        <cfargument type="string" name="additionalinspectioninstructions" dbvarname="@additionalinspectioninstructions" required="no" default="">
        <cfargument type="string" name="rmtoengafterinspectck" dbvarname="@rmtoengafterinspectck" required="no">

        <cfset results = "OK">

		<cftry>
 			<cfstoredproc procedure="#Arguments.storedProcedure#" datasource="#APPLICATION.FAI_Datasource#" >  
                
                <cfif isDefined("Arguments.id")>
                    <cfprocparam
                        cfsqltype="cf_sql_integer"
                        type="In"
                        value="#Arguments.id#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_integer"
                        type="In"
                        null="Yes">
                </cfif>
 
				 <cfif isDefined("Arguments.approvedby")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.approvedby#"
                    >
                <cfelse>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.buyer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.buyer#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.closurecomments")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.closurecomments#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.comments")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.comments#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.company1")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.company1#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.currentlocation")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.currentlocation#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

               <cfif isDefined("Arguments.currentlylocatedasof")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.currentlylocatedasof#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                 
                <cfif isDefined("Arguments.currentstatus")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.currentstatus#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.datecode")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datecode#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.datecompleted")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datecompleted#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinengineering")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinengineering#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinmanufacturing")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinmanufacturing#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinqalab")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinqalab#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinsmt")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinsmt#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.datereceived")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datereceived#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

               
                <cfif isDefined("Arguments.datesenttoqe")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datesenttoqe#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.daystocomplete")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.daystocomplete#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.description")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.description#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.distributormanufacturer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.distributormanufacturer#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dmrref")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dmrref#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engapprover")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engapprover#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engineeringdatecomplete")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringdatecomplete#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engineeringemail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringemail#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engineeringresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringresults#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

               
                <cfif isDefined("Arguments.engqtyreceived")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engqtyreceived#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.failevel")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.failevel#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.faistatus")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.faistatus#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.faitype")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.faitype#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.finalresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.finalresults#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.inspector")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.inspector#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.itemnumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.itemnumber#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.lotqtyrecd")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.lotqtyrecd#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manfpassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manfpassfail#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manfresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manfresults#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingcompletedate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingcompletedate#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingnewpn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingnewpn#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
               <!------>
                <cfif isDefined("Arguments.manufacturingnewpn2")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingnewpn2#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingqtycompleted")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingqtycompleted#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingqtyrecieved")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingqtyrecieved#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.needdate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.needdate#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.pilot")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.pilot#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.po")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.po#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.project")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.project#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.projectengineer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.projectengineer#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qadate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qadate#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qalabdatecomplete")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qalabdatecomplete#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
 
                 
                <cfif isDefined("Arguments.qalabqtycompleted")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qalabqtycompleted#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qalabqtyreceived")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qalabqtyreceived#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                 <!------>
                <cfif isDefined("Arguments.qatestlabnewpn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabnewpn#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
             
                <cfif isDefined("Arguments.qatestlabnewpn2")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabnewpn2#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
              
                <cfif isDefined("Arguments.qatestlabpassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabpassfail#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qatestlabreport")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabreport#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.reasonforfai")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.reasonforfai#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.revision")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.revision#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.ricompletedate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.ricompletedate#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.rievalutionresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.rievalutionresults#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.ripassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.ripassfail#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.riqtyinspected")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.riqtyinspected#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.riqtyrejected")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.riqtyrejected#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.rmonumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.rmonumber#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.rohscert")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.rohscert#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtcompletedate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtcompletedate#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtnewpn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtnewpn#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                <!------>
                <cfif isDefined("Arguments.smtnewpn2")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtnewpn2#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtpassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtpassfail#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtqty")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtqty#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtqtycompleted")>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtqtycompleted#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

              
                <cfif isDefined("Arguments.smtqtyreceived")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtqtyreceived#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtresults#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
               
                <cfif isDefined("Arguments.supplierreport")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.supplierreport#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.thapassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.thapassfail#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.tharesults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.tharesults#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>



               <cfif isDefined("Arguments.reportrequiredck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.reportrequiredck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>

             <cfif isDefined("Arguments.engineeringwock")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.engineeringwock#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.previousevidck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.previousevidck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.scarck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.scarck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.limteddurationck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.limteddurationck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.lotnumberonlyck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.lotnumberonlyck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.inspectallck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.inspectallck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.inspectcriticalck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.inspectcriticalck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.inspectcircledck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.inspectcircledck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.oktodestroyck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.oktodestroyck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.ingorepkgrohsfitcheckck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ingorepkgrohsfitcheckck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.ignorenotesck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ignorenotesck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.visualck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.visualck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.sortingonlyck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.sortingonlyck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.contactuponreceiptck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.contactuponreceiptck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.removedtsck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.removedtsck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
              <cfif isDefined("Arguments.igorefitcheckck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.igorefitcheckck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>

               
                <cfif isDefined("Arguments.engineeringwo")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringwo#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.previousevid")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.previousevid#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.limtedduration")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.limtedduration#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.lotnumberonly")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.lotnumberonly#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.scar")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.scar#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
            
              <cfif isDefined("Arguments.ignoretextureck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ignoretextureck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
              <cfif isDefined("Arguments.ignorepartmarketingck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ignorepartmarketingck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
                         
               
                <cfif isDefined("Arguments.additionalinspectioninstructions")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.additionalinspectioninstructions#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                 <cfif isDefined("Arguments.rmtoengafterinspectck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.rmtoengafterinspectck#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>             
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="Out" 
                    value=0
                    variable="idenity"
                    >

               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="sqlRequest"
                    >

            	<cfprocresult name="qry_getSelect">     
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
			returnResult['idenity'] = idenity;
			returnResult['sqlRequest'] = sqlRequest;
		</cfscript>  
           
        <cfreturn returnResult />
	</cffunction>


	<cffunction name="FAIInsert" output="false" returntype="any" access="public" description="Create FAI">
        <cfargument type="string" name="storedProcedure" required="yes">
        <cfargument type="string" name="approvedby" dbvarname="@approvedby" required="no">
        <cfargument type="string" name="attachments" dbvarname="@attachments" required="no">
        <cfargument type="string" name="buyer" dbvarname="@buyer" required="no">
        <cfargument type="string" name="closurecomments" dbvarname="@closurecomments" required="no" default="">
        <cfargument type="string" name="comments" dbvarname="@comments" required="no">
        <cfargument type="string" name="company1" dbvarname="@company1" required="no">
        <cfargument type="string" name="currentlocation" dbvarname="@currentlocation" required="no">
        <cfargument type="string" name="currentlylocatedasof" dbvarname="@currentlylocatedasof" required="no">
        <cfargument type="string" name="currentstatus" dbvarname="@currentstatus" required="no">
        <cfargument type="string" name="datecode" dbvarname="@datecode" required="no">
        <cfargument type="string" name="datecompleted" dbvarname="@datecompleted" required="no">
        <cfargument type="string" name="dateinengineering" dbvarname="@dateinengineering" required="no">
        <cfargument type="string" name="dateinmanufacturing" dbvarname="@dateinmanufacturing" required="no">
        <cfargument type="string" name="dateinqalab" dbvarname="@dateinqalab" required="no">
        <cfargument type="string" name="dateinsmt" dbvarname="@dateinsmt" required="no">
        <cfargument type="string" name="datereceived" dbvarname="@datereceived" required="no">
        <cfargument type="string" name="datesenttoqe" dbvarname="@datesenttoqe" required="no">
        <cfargument type="string" name="daystocomplete" dbvarname="@daystocomplete" required="no">
        <cfargument type="string" name="description" dbvarname="@description" required="no">
        <cfargument type="string" name="distributormanufacturer" dbvarname="@distributormanufacturer" required="no">
        <cfargument type="string" name="dmrref" dbvarname="@dmrref" required="no">
        <cfargument type="string" name="engapprover" dbvarname="@engapprover" required="no">
        <cfargument type="string" name="engineeringdatecomplete" dbvarname="@engineeringdatecomplete" required="no">
        <cfargument type="string" name="engineeringemail" dbvarname="@engineeringemail" required="no">
        <cfargument type="string" name="engineeringresults" dbvarname="@engineeringresults" required="no">
        <cfargument type="string" name="engqtyreceived" dbvarname="@engqtyreceived" required="no">
        <cfargument type="string" name="failevel" dbvarname="@failevel" required="no">
        <cfargument type="string" name="faistatus" dbvarname="@faistatus" required="no">
        <cfargument type="string" name="faitype" dbvarname="@faitype" required="no">
        <cfargument type="string" name="finalresults" dbvarname="@finalresults" required="no">
        <cfargument type="string" name="id" dbvarname="@id" required="no">
        <cfargument type="string" name="inspector" dbvarname="@inspector" required="no">
        <cfargument type="string" name="itemnumber" dbvarname="@itemnumber" required="no">
        <cfargument type="string" name="lotqtyrecd" dbvarname="@lotqtyrecd" required="no">
        <cfargument type="string" name="manfpassfail" dbvarname="@manfpassfail" required="no">
        <cfargument type="string" name="manfresults" dbvarname="@manfresults" required="no">
        <cfargument type="string" name="manufacturingcompletedate" dbvarname="@manufacturingcompletedate" required="no">
        <cfargument type="string" name="manufacturingnewpn" dbvarname="@manufacturingnewpn" required="no">
        <cfargument type="string" name="manufacturingnewpn2" dbvarname="@manufacturingnewpn2" required="no">
        <cfargument type="string" name="manufacturingqtycompleted" dbvarname="@manufacturingqtycompleted" required="no">
        <cfargument type="string" name="manufacturingqtyrecieved" dbvarname="@manufacturingqtyrecieved" required="no">
        <cfargument type="string" name="needdate" dbvarname="@needdate" required="no">
        <cfargument type="string" name="pilot" dbvarname="@pilot" required="no">
        <cfargument type="string" name="po" dbvarname="@po" required="no">
        <cfargument type="string" name="project" dbvarname="@project" required="no">
        <cfargument type="string" name="projectengineer" dbvarname="@projectengineer" required="no" default="">
        <cfargument type="string" name="qadate" dbvarname="@qadate" required="no">
        <cfargument type="string" name="qalabdatecomplete" dbvarname="@qalabdatecomplete" required="no">
        <cfargument type="string" name="qalabqtycomplete" dbvarname="@qalabqtycomplete" required="no">
        <cfargument type="string" name="qalabqtyreceived" dbvarname="@qalabqtyreceived" required="no">
        <cfargument type="string" name="qatestlabnewpn" dbvarname="@qatestlabnewpn" required="no">
        <cfargument type="string" name="qatestlabnewpn2" dbvarname="@qatestlabnewpn2" required="no">
        <cfargument type="string" name="qatestlabpassfail" dbvarname="@qatestlabpassfail" required="no">
        <cfargument type="string" name="qatestlabreport" dbvarname="@qatestlabreport" required="no">
        <cfargument type="string" name="reasonforfai" dbvarname="@reasonforfai" required="no">
        <cfargument type="string" name="revision" dbvarname="@revision" required="no">
        <cfargument type="string" name="ricompletedate" dbvarname="@ricompletedate" required="no">
        <cfargument type="string" name="rievalutionresults" dbvarname="@rievalutionresults" required="no" default="">
        <cfargument type="string" name="ripassfail" dbvarname="@ripassfail" required="no">
        <cfargument type="string" name="riqtyinspected" dbvarname="@riqtyinspected" required="no">
        <cfargument type="string" name="riqtyrejected" dbvarname="@riqtyrejected" required="no">
        <cfargument type="string" name="rmonumber" dbvarname="@rmonumber" required="no">
        <cfargument type="string" name="rohscert" dbvarname="@rohscert" required="no">
        <cfargument type="string" name="smtcompletedate" dbvarname="@smtcompletedate" required="no">
        <cfargument type="string" name="smtnewpn" dbvarname="@smtnewpn" required="no">
        <cfargument type="string" name="smtnewpn2" dbvarname="@smtnewpn2" required="no">
        <cfargument type="string" name="smtpassfail" dbvarname="@smtpassfail" required="no">
        <cfargument type="string" name="smtqty" dbvarname="@smtqty" required="no">
        <cfargument type="string" name="smtqtycompleted" dbvarname="@smtqtycompleted" required="no">
        <cfargument type="string" name="smtqtyreceived" dbvarname="@smtqtyreceived" required="no">
        <cfargument type="string" name="smtresults" dbvarname="@smtresults" required="no">
        <cfargument type="string" name="supplierreport" dbvarname="@supplierreport" required="no">
        <cfargument type="string" name="thapassfail" dbvarname="@thapassfail" required="no">
        <cfargument type="string" name="tharesults" dbvarname="@tharesults" required="no">
        
               
        <cfargument type="string" name="reportrequiredck" dbvarname="@reportrequiredck" required="no">
        <cfargument type="string" name="engineeringwock" dbvarname="@engineeringwock" required="no">
        <cfargument type="string" name="previousevidck" dbvarname="@previousevidck" required="no">
        <cfargument type="string" name="scarck" dbvarname="@scarck" required="no">
        <cfargument type="string" name="limteddurationck" dbvarname="@limteddurationck" required="no">
        <cfargument type="string" name="lotnumberonlyck" dbvarname="@lotnumberonlyck" required="no">
        <cfargument type="string" name="inspectallck" dbvarname="@inspectallck" required="no">
        <cfargument type="string" name="inspectcriticalck" dbvarname="@inspectcriticalck" required="no">
        <cfargument type="string" name="inspectcircledck" dbvarname="@inspectcircledck" required="no">
        <cfargument type="string" name="oktodestroyck" dbvarname="@oktodestroyck" required="no">
        <cfargument type="string" name="ingorepkgrohsfitcheckck" dbvarname="@ingorepkgrohsfitcheckck" required="no">
        <cfargument type="string" name="ignorenotesck" dbvarname="@ignorenotesck" required="no">
        <cfargument type="string" name="visualck" dbvarname="@visualck" required="no">
        <cfargument type="string" name="sortingonlyck" dbvarname="@sortingonlyck" required="no">
        <cfargument type="string" name="contactuponreceiptck" dbvarname="@contactuponreceiptck" required="no">
        <cfargument type="string" name="removedtsck" dbvarname="@removedtsck" required="no">
        <cfargument type="string" name="igorefitcheckck" dbvarname="@igorefitcheckck" required="no">
        
        <cfargument type="string" name="engineeringwo" dbvarname="@engineeringwo" required="no">
        <cfargument type="string" name="previousevid" dbvarname="@previousevid" required="no">
        <cfargument type="string" name="limtedduration" dbvarname="@limtedduration" required="no">
        <cfargument type="string" name="lotnumberonly" dbvarname="@lotnumberonly" required="no">
        <cfargument type="string" name="scar" dbvarname="@scar" required="no">
        <cfargument type="string" name="ignoretextureck" dbvarname="@ignoretextureck" required="no">
        <cfargument type="string" name="ignorepartmarketingck" dbvarname="@ignorepartmarketingck" required="no">
        <cfargument type="string" name="additionalinspectioninstructions" dbvarname="@additionalinspectioninstructions" required="no" default="">
        <cfargument type="string" name="rmtoengafterinspectck" dbvarname="@rmtoengafterinspectck" required="no">
<cfset results = "OK">

		<cftry>
 			<cfstoredproc procedure="#Arguments.storedProcedure#" datasource="#APPLICATION.FAI_Datasource#" >  
                
                <cfif isDefined("Arguments.id")>
                    <cfprocparam
                        cfsqltype="cf_sql_integer"
                        type="In"
                        value="#Arguments.id#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_integer"
                        type="In"
                        null="Yes">
                </cfif>
 
				 <cfif isDefined("Arguments.approvedby")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.approvedby#"
                        null="#NOT len(trim(Arguments.approvedby))#"
                    >
                <cfelse>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.buyer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.buyer#"
                         null="#NOT len(trim(Arguments.buyer))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.closurecomments")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.closurecomments#"
                        null="#NOT len(trim(Arguments.closurecomments))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.comments")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.comments#"
                        null="#NOT len(trim(Arguments.comments))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.company1")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.company1#"
                        null="#NOT len(trim(Arguments.company1))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.currentlocation")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.currentlocation#"
                        null="#NOT len(trim(Arguments.currentlocation))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

               <cfif isDefined("Arguments.currentlylocatedasof")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.currentlylocatedasof#"
                        null="#NOT len(trim(Arguments.currentlylocatedasof))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                 
                <cfif isDefined("Arguments.currentstatus")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.currentstatus#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.datecode")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datecode#"
                        null="#NOT len(trim(Arguments.datecode))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.datecompleted")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datecompleted#"
                        null="#NOT len(trim(Arguments.datecompleted))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinengineering")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinengineering#"
                        null="#NOT len(trim(Arguments.dateinengineering))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinmanufacturing")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinmanufacturing#"
                        null="#NOT len(trim(Arguments.dateinmanufacturing))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinqalab")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinqalab#"
                        null="#NOT len(trim(Arguments.dateinqalab))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dateinsmt")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dateinsmt#"
                        null="#NOT len(trim(Arguments.dateinsmt))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.datereceived")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datereceived#"
                        null="#NOT len(trim(Arguments.datereceived))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

               <cfif isDefined("Arguments.datesenttoqe")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.datesenttoqe#"
                        null="#NOT len(trim(Arguments.datesenttoqe))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

                
                <cfif isDefined("Arguments.daystocomplete")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.daystocomplete#"
                        null="#NOT len(trim(Arguments.closurecomments))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.description")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.description#"
                        null="#NOT len(trim(Arguments.description))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.distributormanufacturer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.distributormanufacturer#"
                        null="#NOT len(trim(Arguments.distributormanufacturer))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.dmrref")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.dmrref#"
                        null="#NOT len(trim(Arguments.dmrref))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engapprover")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engapprover#"
                        null="#NOT len(trim(Arguments.engapprover))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engineeringdatecomplete")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringdatecomplete#"
                        null="#NOT len(trim(Arguments.engineeringdatecomplete))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engineeringemail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringemail#"
                        null="#NOT len(trim(Arguments.engineeringemail))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.engineeringresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringresults#"
                        null="#NOT len(trim(Arguments.engineeringresults))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
  
                                 
                <cfif isDefined("Arguments.engqtyreceived")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engqtyreceived#"
                        null="#NOT len(trim(Arguments.engqtyreceived))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>


              
                <cfif isDefined("Arguments.failevel")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.failevel#"
                        null="#NOT len(trim(Arguments.failevel))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.faistatus")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.faistatus#"
                        null="#NOT len(trim(Arguments.faistatus))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.faitype")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.faitype#"
                        null="#NOT len(trim(Arguments.faitype))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.finalresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.finalresults#"
                        null="#NOT len(trim(Arguments.finalresults))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.inspector")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.inspector#"
                        null="#NOT len(trim(Arguments.inspector))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.itemnumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.itemnumber#"
                        null="#NOT len(trim(Arguments.itemnumber))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.lotqtyrecd")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.lotqtyrecd#"
                        null="#NOT len(trim(Arguments.lotqtyrecd))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manfpassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manfpassfail#"
                        null="#NOT len(trim(Arguments.manfpassfail))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manfresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manfresults#"
                        null="#NOT len(trim(Arguments.manfresults))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingcompletedate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingcompletedate#"
                        null="#NOT len(trim(Arguments.manufacturingcompletedate))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingnewpn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingnewpn#"
                        null="#NOT len(trim(Arguments.manufacturingnewpn))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                <!------>
                <cfif isDefined("Arguments.manufacturingnewpn2")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingnewpn2#"
                        null="#NOT len(trim(Arguments.manufacturingnewpn2))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingqtycompleted")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingqtycompleted#"
                        null="#NOT len(trim(Arguments.manufacturingqtycompleted))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.manufacturingqtyrecieved")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.manufacturingqtyrecieved#"
                        null="#NOT len(trim(Arguments.manufacturingqtyrecieved))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.needdate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.needdate#"
                        null="#NOT len(trim(Arguments.needdate))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.pilot")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.pilot#"
                        null="#NOT len(trim(Arguments.pilot))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.po")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.po#"
                        null="#NOT len(trim(Arguments.po))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.project")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.project#"
                        null="#NOT len(trim(Arguments.project))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.projectengineer")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.projectengineer#"
                        null="#NOT len(trim(Arguments.projectengineer))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qadate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qadate#"
                        null="#NOT len(trim(Arguments.qadate))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qalabdatecomplete")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qalabdatecomplete#"
                        null="#NOT len(trim(Arguments.qalabdatecomplete))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
 
                 
                <cfif isDefined("Arguments.qalabqtycompleted")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qalabqtycompleted#"
                        null="#NOT len(trim(Arguments.qalabqtycompleted))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qalabqtyreceived")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qalabqtyreceived#"
                        null="#NOT len(trim(Arguments.qalabqtyreceived))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                <!------>
                <cfif isDefined("Arguments.qatestlabnewpn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabnewpn#"
                        null="#NOT len(trim(Arguments.qatestlabnewpn))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                 
                <cfif isDefined("Arguments.qatestlabnewpn2")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabnewpn2#"
                        null="#NOT len(trim(Arguments.qatestlabnewpn2))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
               
                <cfif isDefined("Arguments.qatestlabpassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabpassfail#"
                        null="#NOT len(trim(Arguments.qatestlabpassfail))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.qatestlabreport")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.qatestlabreport#"
                        null="#NOT len(trim(Arguments.qatestlabreport))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.reasonforfai")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.reasonforfai#"
                        null="#NOT len(trim(Arguments.reasonforfai))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.revision")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.revision#"
                        null="#NOT len(trim(Arguments.revision))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.ricompletedate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.ricompletedate#"
                        null="#NOT len(trim(Arguments.ricompletedate))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.rievalutionresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.rievalutionresults#"
                        null="#NOT len(trim(Arguments.rievalutionresults))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.ripassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.ripassfail#"
                        null="#NOT len(trim(Arguments.ripassfail))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.riqtyinspected")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.riqtyinspected#"
                        null="#NOT len(trim(Arguments.riqtyinspected))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.riqtyrejected")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.riqtyrejected#"
                        null="#NOT len(trim(Arguments.riqtyrejected))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.rmonumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.rmonumber#"
                        null="#NOT len(trim(Arguments.rmonumber))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>

                <cfif isDefined("Arguments.rohscert")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.rohscert#"
                        null="#NOT len(trim(Arguments.rohscert))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
               
                <cfif isDefined("Arguments.smtcompletedate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtcompletedate#"
                        null="#NOT len(trim(Arguments.smtcompletedate))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtnewpn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtnewpn#"
                        null="#NOT len(trim(Arguments.smtnewpn))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
               <!--- ---> 
                <cfif isDefined("Arguments.smtnewpn2")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtnewpn2#"
                        null="#NOT len(trim(Arguments.smtnewpn2))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
               
                <cfif isDefined("Arguments.smtpassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtpassfail#"
                        null="#NOT len(trim(Arguments.smtpassfail))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtqty")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtqty#"
                        null="#NOT len(trim(Arguments.smtqty))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtqtycompleted")>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtqtycompleted#"
                        null="#NOT len(trim(Arguments.smtqtycompleted))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtqtyreceived")>
                   <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtqtyreceived#"
                        null="#NOT len(trim(Arguments.smtqtyreceived))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.smtresults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.smtresults#"
                        null="#NOT len(trim(Arguments.smtresults))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
               
                <cfif isDefined("Arguments.supplierreport")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.supplierreport#"
                        null="#NOT len(trim(Arguments.supplierreport))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.thapassfail")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.thapassfail#"
                        null="#NOT len(trim(Arguments.thapassfail))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.tharesults")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.tharesults#"
                        null="#NOT len(trim(Arguments.tharesults))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>




               <cfif isDefined("Arguments.reportrequiredck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.reportrequiredck#"
                         null="#NOT len(trim(Arguments.reportrequiredck))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>

             <cfif isDefined("Arguments.engineeringwock")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.engineeringwock#"
                        null="#NOT len(trim(Arguments.engineeringwock))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.previousevidck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.previousevidck#"
                         null="#NOT len(trim(Arguments.previousevidck))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.scarck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.scarck#"
                         null="#NOT len(trim(Arguments.scarck))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.limteddurationck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.limteddurationck#"
                        null="#NOT len(trim(Arguments.limteddurationck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.lotnumberonlyck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.lotnumberonlyck#"
                        null="#NOT len(trim(Arguments.lotnumberonlyck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.inspectallck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.inspectallck#"
                         null="#NOT len(trim(Arguments.inspectallck))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.inspectcriticalck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.inspectcriticalck#"
                         null="#NOT len(trim(Arguments.inspectcriticalck))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.inspectcircledck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.inspectcircledck#"
                        null="#NOT len(trim(Arguments.inspectcircledck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.oktodestroyck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.oktodestroyck#"
                        null="#NOT len(trim(Arguments.oktodestroyck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.ingorepkgrohsfitcheckck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ingorepkgrohsfitcheckck#"
                         null="#NOT len(trim(Arguments.ingorepkgrohsfitcheckck))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.ignorenotesck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ignorenotesck#"
                         null="#NOT len(trim(Arguments.ignorenotesck))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.visualck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.visualck#"
                        null="#NOT len(trim(Arguments.visualck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.sortingonlyck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.sortingonlyck#"
                        null="#NOT len(trim(Arguments.sortingonlyck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.contactuponreceiptck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.contactuponreceiptck#"
                        null="#NOT len(trim(Arguments.contactuponreceiptck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>


             <cfif isDefined("Arguments.removedtsck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.removedtsck#"
                        null="#NOT len(trim(Arguments.removedtsck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
 
             <cfif isDefined("Arguments.igorefitcheckck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.igorefitcheckck#"
                        null="#NOT len(trim(Arguments.igorefitcheckck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
               
                <cfif isDefined("Arguments.engineeringwo")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.engineeringwo#"
                         null="#NOT len(trim(Arguments.engineeringwo))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.previousevid")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.previousevid#"
                         null="#NOT len(trim(Arguments.previousevid))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.limtedduration")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.limtedduration#"
                         null="#NOT len(trim(Arguments.limtedduration))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.lotnumberonly")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.lotnumberonly#"
                         null="#NOT len(trim(Arguments.lotnumberonly))#"
                   >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
              
                <cfif isDefined("Arguments.scar")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.scar#"
                        null="#NOT len(trim(Arguments.scar))#"
                    >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>    
                                        
            	<cfif isDefined("Arguments.ignoretextureck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ignoretextureck#"
                        null="#NOT len(trim(Arguments.ignoretextureck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
            	<cfif isDefined("Arguments.ignorepartmarketingck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.ignorepartmarketingck#"
                        null="#NOT len(trim(Arguments.ignorepartmarketingck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>
                
                <cfif isDefined("Arguments.additionalinspectioninstructions")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        value="#Arguments.additionalinspectioninstructions#"
                        null="#NOT len(trim(Arguments.additionalinspectioninstructions))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="Yes">
                </cfif>
                <cfif isDefined("Arguments.rmtoengafterinspectck")>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        value="#Arguments.rmtoengafterinspectck#"
                        null="#NOT len(trim(Arguments.rmtoengafterinspectck))#"
                     >
                <cfelse>
                    <cfprocparam
                        cfsqltype="cf_sql_bit"
                        type="In"
                        null="Yes">
                </cfif>              
               <cfprocparam
                    cfsqltype="cf_sql_integer"
                    type="Out" 
                    value=0
                    variable="idenity"
                    >

               <cfprocparam
                    cfsqltype="CF_SQL_VARCHAR"
                    type="Out" 
                    value=""
                    variable="sqlRequest"
                    >

            	<cfprocresult name="qry_getSelect">     
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
         <cfscript>
		 	returnResult = StructNew();
			returnResult['results'] = results;
			returnResult['idenity'] = idenity;
			returnResult['sqlRequest'] = sqlRequest;
		</cfscript>  
           
        <cfreturn returnResult />
	</cffunction>
                
</cfcomponent>