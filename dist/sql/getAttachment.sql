USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[getAttachment]    Script Date: 11/22/2016 8:49:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[getAttachment](
	@guid nvarchar(150)
)AS

SELECT [key]
      ,[id]
      ,[fileName]
      ,[prefix]
      ,[file]
      ,[guid]
      ,[createdOn]
      ,[isActive]
 FROM [db_fai].[dbo].[fai_Attachments]
 WHERE guid = @guid-- AND isActive = 1