USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[addNewAttachment]    Script Date: 11/22/2016 8:40:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[addNewAttachment](
	@id int,
	@fileName NVARCHAR(250),
	@prefix  NVARCHAR(10),
	@file varbinary(MAX)
)AS

insert into [db_fai].[dbo].[fai_Attachments]
(
	 id
	,fileName
	,prefix 
	,[file]
)

VALUES
(
	 @id
	,@fileName
	,@prefix
	,@file
)
