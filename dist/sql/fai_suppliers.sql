USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_suppliers]    Script Date: 11/22/2016 8:48:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_suppliers]
AS
SELECT'supplier' AS [key], UPPER(SupplierName) AS [value] 
FROM dbo.suppliers
ORDER BY SupplierName
