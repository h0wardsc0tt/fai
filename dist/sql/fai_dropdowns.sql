USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_dropdowns]    Script Date: 11/22/2016 8:44:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_dropdowns]
AS
DECLARE @dropdowns AS TABLE([key] NVARCHAR(100), [value] NVARCHAR(255))
INSERT INTO @dropdowns
SELECT distinct 'company' AS [key], UPPER([company 1]) AS value
FROM tbl_FAI
WHERE [company 1] IS NOT NULL AND [company 1] != ''  AND [company 1] != 'VOID'
ORDER BY value

INSERT INTO @dropdowns
SELECT distinct 'approvedby' AS [key], UPPER([APPROVED BY]) AS value
FROM tbl_FAI
WHERE [APPROVED BY] IS NOT NULL AND [APPROVED BY] != ''  AND [APPROVED BY] != 'VOID'
ORDER BY value

SELECT [key],[value] from @dropdowns
GROUP BY [key],[value]
ORDER BY [key], value
