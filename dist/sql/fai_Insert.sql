USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_Insert]    Script Date: 12/1/2016 7:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[fai_Insert](
	@id INT,
	@approvedby NVARCHAR(255) = NULL,
	@buyer NVARCHAR(255) = NULL,
	@closurecomments NVARCHAR(MAX) = NULL,
	@comments NVARCHAR(255) = NULL,
	@company1 NVARCHAR(255) = NULL,
	@currentlocation NVARCHAR(255) = NULL,
	@currentlylocatedasof NVARCHAR(25) = NULL,
	@currentstatus NVARCHAR(255) = NULL,
	@datecode NVARCHAR(255) = NULL,
	@datecompleted NVARCHAR(25) = NULL,
	@dateinengineering NVARCHAR(25) = NULL,
	@dateinmanufacturing NVARCHAR(25) = NULL,
	@dateinqalab NVARCHAR(25) = NULL,
	@dateinsmt NVARCHAR(25) = NULL,
	@datereceived NVARCHAR(25) = NULL,
	@daystocomplete NVARCHAR(15) = NULL,
	@description NVARCHAR(255) = NULL,
	@distributormanufacturer NVARCHAR(255) = NULL,
	@dmrref NVARCHAR(15) = NULL,
	@engapprover NVARCHAR(255) = NULL,
	@engineeringdatecomplete NVARCHAR(25) = NULL,
	@engineeringemail NVARCHAR(255) = NULL,
	@engineeringresults NVARCHAR(255) = NULL,
	@failevel NVARCHAR(255) = NULL,
	@faistatus NVARCHAR(255) = NULL,
	@faitype NVARCHAR(255) = NULL,
	@finalresults NVARCHAR(255) = NULL,
	@inspector NVARCHAR(255) = NULL,
	@itemnumber NVARCHAR(255) = NULL,
	@lotqtyrecd NVARCHAR(15) = NULL,
	@manfpassfail NVARCHAR(255) = NULL,
	@manfresults NVARCHAR(255) = NULL,
	@manufacturingcompletedate NVARCHAR(25) = NULL,
	@manufacturingnewpn NVARCHAR(255) = NULL,
	@manufacturingqtycompleted NVARCHAR(255) = NULL,
	@manufacturingqtyrecieved NVARCHAR(255) = NULL,
	@needdate NVARCHAR(25) = NULL,
	@pilot BIT = NULL,
	@po NVARCHAR(255) = NULL,
	@project NVARCHAR(255) = NULL,
	@projectengineer NVARCHAR(MAX) = NULL,
	@qadate NVARCHAR(25) = NULL,
	@qalabdatecomplete NVARCHAR(255) = NULL,
	@qalabqtycompleted NVARCHAR(255) = NULL,
	@qalabqtyreceived NVARCHAR(255) = NULL,
	@qatestlabpassfail NVARCHAR(255) = NULL,
	@qatestlabreport NVARCHAR(255) = NULL,
	@reasonforfai NVARCHAR(255) = NULL,
	@revision NVARCHAR(255) = NULL,
	@ricompletedate NVARCHAR(25) = NULL,
	@rievalutionresults NVARCHAR(MAX) = NULL,
	@ripassfail NVARCHAR(255) = NULL,
	@riqtyinspected NVARCHAR(15) = NULL,
	@riqtyrejected NVARCHAR(15) = NULL,
	@rmonumber NVARCHAR(15) = NULL,
	@smtcompletedate NVARCHAR(255) = NULL,
	@smtnewpn NVARCHAR(255) = NULL,
	@smtpassfail NVARCHAR(255) = NULL,
	@smtqty NVARCHAR(15) = NULL,
	@smtqtycompleted NVARCHAR(15) = NULL,
	@smtresults NVARCHAR(255) = NULL,
	@supplierreport BIT = NULL,
	@thapassfail NVARCHAR(255) = NULL,
	@tharesults NVARCHAR(255) = NULL,
	@idenity INT OUTPUT,
	@sqlRequest NVARCHAR(15) OUTPUT
)AS
SET @idenity =  @id
SET @sqlRequest = 'insert'

insert into [dbo].[tbl_FAI]
(
	 [approved by]
	,[buyer]
	,[closure comments]
	,[comments]
	,[company 1]
	,[current  location]
	,[currently located as of]
	,[current status]
	,[date code]
	,[date completed]
	,[date in engineering]
	,[date in manufacturing]
	,[date in qa lab]
	,[date in smt]
	,[date received]
	,[days to complete]
	,[description]
	,[distributor/manufacturer]
	,[dmr ref #]
	,[eng approver]
	,[engineering date  complete]
	,[engineering email]
	,[engineering results]
	,[fai level]
	,[fai status]
	,[fai type]
	,[final results]
	,[inspector]
	,[item number]
	,[lot qty recd]
	,[manf pass / fail]
	,[manf results]
	,[manufacturing complete date]
	,[manufacturing new pn]
	,[manufacturing qty completed]
	,[manufacturing qty recieved]
	,[need date]
	,[pilot?]
	,[po #]
	,[project]
	,[project engineer]
	,[qa  date]
	,[qa lab date complete]
	,[qa lab qty completed]
	,[qa lab qty received]
	,[qa test lab pass / fail]
	,[qa test lab  report #]
	,[reason for fai]
	,[revision]
	,[ri complete date]
	,[ri evalution  - results]
	,[ri pass / fail]
	,[ri qty inspected]
	,[ri qty rejected]
	,[rmo number]
	,[smt complete date]
	,[smt new pn]
	,[smt pass / fail]
	,[smt qty]
	,[smt qty completed]
	,[smt results]
	,[supplierreport]
	,[tha pass]
	,[tha results]
)

VALUES
(
	 @approvedby
	,@buyer
	,@closurecomments
	,@comments
	,@company1
	,@currentlocation
	--,@currentlylocatedasof
	,CASE WHEN ISDATE(@currentlylocatedasof) = 1 THEN @currentlylocatedasof ELSE NULL END
	,@currentstatus
	,@datecode
	--,@datecompleted
	,CASE WHEN ISDATE(@datecompleted) = 1 THEN @datecompleted ELSE NULL END
	--,@dateinengineering
	,CASE WHEN ISDATE(@dateinengineering) = 1 THEN @dateinengineering ELSE NULL END
	--,@dateinmanufacturing
	,CASE WHEN ISDATE(@dateinmanufacturing) = 1 THEN @dateinmanufacturing ELSE NULL END
	--,@dateinqalab
	,CASE WHEN ISDATE(@dateinqalab) = 1 THEN @dateinqalab ELSE NULL END
	--,@dateinsmt
	,CASE WHEN ISDATE(@dateinsmt) = 1 THEN @dateinsmt ELSE NULL END
	--,@datereceived
	,CASE WHEN ISDATE(@datereceived) = 1 THEN @datereceived ELSE NULL END
	,@daystocomplete
	,@description
	,@distributormanufacturer
	,@dmrref
	,@engapprover
	--,@engineeringdatecomplete
	,CASE WHEN ISDATE(@engineeringdatecomplete) = 1 THEN @engineeringdatecomplete ELSE NULL END
	,@engineeringemail
	,@engineeringresults
	,@failevel
	,@faistatus
	,@faitype
	,@finalresults
	,@inspector
	,@itemnumber
	,@lotqtyrecd
	,@manfpassfail
	,@manfresults
	--,@manufacturingcompletedate
	,CASE WHEN ISDATE(@manufacturingcompletedate) = 1 THEN @manufacturingcompletedate ELSE NULL END
	,@manufacturingnewpn
	,@manufacturingqtycompleted
	,@manufacturingqtyrecieved
	--,@needdate
	,CASE WHEN ISDATE(@needdate) = 1 THEN @needdate ELSE NULL END
	,@pilot
	,@po
	,@project
	,@projectengineer
	,@qadate
	--,@qalabdatecomplete
	,CASE WHEN ISDATE(@qalabdatecomplete) = 1 THEN @qalabdatecomplete ELSE NULL END
	,@qalabqtycompleted
	,@qalabqtyreceived
	,@qatestlabpassfail
	,@qatestlabreport
	,@reasonforfai
	,@revision
	--,@ricompletedate
	,CASE WHEN ISDATE(@ricompletedate) = 1 THEN @ricompletedate ELSE NULL END
	,@rievalutionresults
	,@ripassfail
	,@riqtyinspected
	,@riqtyrejected
	,@rmonumber
	--,@smtcompletedate
	,CASE WHEN ISDATE(@ricompletedate) = 1 THEN @ricompletedate ELSE NULL END
	,@smtnewpn
	,@smtpassfail
	,@smtqty
	,@smtqtycompleted
	,@smtresults
	,@supplierreport
	,@thapassfail
	,@tharesults
)
SET @idenity =  @@IDENTITY

