USE [db_fai]
GO
/****** Object:  UserDefinedFunction [dbo].[fai_where]    Script Date: 11/22/2016 8:54:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fai_where](@params dbo.fai_parameters READONLY)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE 
	@id INT,
	@company1 NVARCHAR(255),
	@description NVARCHAR(255),
	@distributormanufacturer NVARCHAR(255),
	@inspector NVARCHAR(255),
	@itemnumber NVARCHAR(255),
	@needdate_from NVARCHAR(50),
	@needdate_to NVARCHAR(50),
	@po NVARCHAR(255),
	@project NVARCHAR(255),
	@rmonumber NVARCHAR(10),
	@custom NVARCHAR(2000),
	@currentDate DateTime,
	@where NVARCHAR(MAX),
	@finalresults NVARCHAR(255),
	@ricompletedate_from NVARCHAR(50),
	@ricompletedateate_to NVARCHAR(50),
	@revision NVARCHAR(255),
	@datecompleted_from NVARCHAR(50),
	@datecompleted_to NVARCHAR(50)

	SET @where = ''
	SET @currentDate = GETDATE()

	SELECT 	@id = id,
		@company1 = company1,
		@description = description,
		@distributormanufacturer = distributormanufacturer,
		@inspector = inspector,
		@itemnumber = itemnumber,
		@needdate_from = needdate_from,
		@needdate_to = needdate_to,
		@po = po,
		@project = project,
		@ricompletedate_from = ricompletedate_from,
		@ricompletedateate_to = ricompletedateate_to,
		@rmonumber = rmonumber,
		@custom = custom,
		@datecompleted_to = datecompleted_to,
		@datecompleted_from = datecompleted_from,
		@finalresults = finalresults,
		@revision = revision
	FROM @params

	IF @custom IS NOT NULL
		BEGIN
			SET @where = @where + ' AND ' + @custom
		END
	IF @description IS NOT NULL
		BEGIN
			SET @where = @where + ' AND description LIKE ' + '''%' + @description + '%'''
		END
	IF @distributormanufacturer IS NOT NULL
		BEGIN
			SET @where = @where + ' AND [DISTRIBUTOR/MANUFACTURER] LIKE ' + '''%' + @distributormanufacturer + '%'''
		END
	IF @inspector IS NOT NULL
		BEGIN
			SET @where = @where + ' AND inspector LIKE ' + '''%' + @inspector + '%'''
		END
	IF @itemnumber IS NOT NULL
		BEGIN
			SET @where = @where + ' AND [ITEM NUMBER] LIKE ' + '''%' + @itemnumber + '%'''
		END
	IF @finalresults IS NOT NULL
		BEGIN
			SET @where = @where + ' AND [FINAL RESULTS] LIKE ' + '''%' + @finalresults + '%'''
		END
	IF @revision IS NOT NULL
		BEGIN
			SET @where = @where + ' AND [REVISION] LIKE ' + '''%' + @revision + '%'''
		END


	IF @datecompleted_from  IS NOT NULL AND @datecompleted_to  IS NOT NULL
		BEGIN
			SET @where = @where + ' AND CAST([DATE COMPLETED] AS DATE) >=  ''' + @datecompleted_from  + '''  AND CAST([DATE COMPLETED] AS DATE) <=  ''' + @datecompleted_to  + ''''
		END
	IF @datecompleted_from  IS NOT NULL AND @datecompleted_to  IS NULL
		BEGIN
			SET @where = @where + ' AND CAST([DATE COMPLETED] AS DATE) >= ''' + @datecompleted_from  + ''''
		END
	IF @datecompleted_from  IS NULL AND @datecompleted_to  IS NOT NULL
		BEGIN
			SET @where = @where + ' AND CAST([DATE COMPLETED] AS DATE) <=  ''' + @datecompleted_to  + ''''
		END



	IF @needdate_from  IS NOT NULL AND @needdate_to  IS NOT NULL
		BEGIN
			SET @where = @where + ' AND CAST([NEED DATE] AS DATE) >=  ''' + @needdate_from  + '''  AND CAST([NEED DATE] AS DATE) <=  ''' + @needdate_to  + ''''
		END
	IF @needdate_from  IS NOT NULL AND @needdate_to  IS NULL
		BEGIN
			SET @where = @where + ' AND CAST([NEED DATE] AS DATE) >= ''' + @needdate_from  + ''''
		END
	IF @needdate_from  IS NULL AND @needdate_to  IS NOT NULL
		BEGIN
			SET @where = @where + ' AND CAST([NEED DATE] AS DATE) <=  ''' + @needdate_to  + ''''
		END

	IF @ricompletedate_from  IS NOT NULL AND @ricompletedateate_to  IS NOT NULL
		BEGIN
			SET @where = @where + ' AND CAST([RI COMPLETE DATE] AS DATE) >=  ''' + @ricompletedate_from  + '''  AND CAST([RI COMPLETE DATE] AS DATE) <=  ''' + @ricompletedateate_to  + ''''
		END
	IF @ricompletedate_from  IS NOT NULL AND @ricompletedateate_to  IS NULL
		BEGIN
			SET @where = @where + ' AND CAST([RI COMPLETE DATE] AS DATE) >= ''' + @ricompletedate_from  + ''''
		END
	IF @ricompletedate_from  IS NULL AND @ricompletedateate_to  IS NOT NULL
		BEGIN
			SET @where = @where + ' AND CAST([RI COMPLETE DATE] AS DATE) <=  ''' + @ricompletedateate_to  + ''''
		END

	RETURN  @where
END
