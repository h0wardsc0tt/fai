USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_changeAttachmentName]    Script Date: 11/22/2016 8:42:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_changeAttachmentName](
	@filename NVARCHAR(250),
	@guid NVARCHAR(50)
)AS
UPDATE dbo.fai_Attachments SET filename = @filename
WHERE guid = @guid