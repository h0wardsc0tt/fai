USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_ewip]    Script Date: 11/22/2016 8:45:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_ewip](
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL
) AS



DECLARE
	@select  NVARCHAR(4000),
	@from	 NVARCHAR(50),
	@where	 NVARCHAR(4000),
	@sql	 NVARCHAR(4000),
	@orderBy NVARCHAR(200),
	@groupBy NVARCHAR(150),
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@idNotIn  NVARCHAR(100),
	@cnt INT,
	@count INT,
	@recordSet INT,
	@excelSelect NVARCHAR(100),
	@notIn NVARCHAR(100),
	@currentDate DateTime,
	@culture nvarchar(10)

SET @culture = 'en-US'
SET @pageFetch = ''
SET @currentDate = GETDATE()

SET @excelSelect = NULL
SET @notIn = NULL

IF @notIn IS NOT NULL
	BEGIN
		SET @idNotIn = ' AND id NOT IN (' + @notIn + ') '	
	END
ELSE
	BEGIN
		SET @idNotIn = ''	
	END

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL AND @excelSelect IS NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' + CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END

SELECT @cnt = count(*) 
FROM tbl_fai
WHERE [FAI STATUS]='In Work' AND [DATE COMPLETED] Is Null AND [DATE IN ENGINEERING] Is Not Null AND [CLOSURE COMMENTS] Is Null

SELECT 
	@cnt as count,
	[APPROVED BY] AS approvedby,
	'[' + STUFF((SELECT  ',' + '{"guid":"'+ cast(guid as varchar(36)) + '","filename":"' + filename + '","isactive":"' + cast(isActive as varchar(1)) + '"}'
			FROM [db_fai].[dbo].[fai_Attachments]
			WHERE  id = fai.id
			FOR XML PATH('')), 1, 1, '') + ']' AS attachments,
	[BUYER] AS buyer,
	[CLOSURE COMMENTS] AS closurecomments,
	[COMMENTS] AS comments,
	[COMPANY 1] AS company1,
	[CURRENT  LOCATION] AS currentlocation,
	[CURRENT STATUS] AS currentstatus,
	[CURRENTLY LOCATED AS OF] AS currentlylocatedasof,
	[DATE CODE] AS datecode,
	FORMAT([DATE IN ENGINEERING],'d', @culture) AS dateinengineering,
	FORMAT([DATE IN QA LAB],'d', @culture) AS dateinqalab,
	FORMAT([DATE COMPLETED],'d', @culture) AS datecompleted,
	FORMAT([DATE IN MANUFACTURING],'d', @culture) AS dateinmanufacturing,
	FORMAT([DATE IN SMT],'d', @culture) AS dateinsmt,
	FORMAT([DATE RECEIVED],'d', @culture) AS datereceived,
	[DAYS TO COMPLETE] AS daystocomplete,
	[DESCRIPTION] AS description,
	[DISTRIBUTOR/MANUFACTURER] AS distributormanufacturer,
	[DMR REF #] AS dmrref,
	[ENG APPROVER] AS engapprover,
	FORMAT([ENGINEERING DATE  COMPLETE],'d', @culture) AS engineeringdatecomplete,
	[ENGINEERING EMAIL] AS engineeringemail,
	[ENGINEERING RESULTS] AS engineeringresults,
	[FAI LEVEL] AS failevel,
	[FAI STATUS] AS faistatus,
	[FAI TYPE] AS faitype,
	[FINAL RESULTS] AS finalresults,
	[ID] AS id,
	[INSPECTOR] AS inspector,
	[ITEM NUMBER] AS itemnumber,
	[LOT QTY RECD] AS lotqtyrecd,
	[MANF PASS / FAIL] AS manfpassfail,
	[MANF RESULTS] AS manfresults,
	FORMAT([MANUFACTURING COMPLETE DATE],'d', @culture) AS manufacturingcompletedate,
	[MANUFACTURING NEW PN] AS manufacturingnewpn,
	[MANUFACTURING QTY COMPLETED] AS manufacturingqtycompleted,
	[MANUFACTURING QTY RECIEVED] AS manufacturingqtyrecieved,
	FORMAT([NEED DATE],'d', @culture) AS needdate,
	[PILOT?] AS pilot,
	[PO #] AS po,
	[PROJECT] AS project,
	[PROJECT ENGINEER] AS projectengineer,
	FORMAT([QA  DATE],'d', @culture) AS qadate,
	FORMAT([QA LAB DATE COMPLETE],'d', @culture) AS qalabdatecomplete,
	[qa lab qty completed] AS qalabqtycompleted,
	[qa lab qty received] AS qalabqtyreceived,
	[QA TEST LAB  REPORT #] AS qatestlabreport,
	[QA TEST LAB PASS / FAIL] AS qatestlabpassfail,
	[REASON FOR FAI] AS reasonforfai,
	[REVISION] AS revision,
	LTRIM([RI EVALUTION  - RESULTS]) AS rievalutionresults,
	[RI PASS / FAIL] AS ripassfail,
	[RI QTY INSPECTED] AS riqtyinspected,
	[RI QTY REJECTED] AS riqtyrejected,
	FORMAT([RI COMPLETE DATE],'d', @culture) AS ricompletedate,
	[RMO NUMBER] AS rmonumber,
	[SMT PASS / FAIL] AS smtpassfail,
	[SMT RESULTS] AS smtresults,
	FORMAT([SMT COMPLETE DATE],'d', @culture) AS smtcompletedate,
	[SMT NEW PN] AS smtnewpn,
	[SMT QTY] AS smtqty,
	[SMT QTY COMPLETED] AS smtqtycompleted,
	[SupplierReport]  AS supplierreport,
	[THA RESULTS] AS tharesults,
	[THA PASS]  AS thapassfail
FROM [db_fai].[dbo].[tbl_FAI] fai
WHERE [FAI STATUS]='In Work' AND [DATE COMPLETED] Is Null AND [DATE IN ENGINEERING] Is Not Null AND [CLOSURE COMMENTS] Is Null
ORDER BY (getdate()-[DATE RECEIVED]) desc
OFFSET @recordSet ROWS FETCH NEXT @itemsPerPage ROWS ONLY


