USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[getActiveAttachmentList]    Script Date: 11/22/2016 8:49:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[getActiveAttachmentList](
	@id INT
)AS

SELECT (SELECT itbl.filename+';'
            FROM fai_Attachments itbl
            WHERE itbl.id=tbl.id
            FOR XML PATH('')) attachments
FROM fai_Attachments tbl
WHERE tbl.id = @id AND tbl.isActive = 1
GROUP BY tbl.id