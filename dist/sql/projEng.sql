USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_projEng]    Script Date: 11/2/2016 10:29:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_projEng]
AS
SELECT distinct 'projectengineer' AS [key], UPPER(item) AS value
FROM tbl_FAI
CROSS APPLY dbo.SplitStrings([Project Engineer], ';')
WHERE [Project Engineer] IS NOT NULL AND [Project Engineer] != ''