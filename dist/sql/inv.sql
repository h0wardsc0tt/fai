USE [db_fai]
GO
/****** Object:  UserDefinedFunction [dbo].[inv]    Script Date: 11/22/2016 8:54:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[inv](@val NVARCHAR(2000))
RETURNS NVARCHAR(2000)
AS
BEGIN
	return ISNULL(@val, '')
END
