USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[Save_ARCHIVEqryRIInspectionComplete]    Script Date: 11/22/2016 8:51:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Save_ARCHIVEqryRIInspectionComplete]
AS

DECLARE @currentDate DateTime
SET @currentDate = GETDATE()

SELECT 
	[COMPANY 1],
	ID,
	INSPECTOR,
	[ITEM NUMBER],
	[DESCRIPTION],
	REVISION,
	[DISTRIBUTOR/MANUFACTURER],
	[FAI TYPE],
	[Need Date],
	[DATE RECEIVED],
	[RI COMPLETE DATE],
	[REASON FOR FAI],
	[FAI STATUS],
	[FAI LEVEL],
	DATEDIFF(DAY, [DATE RECEIVED], @currentDate) AS [Total Days In Work]
FROM [db_fai].[dbo].[tbl_FAI]
WHERE ((([RI COMPLETE DATE]) Is Null) AND (([FAI STATUS])='IN WORK'))
ORDER BY @currentDate-[DATE RECEIVED] DESC;