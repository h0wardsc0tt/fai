USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_select]    Script Date: 10/31/2016 10:09:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_test](
	@id INT = NULL,
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL,
	@s_company1 NVARCHAR(255) = NULL,
	@sortColumn NVARCHAR(50) = NULL,
	@sortOrder NVARCHAR(5) = NULL,
	@needdate_from NVARCHAR(50) = NULL,
	@needdate_to NVARCHAR(50) = NULL,
	@s_description NVARCHAR(255) = NULL,
	@s_distributormanufacturer NVARCHAR(255) = NULL,
	@s_inspector NVARCHAR(255) = NULL,
	@s_itemnumber NVARCHAR(255) = NULL,
	@s_po NVARCHAR(255) = NULL,
	@s_project NVARCHAR(255) = NULL,
	@s_ricompletedate_from NVARCHAR(50) = NULL,
	@s_ricompletedateate_to NVARCHAR(50) = NULL,
	@s_rmonumber NVARCHAR(10) = NULL
 )AS


DECLARE @culture nvarchar(10)
SET @culture = 'en-US'

DECLARE
	@cnt INT,
	@recordSet INT

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
	END

SELECT @cnt = count(*) FROM [db_fai].[dbo].[tbl_FAI]
WHERE 1=1 
	AND (@id IS NULL OR [ID] = @id)
	AND (@s_company1 IS NULL OR [COMPANY 1] LIKE '%' + @s_company1 + '%')
	AND (@s_description IS NULL OR description LIKE '%' + @s_description + '%''')
	AND (@s_distributormanufacturer IS NULL OR [DISTRIBUTOR/MANUFACTURER] LIKE '%' + @s_distributormanufacturer + '%')
	AND (@s_inspector IS NULL OR inspector LIKE '%' + @s_inspector + '%')
	AND (@s_itemnumber IS NULL OR [ITEM NUMBER] LIKE '%' + @s_itemnumber + '%')
	AND (@s_po IS NULL OR [PO #] LIKE '%' + @s_po + '%')
	AND (@s_project IS NULL OR project LIKE '%' + @s_project + '%')
	AND ((@needdate_from IS NOT NULL AND @needdate_to IS NOT NULL) OR CAST([NEED DATE] AS DATE) >=  @needdate_from  AND CAST([NEED DATE] AS DATE) <=  @needdate_to)
	AND ((@needdate_from IS NOT NULL AND @needdate_to IS NULL) OR CAST([NEED DATE] AS DATE) >= @needdate_from)
	AND ((@needdate_from IS NULL AND @needdate_to IS NOT NULL) OR CAST([NEED DATE] AS DATE) <=  @needdate_to)
	AND (@s_rmonumber IS NULL OR  CAST([RMO NUMBER] AS varchar(20)) LIKE '%' + @s_rmonumber + '%')
	--AND ((@s_ricompletedate_from IS NOT NULL AND @s_ricompletedateate_to IS NOT NULL) OR CAST([RI COMPLETE DATE] AS DATE) >= @s_ricompletedate_from  AND CAST([RI COMPLETE DATE] AS DATE) <=  @s_ricompletedateate_to)
	--AND ((@s_ricompletedate_from IS NOT NULL AND @s_ricompletedateate_to IS NULL) OR CAST([RI COMPLETE DATE] AS DATE) >= @s_ricompletedate_from)	
	--AND ((@s_ricompletedate_from IS NULL AND @s_ricompletedateate_to IS NOT NULL) OR CAST([RI COMPLETE DATE] AS DATE) <= @s_ricompletedateate_to)

SELECT @cnt AS count,
	[APPROVED BY] AS approvedby,
	'[' + STUFF((SELECT  ',' + '{"guid":"'+ cast(guid as varchar(36)) + '","filename":"' + filename + '","isactive":"' + cast(isActive as varchar(1)) + '"}'
			FROM [db_fai].[dbo].[fai_Attachments]
			WHERE  id = fai.id
			FOR XML PATH('')), 1, 1, '') + ']' AS attachments,
	[BUYER] AS buyer,
	[CLOSURE COMMENTS] AS closurecomments,
	[COMMENTS] AS comments,
	[COMPANY 1] AS company1,
	[CURRENT  LOCATION] AS currentlocation,
	[CURRENT STATUS] AS currentstatus,
	[CURRENTLY LOCATED AS OF] AS currentlylocatedasof,
	[DATE CODE] AS datecode,
	FORMAT([DATE IN ENGINEERING],'d', @culture) AS dateinengineering,
	FORMAT([DATE IN QA LAB],'d', @culture) AS dateinqalab,
	FORMAT([DATE COMPLETED],'d', @culture) AS datecompleted,
	FORMAT([DATE IN MANUFACTURING],'d', @culture) AS dateinmanufacturing,
	FORMAT([DATE IN SMT],'d', @culture) AS dateinsmt,
	FORMAT([DATE RECEIVED],'d', @culture) AS datereceived,
	[DAYS TO COMPLETE] AS daystocomplete,
	[DESCRIPTION] AS description,
	[DISTRIBUTOR/MANUFACTURER] AS distributormanufacturer,
	[DMR REF #] AS dmrref,
	[ENG APPROVER] AS engapprover,
	FORMAT([ENGINEERING DATE  COMPLETE],'d', @culture) AS engineeringdatecomplete,
	[ENGINEERING EMAIL] AS engineeringemail,
	[ENGINEERING RESULTS] AS engineeringresults,
	[FAI LEVEL] AS failevel,
	[FAI STATUS] AS faistatus,
	[FAI TYPE] AS faitype,
	[FINAL RESULTS] AS finalresults,
	[ID] AS id,
	[INSPECTOR] AS inspector,
	[ITEM NUMBER] AS itemnumber,
	[LOT QTY RECD] AS lotqtyrecd,
	[MANF PASS / FAIL] AS manfpassfail,
	[MANF RESULTS] AS manfresults,
	FORMAT([MANUFACTURING COMPLETE DATE],'d', @culture) AS manufacturingcompletedate,
	[MANUFACTURING NEW PN] AS manufacturingnewpn,
	[MANUFACTURING QTY COMPLETED] AS manufacturingqtycompleted,
	[MANUFACTURING QTY RECIEVED] AS manufacturingqtyrecieved,
	FORMAT([NEED DATE],'d', @culture) AS needdate,
	[PILOT?] AS pilot,
	[PO #] AS po,
	[PROJECT] AS project,
	[PROJECT ENGINEER] AS projectengineer,
	FORMAT([QA  DATE],'d', @culture) AS qadate,
	FORMAT([QA LAB DATE COMPLETE],'d', @culture) AS qalabdatecomplete,
	[QA TEST LAB  REPORT #] AS qatestlabreport,
	[QA TEST LAB PASS / FAIL] AS qatestlabpassfail,
	[REASON FOR FAI] AS reasonforfai,
	[REVISION] AS revision,
	LTRIM([RI EVALUTION  - RESULTS]) AS rievalutionresults,
	[RI PASS / FAIL] AS ripassfail,
	[RI QTY INSPECTED] AS riqtyinspected,
	[RI QTY REJECTED] AS riqtyrejected,
	FORMAT([RI COMPLETE DATE],'d', @culture) AS ricompletedate,
	[RMO NUMBER] AS rmonumber,
	[SMT PASS / FAIL] AS smtpassfail,
	[SMT RESULTS] AS smtresults,
	FORMAT([SMT COMPLETE DATE],'d', @culture) AS smtcompletedate,
	[SMT NEW PN] AS smtnewpn,
	[SMT QTY] AS smtqty,
	[SMT QTY COMPLETED] AS smtqtycompleted,
	[THA RESULTS] AS tharesults,
	[THA PASS]  AS thapassfail
FROM [db_fai].[dbo].[tbl_FAI] fai
WHERE 1=1 
	AND (@id IS NULL OR [ID] = @id)
	AND (@s_company1 IS NULL OR [COMPANY 1] LIKE '%' + @s_company1 + '%')
	AND (@s_description IS NULL OR description LIKE '%' + @s_description + '%''')
	AND (@s_distributormanufacturer IS NULL OR [DISTRIBUTOR/MANUFACTURER] LIKE '%' + @s_distributormanufacturer + '%')
	AND (@s_inspector IS NULL OR inspector LIKE '%' + @s_inspector + '%')
	AND (@s_itemnumber IS NULL OR [ITEM NUMBER] LIKE '%' + @s_itemnumber + '%')
	AND (@s_po IS NULL OR [PO #] LIKE '%' + @s_po + '%')
	AND (@s_project IS NULL OR project LIKE '%' + @s_project + '%')
	--AND ((@s_needdate_from IS NOT NULL AND @s_needdate_to IS NOT NULL) OR CAST([NEED DATE] AS DATE) >=  @s_needdate_from  AND CAST([NEED DATE] AS DATE) <=  @s_needdate_to)
	--AND ((@s_needdate_from IS NOT NULL AND @s_needdate_to IS NULL) OR CAST([NEED DATE] AS DATE) >= @s_needdate_from)
	--AND ((@s_needdate_from IS NULL AND @s_needdate_to IS NOT NULL) OR CAST([NEED DATE] AS DATE) <=  @s_needdate_to)
	AND (@s_rmonumber IS NULL OR  CAST([RMO NUMBER] AS varchar(20)) LIKE '%' + @s_rmonumber + '%')
	--AND ((@s_ricompletedate_from IS NOT NULL AND @s_ricompletedateate_to IS NOT NULL) OR CAST([RI COMPLETE DATE] AS DATE) >= @s_ricompletedate_from  AND CAST([RI COMPLETE DATE] AS DATE) <=  @s_ricompletedateate_to)
	--AND ((@s_ricompletedate_from IS NOT NULL AND @s_ricompletedateate_to IS NULL) OR CAST([RI COMPLETE DATE] AS DATE) >= @s_ricompletedate_from)	
	--AND ((@s_ricompletedate_from IS NULL AND @s_ricompletedateate_to IS NOT NULL) OR CAST([RI COMPLETE DATE] AS DATE) <= @s_ricompletedateate_to)

ORDER BY 
      CASE WHEN @sortOrder = 'asc' THEN
		CASE 
			WHEN @sortColumn IS NOT NULL THEN  @sortColumn
			--ELSE [NEED DATE]
		END 
      END ASC,
      CASE WHEN @sortOrder = 'desc' THEN
 		CASE 
			WHEN @sortColumn IS NOT NULL THEN  @sortColumn
			--ELSE [NEED DATE]
		END 
      END DESC 
OFFSET @recordSet ROWS FETCH NEXT @itemsPerPage ROWS ONLY
