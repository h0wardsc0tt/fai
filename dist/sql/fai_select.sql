USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_select]    Script Date: 11/22/2016 8:48:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_select](
	@id INT = NULL,
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL,
	@s_description NVARCHAR(255) = NULL,
	@s_distributormanufacturer NVARCHAR(255) = NULL,
	@s_inspector NVARCHAR(255) = NULL,
	@s_itemnumber NVARCHAR(255) = NULL,
	@s_needdate_from NVARCHAR(50) = NULL,
	@s_needdate_to NVARCHAR(50) = NULL,
	@s_datecompleted_from NVARCHAR(50) = NULL,
	@s_datecompleted_to NVARCHAR(50) = NULL,
	@s_finalresults NVARCHAR(255) = NULL,
	@s_ricompletedate_from NVARCHAR(50) = NULL,
	@s_ricompletedateate_to NVARCHAR(50) = NULL,
	@s_revision NVARCHAR(255) = NULL,
	@sortColumn NVARCHAR(50) = NULL,
	@sortOrder NVARCHAR(5) = NULL,
	@custom NVARCHAR(2000) = NULL
) AS


DECLARE @params dbo.fai_parameters;
INSERT INTO @params VALUES(
	@id,
	@pageNumber,
	@itemsPerPage,
	NULL,--company1
	@s_description,
	@s_distributormanufacturer,
	@s_inspector,
	@s_itemnumber,
	@s_needdate_from,
	@s_needdate_to,
	NULL,--po
	NULL,--project
	@sortColumn,
	@sortOrder,
	@s_ricompletedate_from,
	@s_ricompletedateate_to,
	NULL,
	@custom,
	@s_finalresults,
	@s_revision,
	@s_datecompleted_from,
	@s_datecompleted_to

)

DECLARE
	@select  NVARCHAR(4000),
	@from	 NVARCHAR(50),
	@where	 NVARCHAR(4000),
	@sql	 NVARCHAR(4000),
	@orderBy NVARCHAR(200),
	@groupBy NVARCHAR(150),
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@idNotIn  NVARCHAR(100),
	@cnt INT,
	@count INT,
	@recordSet INT,
	@excelSelect NVARCHAR(100),
	@notIn NVARCHAR(100),
	@currentDate DateTime,
	@culture nvarchar(10)

SET @culture = 'en-US'
SET @pageFetch = ''
SET @currentDate = GETDATE()

SET @excelSelect = NULL
SET @notIn = NULL

IF @notIn IS NOT NULL
	BEGIN
		SET @idNotIn = ' AND id NOT IN (' + @notIn + ') '	
	END
ELSE
	BEGIN
		SET @idNotIn = ''	
	END

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL AND @excelSelect IS NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' + CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END

	print @pageFetch

	--SET @id = NULL
IF @id IS NOT NULL 
	BEGIN
		SELECT 
			[APPROVED BY] AS approvedby,
			'[' + STUFF((SELECT  ',' + '{"guid":"'+ cast(guid as varchar(36)) + '","filename":"' + filename + '","isactive":"' + cast(isActive as varchar(1)) + '"}'
					FROM [db_fai].[dbo].[fai_Attachments]
					WHERE  id = fai.id
					FOR XML PATH('')), 1, 1, '') + ']' AS attachments,
			[BUYER] AS buyer,
			[CLOSURE COMMENTS] AS closurecomments,
			[COMMENTS] AS comments,
			[COMPANY 1] AS company1,
			[CURRENT  LOCATION] AS currentlocation,
			[CURRENT STATUS] AS currentstatus,
			[CURRENTLY LOCATED AS OF] AS currentlylocatedasof,
			[DATE CODE] AS datecode,
			FORMAT([DATE IN ENGINEERING],'d', @culture) AS dateinengineering,
			FORMAT([DATE IN QA LAB],'d', @culture) AS dateinqalab,
			FORMAT([DATE COMPLETED],'d', @culture) AS datecompleted,
			FORMAT([DATE IN MANUFACTURING],'d', @culture) AS dateinmanufacturing,
			FORMAT([DATE IN SMT],'d', @culture) AS dateinsmt,
			FORMAT([DATE RECEIVED],'d', @culture) AS datereceived,
			[DAYS TO COMPLETE] AS daystocomplete,
			[DESCRIPTION] AS description,
			[DISTRIBUTOR/MANUFACTURER] AS distributormanufacturer,
			[DMR REF #] AS dmrref,
			[ENG APPROVER] AS engapprover,
			FORMAT([ENGINEERING DATE  COMPLETE],'d', @culture) AS engineeringdatecomplete,
			[ENGINEERING EMAIL] AS engineeringemail,
			[ENGINEERING RESULTS] AS engineeringresults,
			[FAI LEVEL] AS failevel,
			[FAI STATUS] AS faistatus,
			[FAI TYPE] AS faitype,
			[FINAL RESULTS] AS finalresults,
			[ID] AS id,
			[INSPECTOR] AS inspector,
			[ITEM NUMBER] AS itemnumber,
			[LOT QTY RECD] AS lotqtyrecd,
			[MANF PASS / FAIL] AS manfpassfail,
			[MANF RESULTS] AS manfresults,
			FORMAT([MANUFACTURING COMPLETE DATE],'d', @culture) AS manufacturingcompletedate,
			[MANUFACTURING NEW PN] AS manufacturingnewpn,
			[MANUFACTURING QTY COMPLETED] AS manufacturingqtycompleted,
			[MANUFACTURING QTY RECIEVED] AS manufacturingqtyrecieved,
			FORMAT([NEED DATE],'d', @culture) AS needdate,
			[PILOT?] AS pilot,
			[PO #] AS po,
			[PROJECT] AS project,
			[PROJECT ENGINEER] AS projectengineer,
			FORMAT([QA  DATE],'d', @culture) AS qadate,
			FORMAT([QA LAB DATE COMPLETE],'d', @culture) AS qalabdatecomplete,
			[qa lab qty completed] AS qalabqtycompleted,
			[qa lab qty received] AS qalabqtyreceived,
			[QA TEST LAB  REPORT #] AS qatestlabreport,
			[QA TEST LAB PASS / FAIL] AS qatestlabpassfail,
			[REASON FOR FAI] AS reasonforfai,
			[REVISION] AS revision,
			LTRIM([RI EVALUTION  - RESULTS]) AS rievalutionresults,
			[RI PASS / FAIL] AS ripassfail,
			[RI QTY INSPECTED] AS riqtyinspected,
			[RI QTY REJECTED] AS riqtyrejected,
			FORMAT([RI COMPLETE DATE],'d', @culture) AS ricompletedate,
			[RMO NUMBER] AS rmonumber,
			[SMT PASS / FAIL] AS smtpassfail,
			[SMT RESULTS] AS smtresults,
			FORMAT([SMT COMPLETE DATE],'d', @culture) AS smtcompletedate,
			[SMT NEW PN] AS smtnewpn,
			[SMT QTY] AS smtqty,
			[SMT QTY COMPLETED] AS smtqtycompleted,
			[SupplierReport]  AS supplierreport,
			[THA RESULTS] AS tharesults,
			[THA PASS]  AS thapassfail
		FROM [db_fai].[dbo].[tbl_FAI] fai
		WHERE ID = @id
	END
ELSE 
	BEGIN

		SET @orderBy = ' order by ' + ISNULL(@sortColumn, '[DATE COMPLETED]') + ' ' + ISNULL(@sortOrder, '')

		--IF(@sortColumn = '[NEED DATE]')
		--	BEGIN
		--		SET @orderBy = ' order by ' + @sortColumn + ' ' + @sortOrder
		--		--SET @orderBy = ' order by [NEED DATE] desc'
		--	END
		--ELSE
		--	BEGIN
		--		SET @orderBy = ' order by ' + @sortColumn + ' ' + @sortOrder
		--		--SET @orderBy = ' order by LOWER(ltrim(' + @sortColumn + ')) ' + @sortOrder-- + ', [NEED DATE] desc'
		--		--SET @orderBy = ' order by id' + ' ' + @sortOrder
		--	END
	print @orderBy

		IF @excelSelect IS NOT NULL
			BEGIN
				SET @select = @excelSelect
			END
		ELSE
			BEGIN
				SET @rowCount = 'SELECT @cnt = count(*)'
				SET @select = 'SELECT @cnt as count
				,dbo.inv([APPROVED BY]) AS approvedby
				,''['' + STUFF((SELECT  '','' + ''{"guid":"''+ cast(guid as varchar(36)) + ''","filename":"'' + filename + ''","isactive":"'' + cast(isActive as varchar(1)) + ''"}''
						FROM [db_fai].[dbo].[fai_Attachments]
						WHERE  id = fai.id
						FOR XML PATH('''')), 1, 1, '''') + '']'' AS attachments
				,dbo.inv([BUYER]) AS buyer
				,dbo.inv([CLOSURE COMMENTS]) AS closurecomments
				,dbo.inv([COMMENTS]) AS comments
				,dbo.inv([COMPANY 1]) AS company1
				,dbo.inv([CURRENT  LOCATION]) AS currentlocation
				,dbo.inv([CURRENT STATUS]) AS currentstatus
				,dbo.inv([CURRENTLY LOCATED AS OF]) AS currentlylocatedasof
				,dbo.inv([DATE CODE]) AS datecode
				,dbo.inv(FORMAT([DATE IN ENGINEERING],''d'', ''' + @culture + ''')) AS dateinengineering
				,dbo.inv(FORMAT([DATE IN QA LAB],''d'', ''' + @culture + ''')) AS dateinqalab
				,dbo.inv(FORMAT([DATE COMPLETED],''d'', ''' + @culture + ''')) AS datecompleted
				,dbo.inv(FORMAT([DATE IN MANUFACTURING],''d'', ''' + @culture + ''')) AS dateinmanufacturing
				,dbo.inv(FORMAT([DATE IN SMT],''d'', ''' + @culture + ''')) AS dateinsmt
				,dbo.inv(FORMAT([DATE RECEIVED],''d'', ''' + @culture + ''')) AS datereceived
				,dbo.inv([DAYS TO COMPLETE]) AS daystocomplete
				,dbo.inv([DESCRIPTION]) AS description
				,dbo.inv([DISTRIBUTOR/MANUFACTURER]) AS distributormanufacturer
				,dbo.inv([DMR REF #]) AS dmrref
				,dbo.inv([ENG APPROVER]) AS engapprover
				,dbo.inv(FORMAT([ENGINEERING DATE  COMPLETE],''d'', ''' + @culture + ''')) AS engineeringdatecomplete
				,dbo.inv([ENGINEERING EMAIL]) AS engineeringemail
				,dbo.inv([ENGINEERING RESULTS]) AS engineeringresults
				,dbo.inv([FAI LEVEL]) AS failevel
				,dbo.inv([FAI STATUS]) AS faistatus
				,dbo.inv([FAI TYPE]) AS faitype
				,dbo.inv([FINAL RESULTS]) AS finalresults
				,dbo.inv([ID]) AS id
				,dbo.inv([INSPECTOR]) AS inspector
				,dbo.inv([ITEM NUMBER]) AS itemnumber
				,dbo.inv([LOT QTY RECD]) AS lotqtyrecd
				,dbo.inv([MANF PASS / FAIL]) AS manfpassfail
				,dbo.inv([MANF RESULTS]) AS manfresults
				,dbo.inv(FORMAT([MANUFACTURING COMPLETE DATE],''d'', ''' + @culture + ''')) AS manufacturingcompletedate
				,dbo.inv([MANUFACTURING NEW PN]) AS manufacturingnewpn
				,dbo.inv([MANUFACTURING QTY COMPLETED]) AS manufacturingqtycompleted
				,dbo.inv([MANUFACTURING QTY RECIEVED]) AS manufacturingqtyrecieved
				,dbo.inv(FORMAT([NEED DATE],''d'', ''' + @culture + ''')) AS needdate
				,dbo.inv([PILOT?]) AS pilot
				,dbo.inv([PO #]) AS po
				,dbo.inv([PROJECT]) AS project
				,dbo.inv([PROJECT ENGINEER]) AS projectengineer
				,dbo.inv(FORMAT([QA  DATE],''d'', ''' + @culture + ''')) AS qadate
				,dbo.inv(FORMAT([QA LAB DATE COMPLETE],''d'', ''' + @culture + ''')) AS qalabdatecomplete
				,dbo.inv([qa lab qty completed]) AS qalabqtycompleted
				,dbo.inv([qa lab qty received]) AS qalabqtyreceived
				,dbo.inv([QA TEST LAB  REPORT #]) AS qatestlabreport
				,dbo.inv([QA TEST LAB PASS / FAIL]) AS qatestlabpassfail
				,dbo.inv([REASON FOR FAI]) AS reasonforfai
				,dbo.inv([REVISION]) AS revision
				,dbo.inv(LTRIM([RI EVALUTION  - RESULTS])) AS rievalutionresults
				,dbo.inv([RI PASS / FAIL]) AS ripassfail
				,dbo.inv([RI QTY INSPECTED]) AS riqtyinspected
				,dbo.inv([RI QTY REJECTED]) AS riqtyrejected
				,dbo.inv(FORMAT([RI COMPLETE DATE],''d'', ''' + @culture + ''')) AS ricompletedate
				,dbo.inv([RMO NUMBER]) AS rmonumber
				,dbo.inv([SMT PASS / FAIL]) AS smtpassfail
				,dbo.inv([SMT RESULTS]) AS smtresults
				,dbo.inv(FORMAT([SMT COMPLETE DATE],''d'', ''' + @culture + ''')) AS smtcompletedate
				,dbo.inv([SMT NEW PN]) AS smtnewpn
				,dbo.inv([SMT QTY]) AS smtqty
				,dbo.inv([SMT QTY COMPLETED]) AS smtqtycompleted
				,dbo.inv([SupplierReport])  AS supplierreport
				,dbo.inv([THA RESULTS]) AS tharesults
				,dbo.inv([THA PASS])  AS thapassfail
				'
				SET @groupBy = ''--'pr.id, pr.PurchaseOrder, pr.Requestor, pr.RequestedDate, li.[key], li.ManufacturePinDescription'
		END
	  --END IF
		SET @from =  ' FROM tbl_FAI fai'
		SET @where = ' WHERE 1=1' + dbo.fai_where(@params)

		--IF @s_company1 IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND [COMPANY 1] LIKE ' + '''%' + @s_company1 + '%'''
		--	END
		--IF @s_description IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND description LIKE ' + '''%' + @s_description + '%'''
		--	END
		--IF @s_distributormanufacturer IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND [DISTRIBUTOR/MANUFACTURER] LIKE ' + '''%' + @s_distributormanufacturer + '%'''
		--	END
		--IF @s_inspector IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND inspector LIKE ' + '''%' + @s_inspector + '%'''
		--	END

		--IF @s_itemnumber IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND [ITEM NUMBER] LIKE ' + '''%' + @s_itemnumber + '%'''
		--	END
		--IF @s_po IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND [PO #] LIKE ' + '''%' + @s_po + '%'''
		--	END
		--IF @s_project IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND project LIKE ' + '''%' + @s_project + '%'''
		--	END

		--IF @s_needdate_from  IS NOT NULL AND @s_needdate_to  IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND CAST([NEED DATE] AS DATE) >=  ''' + @s_needdate_from  + '''  AND CAST([NEED DATE] AS DATE) <=  ''' + @s_needdate_to  + ''''
		--	END
		--IF @s_needdate_from  IS NOT NULL AND @s_needdate_to  IS NULL
		--	BEGIN
		--		SET @where = @where + ' AND CAST([NEED DATE] AS DATE) >= ''' + @s_needdate_from  + ''''
		--	END
		--IF @s_needdate_from  IS NULL AND @s_needdate_to  IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND CAST([NEED DATE] AS DATE) <=  ''' + @s_needdate_to  + ''''
		--	END


		--IF @s_rmonumber IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND CAST([RMO NUMBER] AS varchar(20)) LIKE ' + '''%' + @s_rmonumber + '%'''
		--	END


		--IF @s_ricompletedate_from  IS NOT NULL AND @s_ricompletedateate_to  IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND CAST([RI COMPLETE DATE] AS DATE) >=  ''' + @s_ricompletedate_from  + '''  AND CAST([RI COMPLETE DATE] AS DATE) <=  ''' + @s_ricompletedateate_to  + ''''
		--	END
		--IF @s_ricompletedate_from  IS NOT NULL AND @s_ricompletedateate_to  IS NULL
		--	BEGIN
		--		SET @where = @where + ' AND CAST([RI COMPLETE DATE] AS DATE) >= ''' + @s_ricompletedate_from  + ''''
		--	END
		--IF @s_ricompletedate_from  IS NULL AND @s_ricompletedateate_to  IS NOT NULL
		--	BEGIN
		--		SET @where = @where + ' AND CAST([RI COMPLETE DATE] AS DATE) <=  ''' + @s_ricompletedateate_to  + ''''
		--	END


		IF @excelSelect IS NOT NULL
			BEGIN
				SET @sql = @select + @from + @where + @idNotIn + @orderBy
				EXEC sp_executesql @sql
			END
		ELSE
			BEGIN
				SET @sql = @rowCount  + @from + @where + @idNotIn
				EXEC sp_executesql @sql, N'@cnt int OUTPUT', @cnt=@count OUTPUT
				SET @sql = @select + @from + @where + @idNotIn + @orderBy + @pageFetch
				print @sql
				EXEC sp_executesql @sql , N'@cnt int OUTPUT',@cnt=@count OUTPUT
			END
	END