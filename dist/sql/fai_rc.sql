USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_rc]    Script Date: 11/22/2016 8:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[fai_rc](
	@id INT = NULL,
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL,
	@s_description NVARCHAR(255) = NULL,
	@s_distributormanufacturer NVARCHAR(255) = NULL,
	@s_inspector NVARCHAR(255) = NULL,
	@s_itemnumber NVARCHAR(255) = NULL,
	@s_needdate_from NVARCHAR(50) = NULL,
	@s_needdate_to NVARCHAR(50) = NULL,
	@s_datecompleted_from NVARCHAR(50) = NULL,
	@s_datecompleted_to NVARCHAR(50) = NULL,
	@s_finalresults NVARCHAR(255) = NULL,
	@s_ricompletedate_from NVARCHAR(50) = NULL,
	@s_ricompletedateate_to NVARCHAR(50) = NULL,
	@s_revision NVARCHAR(255) = NULL,
	@sortColumn NVARCHAR(50) = NULL,
	@sortOrder NVARCHAR(5) = NULL,
	@custom NVARCHAR(2000) = NULL
) AS



DECLARE
	@rowCount   NVARCHAR(100),
	@pageFetch  NVARCHAR(300),
	@cnt INT,
	@count INT,
	@recordSet INT,
	@currentDate DateTime,
	@culture nvarchar(10)

SET @culture = 'en-US'
SET @currentDate = GETDATE()

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' + CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END

SELECT @cnt = count(*) 
FROM tbl_fai
WHERE [RI COMPLETE DATE] Is Not Null AND [FAI STATUS]='IN WORK' AND [DATE COMPLETED] Is Not Null

SELECT 
	@cnt as count,
	[APPROVED BY] AS approvedby,
	'[' + STUFF((SELECT  ',' + '{"guid":"'+ cast(guid as varchar(36)) + '","filename":"' + filename + '","isactive":"' + cast(isActive as varchar(1)) + '"}'
			FROM [db_fai].[dbo].[fai_Attachments]
			WHERE  id = fai.id
			FOR XML PATH('')), 1, 1, '') + ']' AS attachments,
	[BUYER] AS buyer,
	[CLOSURE COMMENTS] AS closurecomments,
	[COMMENTS] AS comments,
	[COMPANY 1] AS company1,
	[CURRENT  LOCATION] AS currentlocation,
	[CURRENT STATUS] AS currentstatus,
	[CURRENTLY LOCATED AS OF] AS currentlylocatedasof,
	[DATE CODE] AS datecode,
	FORMAT([DATE IN ENGINEERING],'d', @culture) AS dateinengineering,
	FORMAT([DATE IN QA LAB],'d', @culture) AS dateinqalab,
	FORMAT([DATE COMPLETED],'d', @culture) AS datecompleted,
	FORMAT([DATE IN MANUFACTURING],'d', @culture) AS dateinmanufacturing,
	FORMAT([DATE IN SMT],'d', @culture) AS dateinsmt,
	FORMAT([DATE RECEIVED],'d', @culture) AS datereceived,
	[DAYS TO COMPLETE] AS daystocomplete,
	[DESCRIPTION] AS description,
	[DISTRIBUTOR/MANUFACTURER] AS distributormanufacturer,
	[DMR REF #] AS dmrref,
	[ENG APPROVER] AS engapprover,
	FORMAT([ENGINEERING DATE  COMPLETE],'d', @culture) AS engineeringdatecomplete,
	[ENGINEERING EMAIL] AS engineeringemail,
	[ENGINEERING RESULTS] AS engineeringresults,
	[FAI LEVEL] AS failevel,
	[FAI STATUS] AS faistatus,
	[FAI TYPE] AS faitype,
	[FINAL RESULTS] AS finalresults,
	[ID] AS id,
	[INSPECTOR] AS inspector,
	[ITEM NUMBER] AS itemnumber,
	[LOT QTY RECD] AS lotqtyrecd,
	[MANF PASS / FAIL] AS manfpassfail,
	[MANF RESULTS] AS manfresults,
	FORMAT([MANUFACTURING COMPLETE DATE],'d', @culture) AS manufacturingcompletedate,
	[MANUFACTURING NEW PN] AS manufacturingnewpn,
	[MANUFACTURING QTY COMPLETED] AS manufacturingqtycompleted,
	[MANUFACTURING QTY RECIEVED] AS manufacturingqtyrecieved,
	FORMAT([NEED DATE],'d', @culture) AS needdate,
	[PILOT?] AS pilot,
	[PO #] AS po,
	[PROJECT] AS project,
	[PROJECT ENGINEER] AS projectengineer,
	FORMAT([QA  DATE],'d', @culture) AS qadate,
	FORMAT([QA LAB DATE COMPLETE],'d', @culture) AS qalabdatecomplete,
	[qa lab qty completed] AS qalabqtycompleted,
	[qa lab qty received] AS qalabqtyreceived,
	[QA TEST LAB  REPORT #] AS qatestlabreport,
	[QA TEST LAB PASS / FAIL] AS qatestlabpassfail,
	[REASON FOR FAI] AS reasonforfai,
	[REVISION] AS revision,
	LTRIM([RI EVALUTION  - RESULTS]) AS rievalutionresults,
	[RI PASS / FAIL] AS ripassfail,
	[RI QTY INSPECTED] AS riqtyinspected,
	[RI QTY REJECTED] AS riqtyrejected,
	FORMAT([RI COMPLETE DATE],'d', @culture) AS ricompletedate,
	[RMO NUMBER] AS rmonumber,
	[SMT PASS / FAIL] AS smtpassfail,
	[SMT RESULTS] AS smtresults,
	FORMAT([SMT COMPLETE DATE],'d', @culture) AS smtcompletedate,
	[SMT NEW PN] AS smtnewpn,
	[SMT QTY] AS smtqty,
	[SMT QTY COMPLETED] AS smtqtycompleted,
	[SupplierReport]  AS supplierreport,
	[THA RESULTS] AS tharesults,
	[THA PASS]  AS thapassfail
FROM [db_fai].[dbo].[tbl_FAI] fai
WHERE [RI COMPLETE DATE] Is Not Null AND [FAI STATUS]='IN WORK' AND [DATE COMPLETED] Is Not Null
ORDER BY (getdate()-[DATE RECEIVED]) desc
OFFSET @recordSet ROWS FETCH NEXT @itemsPerPage ROWS ONLY


