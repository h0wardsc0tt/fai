USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_Update]    Script Date: 11/30/2016 6:36:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[fai_Update](
	@id INT,
	@approvedby NVARCHAR(255) = NULL,
	@buyer NVARCHAR(255) = NULL,
	@closurecomments NVARCHAR(MAX) = NULL,
	@comments NVARCHAR(255) = NULL,
	@company1 NVARCHAR(255) = NULL,
	@currentlocation NVARCHAR(255) = NULL,
	@currentlylocatedasof NVARCHAR(25) = NULL,
	@currentstatus NVARCHAR(255) = NULL,
	@datecode NVARCHAR(255) = NULL,
	@datecompleted NVARCHAR(25) = NULL,
	@dateinengineering NVARCHAR(25) = NULL,
	@dateinmanufacturing NVARCHAR(25) = NULL,
	@dateinqalab NVARCHAR(25) = NULL,
	@dateinsmt NVARCHAR(25) = NULL,
	@datereceived NVARCHAR(25) = NULL,
	@datesenttoqe NVARCHAR(25) = NULL,
	@daystocomplete NVARCHAR(15) = NULL,
	@description NVARCHAR(255) = NULL,
	@distributormanufacturer NVARCHAR(255) = NULL,
	@dmrref NVARCHAR(15) = NULL,
	@engapprover NVARCHAR(255) = NULL,
	@engineeringdatecomplete NVARCHAR(25) = NULL,
	@engineeringemail NVARCHAR(255) = NULL,
	@engineeringresults NVARCHAR(255) = NULL,
	@engqtyreceived NVARCHAR(15) = NULL,
	@failevel NVARCHAR(255) = NULL,
	@faistatus NVARCHAR(255) = NULL,
	@faitype NVARCHAR(255) = NULL,
	@finalresults NVARCHAR(255) = NULL,
	@inspector NVARCHAR(255) = NULL,
	@itemnumber NVARCHAR(255) = NULL,
	@lotqtyrecd NVARCHAR(15) = NULL,
	@manfpassfail NVARCHAR(255) = NULL,
	@manfresults NVARCHAR(255) = NULL,
	@manufacturingcompletedate NVARCHAR(25) = NULL,
	@manufacturingnewpn NVARCHAR(255) = NULL,
	@manufacturingqtycompleted NVARCHAR(255) = NULL,
	@manufacturingqtyrecieved NVARCHAR(255) = NULL,
	@needdate NVARCHAR(25) = NULL,
	@pilot BIT = NULL,
	@po NVARCHAR(255) = NULL,
	@project NVARCHAR(255) = NULL,
	@projectengineer NVARCHAR(MAX) = NULL,
	@qadate NVARCHAR(25) = NULL,
	@qalabdatecomplete NVARCHAR(255) = NULL,
	@qalabqtycompleted NVARCHAR(255) = NULL,
	@qalabqtyreceived NVARCHAR(255) = NULL,
	@qatestlabpassfail NVARCHAR(255) = NULL,
	@qatestlabreport NVARCHAR(255) = NULL,
	@reasonforfai NVARCHAR(255) = NULL,
	@revision NVARCHAR(255) = NULL,
	@ricompletedate NVARCHAR(25) = NULL,
	@rievalutionresults NVARCHAR(MAX) = NULL,
	@ripassfail NVARCHAR(255) = NULL,
	@riqtyinspected NVARCHAR(15) = NULL,
	@riqtyrejected NVARCHAR(15) = NULL,
	@rmonumber NVARCHAR(15) = NULL,
	@smtcompletedate NVARCHAR(255) = NULL,
	@smtnewpn NVARCHAR(255) = NULL,
	@smtpassfail NVARCHAR(255) = NULL,
	@smtqty NVARCHAR(15) = NULL,
	@smtqtycompleted NVARCHAR(15) = NULL,
	@smtqtyreceived NVARCHAR(15) = NULL,
	@smtresults NVARCHAR(255) = NULL,
	@supplierreport BIT = NULL,
	@thapassfail NVARCHAR(255) = NULL,
	@tharesults NVARCHAR(255) = NULL,
	@idenity INT OUTPUT,
	@sqlRequest NVARCHAR(15) OUTPUT
)AS

SET @idenity =  @id
SET @sqlRequest = 'update'

UPDATE [dbo].[tbl_FAI]
SET
	 [approved by] = dbo.ifNull255(@approvedby, [approved by])
	,[buyer] = dbo.ifNull255(@buyer, [buyer])
	,[closure comments] = dbo.ifNull255(@closurecomments, [closure comments])
	,[comments] = dbo.ifNull255(@comments, [comments])
	,[company 1] = dbo.ifNull255(@company1, [company 1])
	,[current  location] = dbo.ifNull255(@currentlocation, [current  location])
	,[current status] = dbo.ifNull255(@currentstatus, [current status])
	,[currently located as of] =  CASE WHEN ISDATE(@currentlylocatedasof) = 1 THEN @currentlylocatedasof ELSE NULL END
	,[date code] = dbo.ifNull255(@datecode, [date code])
	,[date completed] = CASE WHEN ISDATE(@datecompleted) = 1 THEN @datecompleted ELSE NULL END
	,[date in engineering] = CASE WHEN ISDATE(@dateinengineering) = 1 THEN @dateinengineering ELSE NULL END
	,[date in manufacturing] = CASE WHEN ISDATE(@dateinmanufacturing) = 1 THEN @dateinmanufacturing ELSE NULL END
	,[date in qa lab] = CASE WHEN ISDATE(@dateinqalab) = 1 THEN @dateinqalab ELSE NULL END
	,[date in smt] = CASE WHEN ISDATE(@dateinsmt) = 1 THEN @dateinsmt ELSE NULL END
	,[date received] = CASE WHEN ISDATE(@datereceived) = 1 THEN @datereceived ELSE NULL END
	,[date sent to qe] = CASE WHEN ISDATE(@datesenttoqe) = 1 THEN @datesenttoqe ELSE NULL END
	,[days to complete] = CASE WHEN @daystocomplete IS NULL THEN [days to complete] ELSE CASE WHEN @daystocomplete = '' THEN NULL ELSE @daystocomplete END END
	,[description] = dbo.ifNull255(@description, [description])
	,[distributor/manufacturer] = dbo.ifNull255(@distributormanufacturer, [distributor/manufacturer])
	,[dmr ref #] = CASE WHEN @dmrref IS NULL THEN [dmr ref #] ELSE CASE WHEN @dmrref = '' THEN NULL ELSE @dmrref END END
	,[eng approver] = dbo.ifNull255(@engapprover, [eng approver])
	,[engineering date  complete] = CASE WHEN ISDATE(@engineeringdatecomplete) = 1 THEN @engineeringdatecomplete ELSE NULL END
	,[engineering email] = dbo.ifNull255(@engineeringemail, [engineering email])
	,[engineering results] = dbo.ifNull255(@engineeringresults, [engineering results])
	,[eng qty received] = CASE WHEN @engqtyreceived IS NULL THEN [eng qty received] ELSE CASE WHEN @engqtyreceived = '' THEN NULL ELSE @engqtyreceived END END
	,[fai level] = dbo.ifNull255(@failevel, [fai level])
	,[fai status] = dbo.ifNull255(@faistatus, [fai status])
	,[fai type] = dbo.ifNull255(@faitype, [fai type])
	,[final results] = dbo.ifNull255(@finalresults, [final results])
	,[inspector] = dbo.ifNull255(@inspector, [inspector])
	,[item number] = dbo.ifNull255(@itemnumber, [item number])
	,[lot qty recd] = CASE WHEN @lotqtyrecd IS NULL THEN [lot qty recd] ELSE CASE WHEN @lotqtyrecd = '' THEN NULL ELSE @lotqtyrecd END END
	,[manf pass / fail] = dbo.ifNull255(@manfpassfail, [manf pass / fail])
	,[manf results] = dbo.ifNull255(@manfresults, [manf results])
	,[manufacturing complete date] = CASE WHEN ISDATE(@manufacturingcompletedate) = 1 THEN @manufacturingcompletedate ELSE NULL END
	,[manufacturing new pn] = dbo.ifNull255(@manufacturingnewpn, [manufacturing new pn])
	,[manufacturing qty completed] = CASE WHEN @manufacturingqtycompleted IS NULL THEN [manufacturing qty completed] ELSE CASE WHEN @manufacturingqtycompleted = '' THEN NULL ELSE @manufacturingqtycompleted END END
	,[manufacturing qty recieved] = CASE WHEN @manufacturingqtyrecieved IS NULL THEN [manufacturing qty recieved] ELSE CASE WHEN @manufacturingqtyrecieved = '' THEN NULL ELSE @manufacturingqtyrecieved END END
	,[need date] = CASE WHEN ISDATE(@needdate) = 1 THEN @needdate ELSE NULL END
	,[pilot?] = ISNULL(@pilot, [pilot?])
	,[po #] = dbo.ifNull255(@po, [po #])
	,[project] = dbo.ifNull255(@project, [project])
	,[project engineer] = ISNULL(@projectengineer, [project engineer])
	,[qa  date] = CASE WHEN ISDATE(@qadate) = 1 THEN @qadate ELSE NULL END
	,[qa lab date complete] = CASE WHEN ISDATE(@qalabdatecomplete) = 1 THEN @qalabdatecomplete ELSE NULL END
	,[qa lab qty completed] = CASE WHEN @qalabqtycompleted IS NULL THEN [qa lab qty completed] ELSE CASE WHEN @qalabqtycompleted = '' THEN NULL ELSE @qalabqtycompleted END END
	,[qa lab qty received] = CASE WHEN @qalabqtyreceived IS NULL THEN [qa lab qty received] ELSE CASE WHEN @qalabqtyreceived = '' THEN NULL ELSE @qalabqtyreceived END END
	,[qa test lab  report #] = dbo.ifNull255(@qatestlabreport, [qa test lab  report #])
	,[qa test lab pass / fail] = dbo.ifNull255(@qatestlabpassfail, [qa test lab pass / fail])
	,[reason for fai] = dbo.ifNull255(@reasonforfai, [reason for fai])
	,[revision] = dbo.ifNull255(@revision, [revision])
	,[ri complete date] = CASE WHEN ISDATE(@ricompletedate) = 1 THEN @ricompletedate ELSE NULL END
	,[ri evalution  - results] = CASE WHEN @rievalutionresults IS NULL THEN [ri evalution  - results] ELSE CASE WHEN @rievalutionresults = '' THEN NULL ELSE @rievalutionresults END END
	--,[ri evalution  - results] = dbo.ifNull255(@rievalutionresults, [ri evalution  - results])
	,[ri pass / fail] = dbo.ifNull255(@ripassfail, [ri pass / fail])
	,[ri qty inspected] = CASE WHEN @riqtyinspected IS NULL THEN [ri qty inspected] ELSE CASE WHEN @riqtyinspected = '' THEN NULL ELSE @riqtyinspected END END
	,[ri qty rejected] = CASE WHEN @riqtyrejected IS NULL THEN [ri qty rejected] ELSE CASE WHEN @riqtyrejected = '' THEN NULL ELSE @riqtyrejected END END
	,[rmo number] = CASE WHEN @rmonumber IS NULL THEN [rmo number] ELSE CASE WHEN @rmonumber = '' THEN NULL ELSE @rmonumber END END
	,[smt complete date] = CASE WHEN ISDATE(@smtcompletedate) = 1 THEN @smtcompletedate ELSE NULL END
	,[smt new pn] = dbo.ifNull255(@smtnewpn, [smt new pn])
	,[smt pass / fail] = dbo.ifNull255(@smtpassfail, [smt pass / fail])
	,[smt qty] = CASE WHEN @smtqty IS NULL THEN [smt qty] ELSE CASE WHEN @smtqty = '' THEN NULL ELSE @smtqty END END
	,[smt qty completed] = CASE WHEN @smtqtycompleted IS NULL THEN [smt qty completed] ELSE CASE WHEN @smtqtycompleted = '' THEN NULL ELSE @smtqtycompleted END END
	,[smt qty received] = CASE WHEN @smtqtyreceived IS NULL THEN [smt qty received] ELSE CASE WHEN @smtqtyreceived = '' THEN NULL ELSE @smtqtyreceived END END
	,[smt results] = dbo.ifNull255(@smtresults, [smt results])
	,[supplierreport] = ISNULL(@supplierreport, [supplierreport])
	,[tha pass] = dbo.ifNull255(@thapassfail, [tha pass])
	,[tha results] = dbo.ifNull255(@tharesults, [tha results])
WHERE id = @id
