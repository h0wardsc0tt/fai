USE [db_fai]
GO
/****** Object:  UserDefinedFunction [dbo].[jsonattachments]    Script Date: 11/22/2016 8:55:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[jsonattachments](@id int)
RETURNS NVARCHAR(4000)
AS
BEGIN
	DECLARE @json NVARCHAR(4000)
	IF EXISTS (SELECT id FROM [db_fai].[dbo].[fai_Attachments]  WHERE  id = @id)
		BEGIN
			SET  @json = '[' + STUFF((SELECT  ',' + '{"guid":"'+ cast(guid as varchar(36)) + '","filename":"' + filename + '","isactive":"' + cast(isActive as varchar(1)) + '"}'
					FROM [db_fai].[dbo].[fai_Attachments]
					WHERE  id = @id
					FOR XML PATH('')), 1, 1, '') + ']'
		END
	ELSE
		BEGIN
				SET @json = '[]'
		END

	RETURN  @json
END
