USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[fai_buyer]    Script Date: 11/2/2016 10:08:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[fai_buyer]
AS
SELECT DISTINCT 'buyer' AS [key], UPPER(buyer) AS [value] 
FROM tbl_fai
WHERE buyer IS NOT NULL AND buyer != ''