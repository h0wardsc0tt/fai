USE [db_fai]
GO
/****** Object:  StoredProcedure [dbo].[save_qryClosedDayBefore]    Script Date: 11/22/2016 8:52:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[save_qryClosedDayBefore](
 @startDate DateTime,
 @endDate DateTime
)AS

DECLARE @currentDate DateTime
SET @currentDate = GETDATE()

SELECT 
	[COMPANY 1],
	ID,
	[ITEM NUMBER],
	DESCRIPTION,
	REVISION,
	[DISTRIBUTOR/MANUFACTURER],
	[FAI TYPE],
	[DATE RECEIVED],
	[RI COMPLETE DATE],
	[REASON FOR FAI],
	[FAI STATUS],
	[FAI LEVEL],
	[DATE IN SMT],
	[SMT COMPLETE DATE],
	[DATE IN MANUFACTURING],
	[MANUFACTURING COMPLETE DATE],
	[DATE IN QA LAB],
	[QA LAB DATE COMPLETE],
	[DATE IN ENGINEERING],
	[ENGINEERING DATE  COMPLETE],
	CASE 
		WHEN [DATE IN QA LAB] IS NOT Null THEN 'QA Lab'
		ELSE
			CASE 
			WHEN [DATE IN MANUFACTURING] IS NOT Null THEN 'MFG'
			ELSE
				CASE 
				WHEN [DATE IN SMT] IS NOT Null THEN 'SMT'
				ELSE
					CASE 
					WHEN [DATE IN ENGINEERING] IS NOT Null THEN 'Engineering'
					ELSE 'Rec Insp'
					END
				END
			END
	END AS [Current Location],

	CASE 
		WHEN [DATE IN QA LAB] IS NOT Null THEN DATEDIFF(DAY, [DATE IN QA LAB], @currentDate)
		ELSE
			CASE 
			WHEN [DATE IN MANUFACTURING] IS NOT Null THEN DATEDIFF(DAY, [DATE IN MANUFACTURING], @currentDate)
			ELSE
				CASE 
				WHEN [DATE IN SMT] IS NOT Null THEN DATEDIFF(DAY, [DATE IN SMT], @currentDate)
				ELSE
					CASE 
					WHEN [DATE IN ENGINEERING] IS NOT Null THEN DATEDIFF(DAY, [DATE IN ENGINEERING], @currentDate)
					ELSE  DATEDIFF(DAY, [DATE RECEIVED], @currentDate)
					END
				END
			END
	END AS [Days in Current Location],

	DATEDIFF(DAY, [DATE RECEIVED], @currentDate) AS [Total Days In Work],[DATE COMPLETED],
	[FINAL RESULTS]
FROM [db_fai].[dbo].[tbl_FAI]
WHERE ((([FAI STATUS])='CLOSED') AND (([DATE COMPLETED])>=@startDate And ([DATE COMPLETED])<=@endDate))
ORDER BY @currentDate-[DATE RECEIVED] DESC, ID;