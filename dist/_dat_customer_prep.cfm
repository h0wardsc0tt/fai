<cfparam name="FORM.CSP_Page_Form" default="">
<cfparam name="FORM.CSP_Per_Page" default="5">
<cfparam name="FORM.CSP_Current_Page" default="1">
<cfparam name="VARIABLES.Page_Start" default="1">
<cfparam name="VARIABLES.Page_End" default="5">
<cfparam name="VARIABLES.Sort_Column" default="Created_DTS">
<cfparam name="VARIABLES.Sort_Order" default="desc">

<cfparam name="FORM.Lead_Name" default="">
<cfparam name="FORM.Lead_City" default="">
<cfparam name="FORM.Lead_State" default="">
<cfparam name="FORM.Lead_Zip" default="">
<cfparam name="FORM.Lead_Email" default="">
<cfparam name="FORM.Lead_Company" default="">
<cfparam name="FORM.Lead_StoreNumber" default="">
<cfparam name="FORM.Lead_Interest" default="">
<cfparam name="FORM.Lead_Phone" default="">
<cfparam name="FORM.Lead_Type" default="">
<cfparam name="FORM.Lead_Date_To" default="">
<cfparam name="FORM.Lead_Date_From" default="">
<cfparam name="FORM.Sort_Column" default="Created_DTS">
<cfparam name="FORM.Sort_Order" default="desc">

<cfscript>
	CustInfoObj = CreateObject("component", "_cfcs.CustInfo");
	serializer = new lib.JsonSerializer();

	CustomerCount = 0;
	leads_json = '';
	
	leadFilter = StructNew();
	leadFilter['pageNumber'] = FORM.CSP_Current_Page;
	leadFilter['itemsPerPage'] = FORM.CSP_Per_Page;
	leadFilter['name'] = FORM.Lead_Name;
	leadFilter['city'] = FORM.Lead_City;
	leadFilter['state'] = FORM.Lead_State;
	leadFilter['zip'] = FORM.Lead_Zip;
	leadFilter['email'] = FORM.Lead_Email;
	leadFilter['company'] = FORM.Lead_Company;
	leadFilter['storeNumber'] = FORM.Lead_StoreNumber;
	leadFilter['company'] = FORM.Lead_Company;
	leadFilter['phone'] = FORM.Lead_Phone;
	leadFilter['type'] = FORM.Lead_Type;
	leadFilter['fromDate'] = FORM.Lead_Date_From;
	leadFilter['toDate'] = FORM.Lead_Date_To;
	leadFilter['storedProcedure'] = 'dbo.[Leads_Select]';
	leadFilter['sortColumn'] = "Lead_" & FORM.Sort_Column;
	leadFilter['sortOrder'] = FORM.Sort_Order;

//writedump(leadFilter);
	returnSelect = CustInfoObj.LeadsSelect(argumentCollection = leadFilter);
	//writedump(returnSelect.RecordCount);
	//deleteStatus = StructDelete(leadFilter,'pageNumber','True');
	//deleteStatus = StructDelete(leadFilter,'itemsPerPage','True');
	deleteStatus = StructDelete(leadFilter,'storedProcedure','True');
	leadFilter_json = serializer.serialize(StructCopy(leadFilter));
	returnSelectResult = DeserializeJSON(SerializeJSON(returnSelect, 'true'));
    leads_json = replace(replace(replace(replace(serializer.serialize(returnSelect),"\n","\\n","all"),"\r","\\r","all"),"\t","\\t","all"),"'","\u0027","all");
 	StructClear(leadFilter);		
	VARIABLES.Page_Start = ((FORM.CSP_Current_Page-1)*FORM.CSP_Per_Page)+1;
	VARIABLES.Page_End = FORM.CSP_Current_Page*FORM.CSP_Per_Page;
	VARIABLES.Sort_Column = FORM.Sort_Column;
	VARIABLES.Sort_Order = FORM.Sort_Order;
	
</cfscript>



