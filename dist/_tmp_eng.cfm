
<div class="row" style="padding-bottom:3px;">
  <div class="form-col eng-only eng-only-bgcolor col-xs-12"> (WHEN REQUIRED ONLY) ENGINEERING </div>
</div>
<div class="row" style="position:relative;">
  <div class="blockout_level_5 blockout_section"></div>
  <div class="form-col fai-cols  col-xs-6">
    <div class="form-group">
      <label for="engqtyreceived" class="control-label eng-only-bgcolor">ENG QTY RECEIVED :</label>
      <div class="fai-input">
        <input type="text" maxlength="100" name="engqtyreceived" id="engqtyreceived" class="level5 form-control fai-input-color numeric"/>
      </div>
    </div>
    <div class="form-group">
      <label for="dateinengineering" class="control-label eng-only-bgcolor">DATE IN ENG :</label>
      <div class="fai-input">
        <input type="text" autocomplete="off" maxlength="100" name="dateinengineering" id="dateinengineering" 
                          class="level5 form-control input-date"/>
      </div>
    </div>
    <div class="form-group">
      <label for="engineeringdatecomplete" class="control-label eng-only-bgcolor">ENG DATE COMPLETE :</label>
      <div class="fai-input">
        <input type="text" autocomplete="off" maxlength="100" name="engineeringdatecomplete" id="engineeringdatecomplete" 
                          class="level5 form-control input-date"/>
      </div>
    </div>
    <div class="form-group">
      <label for="engapprover" class="control-label eng-only-bgcolor">ENG APPROVER :</label>
      <div class="fai-input">
        <input type="text" maxlength="100" name="engapprover" id="engapprover" class="level5 form-control noEnterKey"/>
      </div>
    </div>
	<div class="form-group">
		<label for="previousevid" class="control-label eng-only-bgcolor">PREVIOUS EVID :</label>
		<div class="fai-input" style="position: relative;padding-left: 40px;">
			<input type="checkbox" id="previousevidck" style="height: 20px;width: 20px;position: absolute;left: 15px;">

			<input type="text" maxlength="100" name="previousevid" id="previousevid" class="level5 form-control fai-input-color"/>
		</div>
	</div>                   
	<div class="form-group">
		<label for="engineeringwo" class="control-label eng-only-bgcolor">ENGINEERING WO # :</label>
		<div class="fai-input" style="position: relative;padding-left: 40px;">
			<input type="checkbox" id="engineeringwock" style="height: 20px;width: 20px;position: absolute;left: 15px;">

			<input type="text" maxlength="100" name="engineeringwo" id="engineeringwo" class="level5 form-control fai-input-color"/>
		</div>
	</div>                   

  <!---  <div class="eng-left form-col col-xs-12" style="padding-left:0;">
       <div class="form-col col-xs-6" style="padding-left:0;">
        <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
            <input type="checkbox" id="engineeringwock" style="height: 20px;width: 20px;">
            <input type="text" maxlength="100" name="engineeringwo" id="engineeringwo" placeholder="Engineering WO #"  
			 style= "display:inline-block; width:200px;" class="level4 form-control noEnterKey"/>
          </div>
        </div>
        <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
              <input class="form-check-input" type="checkbox" id="inspectallck" style="height: 20px;width: 20px;">
               <label class="form-check-label" for="inspectallck" style="font-weight: normal;">Inspect All Dimensions</label>
          </div>
        </div>

        
        
      </div>
      <div class="form-col col-xs-6" style="padding-left:0;">
        <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
            <input type="checkbox" id="previousevidck" style="height: 20px;width: 20px;">
            <input type="text" maxlength="100" name="previousevid" id="previousevid" placeholder="PreviousEVID"  
			 style= "display:inline-block; width:200px;" class="level4 form-control noEnterKey"/>
          </div>
        </div>
        <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
              <input class="form-check-input" type="checkbox" id="oktodestroyck" style="height: 20px;width: 20px;">
               <label class="form-check-label" for="oktodestroyck" style="font-weight: normal;">OK to destroy part</label>
          </div>
        </div>
 

      </div>
    </div>--->
  </div>
  <div class="form-col fai-right-cols col-xs-6">
    <div class="form-group" style="margin-bottom:0px;">
      <textarea id="engineeringresults" class="level5-textarea test-results" name="engineeringresults" style="overflow-y: auto"> 
                    </textarea>
    </div>
    <div class="form-col qa-lab-report col-xs-12" style="padding-left:0;">
      <div class="form-col col-xs-6" style="padding-left:0;">
         <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="inspectcriticalck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="inspectcriticalck" style="font-weight: normal;">Inspect Critical Dimensions</label>
            </div>
          </div>
        </div>
        <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="inspectcircledck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="inspectcircledck" style="font-weight: normal;">Inspect Circled Dimensions</label>
            </div>
          </div>
        </div>
        <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="inspectallck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="inspectallck" style="font-weight: normal;">Inspect All Dimensions</label>
            </div>
          </div>
        </div>    
         <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="oktodestroyck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="oktodestroyck" style="font-weight: normal;">OK to destroy part</label>
            </div>
          </div>
        </div>  
         <div class="form-group" style="margin-bottom: 0px;">
          <div class="fai-input">
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="rmtoengafterinspectck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="rmtoengafterinspectck" style="font-weight: normal;">Return material to Eng after Inspection</label>
            </div>
          </div>
        </div>               
      </div>
      <div class="form-col col-xs-6" style="padding-left:0;">
        <div class="form-group" style="margin-bottom: 0px;">
          <div>
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="ingorepkgrohsfitcheckck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="ingorepkgrohsfitcheckck" style="font-weight: normal;">Ignore Pkg & RoHS</label>
            </div>
          </div>
           <div>
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="igorefitcheckck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="igorefitcheckck" style="font-weight: normal;">Ignore Fit Check</label>
            </div>
          </div>
         
          <div>
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="ignorenotesck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="ignorenotesck" style="font-weight: normal;">Ignore All Notes</label>
            </div>
          </div>

          <div>
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="ignoretextureck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="ignoretextureck" style="font-weight: normal;">Ignore Texture</label>
            </div>
          </div>
          <div>
            <div style="display:inline-block;">
              <input class="form-check-input" type="checkbox" id="ignorepartmarketingck" style="height: 20px;width: 20px;">
            </div>
            <div style="display:inline-block;">
              <label class="form-check-label" for="ignorepartmarketingck" style="font-weight: normal;">Ignore Part Marking</label>
            </div>
          </div>
      
       
        </div>
      </div>
    </div>
  </div>
</div>
