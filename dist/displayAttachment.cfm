<!--- Retrive attachment from Database --->
<cfinclude template="./serverSettings.cfm">
<cfsetting showdebugoutput="no">
<cfset file_guid = url.guid />
<cfinclude template="./cfincludes/_functions.cfm">

<cfscript>
	//application/powerpoint	
	obj = getdbComponent();
	fileAttachment = obj.get_Attachment(file_guid);
	contentType = getContentType(fileAttachment.prefix);
</cfscript>

<!---<cfheader name="Content-Disposition" value="attachment; filename=#replace(replace(fileAttachment.fileName," ","_","ALL"), ",", "_")#">--->
<cfheader name="Content-Disposition" value="inline; filename=#replace(replace(fileAttachment.fileName," ","_","ALL"), ",", "_")#">
<cfcontent reset = "Yes" type = "#contentType#" variable = "#fileAttachment.file#" />      

<!---
<cfmail to="hscott@hme.com" from="foo@foo.com" subject="Your PDF"> 
<cfmailparam file="#replace(replace(fileAttachment.fileName," ","_","ALL"), ",", "_")#" content="#fileAttachment.file#"> 
	Attached please find your cool PDF. Enjoy! 
    sdasd
    asd
</cfmail>

--->
