<cfdocument format="PDF" orientation = "landscape" filename="#tmp_file_name#" overwrite="Yes"
marginLeft = ".1" 
marginTop = ".1"
marginRight = ".1"  >
<cfdocumentitem type="footer"> 
    <cfoutput>
        <table style="font-size:10px; color:##C3B8B8; width:100%;"><tr>
        <td align="left">
            HME 555006
        </td>
        <td align="center">
           HME CONFIDENTAL AND PROPRIETARY INFORMATION
        </td>
        <td align="right">
            Page #cfdocument.currentpagenumber# of #cfdocument.totalpagecount#
        </td>
        </tr>
        </table>
    </cfoutput> 
</cfdocumentitem> 

<cfset todayDate = Now()> 

<cfscript>		
		obj = CreateObject("component", "_cfcs.faiReports");
		fai = obj.engineering_wip_report();
</cfscript>


<cfoutput>

<style>

body{
	font-size:12px;
}

table{
	font-size:12px;
}
.bold{
	font-weight:bold;
	font-size:15px;
}

.title{
	color:##0813E9;
	font-size:13px;
}

.title2{
	color:##0813E9;
	font-size:13px;
}
.info{
	color:##000;
	font-size:13px;
	/*border:solid 1px red;*/
}
.width1{

	width:100px;
}

.po-color{
	background-color:##e6b9b8;
}

.ri-color{
	background-color:##CBC5C5;
}

.smt-color{
	background-color:##c4bd97;
}

.mfg-color{
	background-color:##fac090;
}

.eng-color{
	background-color:##000;
}

.eng-font-color{
	color:##EBF106;
}

.qa-color{
	background-color:##c3d69b;
}

.fr-color{
	background-color:##9A9696;
}

.title-dev{
	padding:2px 0px 1px 5px;
}

.val-dev{
	border:solid 1px ##D0C8C8; 
	padding:0px 0px 0px 1px;
	margin:1px 0px 1px 0px;
	
}

.first{
	margin:2px 0px 1px 0px;
}

.fail{
	color:##F00609;
}

.pass{
	color:##06F430;
}

.right-val_lenght{
	width:136px;
}


.right-val_lenght2{
	width:141px;
}

td{
	border-left:solid 1px ##dddddd;
	border-bottom:solid 1px ##dddddd;
	padding:2px 5px 2px 5px;
	vertical-align:middle;
}

.bottom{
	border-left:none !important;
	border-bottom:solid 1px ##0813E9;
}
.no-border{
	border-left:none !important;
	border-bottom:none !important;
}
th{
	border-bottom:solid 1px ##dddddd;
}

.right{
	border-right:solid 1px ##dddddd;
}
.left{
	text-align:left;
}
.center{
	text-align:center;
}
.odd {
	background-color: ##EEEEEE;
}
.even {
	background-color: ##FFFFFF;
}

.red {
	color: ##FF0000;
}
</style>
    
<body>
<form action="" method="POST" id="FAI_Report_Form" class="form-horizontal col-lg-12" role="form">
	<img style="height:30px; height:auto;" src=#localUrl(logoImage)#><br>
    <span style="font-size:15px; color:##CCC8C8;">First Article Inspection</span><br>
	<div align="center" style="font-size:25px;color:##0813E9;">FAI's - Engineering Work In Progress Report</div>
    <div align="center" style="font-size:15px;text-align:center; margin:0px 5px 5px 5px;">
        <span style="color:##333351;"><span style="font-weight:bold;">AS OF: </span> #DateFormat(todayDate, "full")#&nbsp;&nbsp;&nbsp;</span>
        <span style="color:##333351;">#TimeFormat(todayDate, "full")#</span>
    </div>
     <div align="center" style="font-weight:bold; font-size:15px;margin-bottom:10px;">Total FAI's Closed = <span style="color:##ff0000;">#fai.recordcount#</span></div>
    <div style="margin:0px 5px 10px 5px;">
<cfset totalrecords= 0>
<cfset noofrecords= 0>
<cfloop query = #fai#  group="company1"> 
    <cfif totalrecords eq 0>
        <div style="font-size:15px;color:##0813E9;padding:10px 0px 10px 0px;">#UCase(company1)#</div>
        <table cellpadding="0" cellspacing="0" style="padding:0; margin:0; width:100%;">   
               <tr>
               <th valign="bottom">FAI ##</th>
               <th valign="bottom">Part ##</th>
               <th valign="bottom">Description</th>
               <th valign="bottom">Rev</th>
               <th valign="bottom">Supplier</th>
               <th valign="bottom">FAI Type</th>
               <th valign="bottom">Reason For FAI</th>
               <th valign="bottom">Finial Results</th>
               <th class="right" valign="bottom">Total Days</th>
           </tr> 
     <cfelse>
    <tr>
    	<td class="bottom" colspan="9"> 
     		<div align="left" style="font-weight:bold; font-size:15px;padding-top:10px;">Total FAI's  = <span style="color:##ff0000;">#noofrecords#</span></div>
    	</td>
    </tr>     
    <tr>
    	<td class="no-border" colspan="9"> 
        	<div style="font-size:15px;color:##0813E9;padding:10px 0px 10px 0px;">#UCase(company1)#</div>
    	</td>
    </tr>     
               <tr>
               <th valign="bottom">FAI ##</th>
               <th valign="bottom">Part ##</th>
               <th valign="bottom">Description</th>
               <th valign="bottom">Rev</th>
               <th valign="bottom">Supplier</th>
               <th valign="bottom">FAI Type</th>
               <th valign="bottom">Reason For FAI</th>
               <th valign="bottom">Finial Results</th>
               <th class="right" valign="bottom">Total Days</th>
     </cfif>  
	<cfset noofrecords= 0>
	<cfloop>
        <tr class="#iif((fai.currentRow MOD 2 EQ 0), DE('even'), DE('odd'))#">
           <td class="center">#fai.id#</td>
           <td class="center">#fai.itemnumber#</td>
           <td>#fai.description#</td>
           <td class="center">#fai.revision#</td>
		   <td>#fai.distributormanufacturer#</td>
           <td class="center">#fai.faitype#</td>
           <td class="center">#fai.reasonforfai#</td>
           <td class="center"><span class="<cfif #fai.finalresults# eq 'PASSED'>pass<cfelseif #fai.finalresults# eq 'FAILED'>red<cfelse>black</cfif>">
           #fai.finalresults#</span></td>
           <td class="center right">#fai.totaldaysinwork#</td>
       </tr>
    	<cfset totalrecords++>
       	<cfset noofrecords++>
	</cfloop>
    <cfif totalrecords eq fai.recordcount>
    <tr>
    	<td class="no-border" colspan="9"> 
     		<div align="left" style="font-weight:bold; font-size:15px;padding-top:10px;">Total FAI's  = <span style="color:##ff0000;">#noofrecords#</span></div>
    	</td>
    </tr>     
	</cfif>
</cfloop>
	</table>
    </div>
</form>
        <div class="col-lg-12">&nbsp;</div>
 
</body>

</cfoutput> 
</cfdocument>

<cffunction name="localUrl" >
 <cfargument name="file" />
 <cfset var fpath = ExpandPath(file)>
 <cfset var f="">
 <cfset f = createObject("java", "java.io.File")>
 <cfset f.init(fpath)>
 <cfreturn f.toUrl().toString()>
</cffunction>


